package picsarc.picspro.picslab.photocollageeditor.crop.callback;

public interface Callback {
  void onError(Throwable e);
}
