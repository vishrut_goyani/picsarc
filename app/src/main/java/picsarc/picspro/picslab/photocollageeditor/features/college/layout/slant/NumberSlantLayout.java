package picsarc.picspro.picslab.photocollageeditor.features.college.layout.slant;


import picsarc.picspro.picslab.photocollageeditor.features.college.slant.SlantCollegeLayout;

public abstract class NumberSlantLayout extends SlantCollegeLayout {
    protected int theme;

    public abstract int getThemeCount();

    public NumberSlantLayout() {
    }

    public NumberSlantLayout(SlantCollegeLayout slantPuzzleLayout, boolean z) {
        super(slantPuzzleLayout, z);
    }

    public NumberSlantLayout(int i) {
        if (i >= getThemeCount()) {
            StringBuilder sb = new StringBuilder();
            sb.append("NumberSlantLayout: the most theme count is ");
            sb.append(getThemeCount());
            sb.append(" ,you should let theme from 0 to ");
            sb.append(getThemeCount() - 1);
            sb.append(" .");
        }
        this.theme = i;
    }

    public int getTheme() {
        return this.theme;
    }
}
