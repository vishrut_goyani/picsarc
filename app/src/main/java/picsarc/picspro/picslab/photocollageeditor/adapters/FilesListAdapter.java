package picsarc.picspro.picslab.photocollageeditor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.interfaces.PhotoClickListener;
import picsarc.picspro.picslab.photocollageeditor.model.MyFiles;

public class FilesListAdapter extends RecyclerView.Adapter<FilesListAdapter.VH> {

    Context context;
    ArrayList<MyFiles> myFiles;
    PhotoClickListener photoClickListener;

    public FilesListAdapter(Context context, ArrayList<MyFiles> myFiles, PhotoClickListener photoClickListener) {
        this.context = context;
        this.myFiles = myFiles;
        this.photoClickListener = photoClickListener;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(context).inflate(R.layout.item_img_grid, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        Glide.with(context)
                .load(myFiles.get(position).getPath())
                .centerCrop()
                .into(holder.item_img);
        holder.itemView.setOnClickListener(v -> photoClickListener.onPhotoClick(position));
    }

    @Override
    public int getItemCount() {
        return myFiles.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        ImageView item_img;

        public VH(@NonNull View itemView) {
            super(itemView);
            item_img = itemView.findViewById(R.id.item_img);
        }
    }
}
