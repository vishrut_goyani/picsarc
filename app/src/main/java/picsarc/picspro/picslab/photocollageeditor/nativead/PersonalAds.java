package picsarc.picspro.picslab.photocollageeditor.nativead;

public class PersonalAds {
    private String id;
    private int ads_icon;
    private String ads_name;

    public String getAds_name() {
        return ads_name;
    }

    public void setAds_name(String ads_name) {
        this.ads_name = ads_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAds_icon() {
        return ads_icon;
    }

    public void setAds_icon(int ads_icon) {
        this.ads_icon = ads_icon;
    }

    public String getAds_url() {
        return ads_url;
    }

    public void setAds_url(String ads_url) {
        this.ads_url = ads_url;
    }

    private String ads_url;

}