package picsarc.picspro.picslab.photocollageeditor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UtilsSharePreference {
    private static final String PHOTO_EDITOR_PRO_2019 = "PHOTO_EDITOR_PRO_2019";

    public static void setHeightOfKeyboard(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PHOTO_EDITOR_PRO_2019, 0).edit();
        edit.putInt("height_of_keyboard", i);
        edit.apply();
    }

    public static int getHeightOfKeyboard(Context context) {
        return context.getSharedPreferences(PHOTO_EDITOR_PRO_2019, 0).getInt("height_of_keyboard", -1);
    }
}
