package picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.model.PictureModel;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.myinterface.OnListFolder;


public class GalleryListFoldersAdapter extends ArrayAdapter<PictureModel> {
    Context context;
    ArrayList<PictureModel> data;
    int layoutResourceId;
    OnListFolder onListFolder;
    int pHeightItem;

    static class RecordHolder {
        ImageView imageItem;
        RelativeLayout layoutRoot;

        RecordHolder() {
        }
    }

    public GalleryListFoldersAdapter(Context context2, int layoutId, ArrayList<PictureModel> arrayList) {
        super(context2, layoutId, arrayList);
        this.layoutResourceId = layoutId;
        this.context = context2;
        this.data = arrayList;
        this.pHeightItem = getDisplayInfo((Activity) context2).widthPixels / 3;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        RecordHolder recordHolder;
        if (view == null) {
            view = ((Activity) this.context).getLayoutInflater().inflate(this.layoutResourceId, viewGroup, false);
            recordHolder = new RecordHolder();
            recordHolder.imageItem = view.findViewById(R.id.imageItem);
            recordHolder.layoutRoot = view.findViewById(R.id.layoutRoot);
            recordHolder.layoutRoot.getLayoutParams().height = this.pHeightItem;
            recordHolder.imageItem.getLayoutParams().width = this.pHeightItem;
            recordHolder.imageItem.getLayoutParams().height = this.pHeightItem;
            view.setTag(recordHolder);
        } else {
            recordHolder = (RecordHolder) view.getTag();
        }
        final PictureModel pictureModel = this.data.get(i);
        ((RequestBuilder) Glide.with(this.context).load(pictureModel.getPathFile()).placeholder(R.drawable.piclist_icon_default)).into(recordHolder.imageItem);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GalleryListFoldersAdapter.this.onListFolder != null) {
                    GalleryListFoldersAdapter.this.onListFolder.OnItemListAlbumClick(pictureModel);
                }
            }
        });
        return view;
    }


    public void setOnListFolder(OnListFolder onListFolder2) {
        this.onListFolder = onListFolder2;
    }

    public static DisplayMetrics getDisplayInfo(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }
}
