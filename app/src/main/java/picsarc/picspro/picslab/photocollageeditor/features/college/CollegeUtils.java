package picsarc.picspro.picslab.photocollageeditor.features.college;

import java.util.ArrayList;
import java.util.List;

import picsarc.picspro.picslab.photocollageeditor.features.college.layout.slant.SlantLayoutHelper;
import picsarc.picspro.picslab.photocollageeditor.features.college.layout.straight.StraightLayoutHelper;

public class CollegeUtils {

    private CollegeUtils() {
    }

    public static List<CollegeLayout> getPuzzleLayouts(int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(SlantLayoutHelper.getAllThemeLayout(i));
        arrayList.addAll(StraightLayoutHelper.getAllThemeLayout(i));
        return arrayList;
    }
}