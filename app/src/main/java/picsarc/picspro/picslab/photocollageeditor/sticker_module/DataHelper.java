package picsarc.picspro.picslab.photocollageeditor.sticker_module;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import picsarc.picspro.picslab.photocollageeditor.sticker_module.model.Sticker_Model;

import java.util.ArrayList;


public class DataHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "background.db";

    public static final String COL_4 = "SMALL_THUMB";
    public static final String COL_5 = "BIG_THUMB";
    public static final String COL_6 = "ZIP";
    public static final String COL_7 = "CAT_NAME";

    public static final String TABLE = "Bg";

    public DataHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    public String Background = "create table " + TABLE + "(ID INTEGER PRIMARY KEY AUTOINCREMENT,SID TEXT,NAME TEXT)";


    public static final String CREATE_TABLE =
            "CREATE TABLE sticker(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "SMALL_THUMB TEXT," +
                    "BIG_THUMB TEXT," +
                    "ZIP TEXT," + "CAT_NAME TEXT" + ")";
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(Background);
    }

    public void Insert(String small_thumb, String big_thumb, String zip, String cat_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_4, small_thumb);
        contentValues.put(COL_5, big_thumb);
        contentValues.put(COL_6, zip);
        contentValues.put(COL_7, cat_name);

        db.insert("sticker", null, contentValues);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        db.execSQL("DROP TABLE IF EXISTS sticker");
        onCreate(db);
    }

    public Cursor getcount() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("select * from sticker", null);
        return c;
    }

    public ArrayList<Sticker_Model.Datas> getAllNotes(String cat_name) {
        ArrayList<Sticker_Model.Datas> datas = new ArrayList<>();

        String selectQuery = "SELECT * FROM sticker" + " WHERE CAT_NAME" + " = '" + cat_name + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Sticker_Model.Datas model = new Sticker_Model.Datas();
                model.setCategory_big_thumb(cursor.getString(2));
                model.setCategory_zip_url(cursor.getString(3));
                model.setCategory_name(cursor.getString(4));
                datas.add(model);
            } while (cursor.moveToNext());
        }

        db.close();

        return datas;
    }


}
