package picsarc.picspro.picslab.photocollageeditor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.adapters.ShowImagesAdapter;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;

public class FullViewActivity extends AppCompatActivity {
    private int imgPosition = 0;
    private FullViewActivity activity;
    private ArrayList<File> fileArrayList;
    ViewPager vp_view;
    ShowImagesAdapter showImagesAdapter;
    MaterialButton imDelete, imShare;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_full_view);
        this.activity = this;
        init();
        if (getIntent().getExtras() != null) {
            this.fileArrayList = getFromSdcard();
            this.imgPosition = getIntent().getIntExtra("Position", 0);
        }
        initViews();
    }

    public ArrayList<File> getFromSdcard() {
        ArrayList<File> filesList = new ArrayList<>();
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "PicsArc");
        if (file.isDirectory()) {
            File[] listFile = file.listFiles();
            if (listFile != null) {
                filesList.addAll(Arrays.asList(listFile));
            }
        }
        return filesList;
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        vp_view = findViewById(R.id.vp_view);
        imDelete = findViewById(R.id.btnDelete);
        imShare = findViewById(R.id.btnShare);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    public void initViews() {
        showImagesAdapter = new ShowImagesAdapter(this, fileArrayList);
        vp_view.setAdapter(showImagesAdapter);
        vp_view.setCurrentItem(imgPosition);
        ((TextView) findViewById(R.id.app_title)).setText((vp_view.getCurrentItem() + 1) + " of " + fileArrayList.size());
        vp_view.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int i) {
            }

            @Override
            public void onPageScrolled(int i, float f, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                imgPosition = i;
                ((TextView) findViewById(R.id.app_title)).setText((vp_view.getCurrentItem() + 1) + " of " + fileArrayList.size());
            }
        });
        imDelete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Widget_Discard_DialogTheme);
            builder.setPositiveButton("Yes", (dialogInterface, i) -> {
                if (fileArrayList.get(imgPosition).delete()) {
                    deleteFileAA(imgPosition);
                }
            });
            builder.setNegativeButton("No", (dialogInterface, i) -> {
                AppUtils.isDeleted = false;
                dialogInterface.cancel();
            });
            AlertDialog create = builder.create();
            create.setTitle(getResources().getString(R.string.do_u_want_to_dlt));
            create.show();
        });
        imShare.setOnClickListener(view -> {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, fileArrayList.get(imgPosition));
            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(share, getString(R.string.send_to)));
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        activity = this;
    }

    public void deleteFileAA(int i) {
        this.fileArrayList.remove(i);
        this.showImagesAdapter.notifyDataSetChanged();
        ((TextView) findViewById(R.id.app_title)).setText((vp_view.getCurrentItem() + 1) + " of " + fileArrayList.size());
        AppUtils.isDeleted = true;
        Toast.makeText(activity, "File Deleted Successfully", Toast.LENGTH_SHORT).show();
        if (fileArrayList.size() == 0) {
            onBackPressed();
        }
    }
}