package picsarc.picspro.picslab.photocollageeditor.features.draw;

public interface BrushColorListener {
    void onColorChanged(String str);
}
