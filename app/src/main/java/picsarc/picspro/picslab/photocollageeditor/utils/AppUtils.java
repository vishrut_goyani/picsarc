package picsarc.picspro.picslab.photocollageeditor.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import picsarc.picspro.picslab.photocollageeditor.R;

public class AppUtils {
    public static String string;
    public static String images;
    public static Bitmap Sticker_bitmap;

    public static String RECEIVER_ADDRESS = "andro.ops151@gmail.com";
    public static final String is_rated = "is_rated";
    public static final String count_ = "counts";
    public static final String APP_PREFS = "PHOTO_EDITOR_PREFS";
    static SharedPreferences sharedPreferences;
    private static AppUtils mInstance;
    public static Context mContext;
    public static boolean isDeleted = false;

    public AppUtils(Context context) {
        mContext = context;
        try {
            sharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized AppUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AppUtils(context);
        }
        return mInstance;
    }

    public static void ShareApp(Context context2) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.SUBJECT", context2.getString(R.string.app_name));
        intent.putExtra("android.intent.extra.TEXT", context2.getString(R.string.share_app_desc) + ("\nhttps://play.google.com/store/apps/details?id=" + context2.getPackageName()));
        intent.setType("text/plain");
        context2.startActivity(Intent.createChooser(intent, "Share"));
    }

    public static int dpToPx(Context context, int i) {
        return (int) (((float) i) * context.getResources().getDisplayMetrics().density);
    }

    public static void setRated(boolean rated) {
        sharedPreferences.edit().putBoolean(is_rated, rated).apply();
    }

    public static boolean getRated() {
        return sharedPreferences.getBoolean(is_rated, false);
    }

    public static void setCount(int count) {
        sharedPreferences.edit().putInt(count_, count).apply();
    }

    public static int getCounts() {
        return sharedPreferences.getInt(count_, 0);
    }
}