package picsarc.picspro.picslab.photocollageeditor.features.college.layout.straight;

import picsarc.picspro.picslab.photocollageeditor.features.college.straight.StraightCollegeLayout;

public abstract class NumberStraightLayout extends StraightCollegeLayout {
    protected int theme;

    public abstract int getThemeCount();

    public NumberStraightLayout() {
    }

    public NumberStraightLayout(StraightCollegeLayout straightPuzzleLayout, boolean z) {
        super(straightPuzzleLayout, z);
    }

    public NumberStraightLayout(int i) {
        if (i >= getThemeCount()) {
            StringBuilder sb = new StringBuilder();
            sb.append("NumberStraightLayout: the most theme count is ");
            sb.append(getThemeCount());
            sb.append(" ,you should let theme from 0 to ");
            sb.append(getThemeCount() - 1);
            sb.append(" .");
        }
        this.theme = i;
    }

    public int getTheme() {
        return this.theme;
    }
}
