package picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor;

import android.graphics.Bitmap;

public interface OnSaveBitmap {
    void onBitmapReady(Bitmap bitmap);

}
