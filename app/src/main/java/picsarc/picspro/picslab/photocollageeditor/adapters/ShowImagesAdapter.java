package picsarc.picspro.picslab.photocollageeditor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.File;
import java.util.ArrayList;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.utils.BitmapUtils;

public class ShowImagesAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<File> imageList;
    private LayoutInflater inflater;
    SubsamplingScaleImageView im_fullViewImage;

    @Override
    public int getItemPosition(Object obj) {
        return -2;
    }

    @Override
    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public ShowImagesAdapter(Context context2, ArrayList<File> arrayList) {
        this.context = context2;
        this.imageList = arrayList;
        this.inflater = LayoutInflater.from(context2);
    }

    @Override
    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    @SuppressLint("WrongConstant")
    @Override
    public Object instantiateItem(ViewGroup viewGroup, final int i) {
        View inflate = this.inflater.inflate(R.layout.slidingimages_layout, viewGroup, false);
        im_fullViewImage = inflate.findViewById(R.id.im_fullViewImage);
        im_fullViewImage.setOrientation(BitmapUtils.getOrientation(Uri.parse(imageList.get(i).getPath()), context));
        im_fullViewImage.setImage(ImageSource.uri(imageList.get(i).getPath()));
        viewGroup.addView(inflate, 0);
        return inflate;
    }

    @Override
    public int getCount() {
        return this.imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view.equals(obj);
    }
}