package picsarc.picspro.picslab.photocollageeditor.filters;

public interface FilterListener {
    void onFilterSelected(String str);
}
