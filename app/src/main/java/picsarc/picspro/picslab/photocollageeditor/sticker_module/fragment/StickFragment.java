package picsarc.picspro.picslab.photocollageeditor.sticker_module.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.DataHelper;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.ViewAnimation;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class StickFragment extends Fragment {

    ViewPager viewPager;
    TabLayout tabHost;
    private View view;
    private StickFragment stickFragment = this;
    ImageView btn_s_done;
    ViewPagerAdapter viewpagerAdaptrer;
    BGSelectListener galleryListener;
    DataHelper dataHelper;
    LinearLayout lyt_progress;
    ArrayList<String> strings = new ArrayList<>();
    TextView txtinternet;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.stick_catego_lay, container, false);

        dataHelper = new DataHelper(getContext());

        viewPager = view.findViewById(R.id.bottom_sheet_viewpager);
        tabHost = view.findViewById(R.id.bottom_sheet_tabs);
        btn_s_done = view.findViewById(R.id.btn_s_done);
        lyt_progress = view.findViewById(R.id.lyt_progress);
        txtinternet = view.findViewById(R.id.txtinternet);
        btn_s_done = view.findViewById(R.id.btn_s_done);
        if (isNetworkConnected()) {
            getdata();
        } else {

            Cursor c = dataHelper.getcount();
            if (c != null) {
                if (c.moveToFirst()) {
                    do {

                        strings.add(c.getString(4));


                    } while (c.moveToNext());
                }

                try {
                    for (int i = 0; i < strings.size(); i++) {

                        tabHost.addTab(tabHost.newTab().setText(strings.get(i)));

                    }
                    viewpagerAdaptrer = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), tabHost.getTabCount(), strings);
                    viewpagerAdaptrer.notifyDataSetChanged();
                    viewPager.setAdapter(viewpagerAdaptrer);
                    int limit = (viewpagerAdaptrer.getCount() > 1 ? viewpagerAdaptrer.getCount() - 1 : 1);
                    viewPager.setOffscreenPageLimit(limit);
                    viewPager.setOffscreenPageLimit(3);

                    if (strings.size() > 0) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ViewAnimation.fadeOut(lyt_progress);
                            }
                        }, 200);
                    } else {
                        txtinternet.setText("Please Check Internet Connection");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        tabHost.setupWithViewPager(viewPager);


        btn_s_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().hide(stickFragment).commit();
            }
        });
        return view;
    }


    public interface BGSelectListener {

        void onBGSelectFragmentCancel();
    }

    public void setBgSelectListner(BGSelectListener var1) {

        this.galleryListener = var1;
    }

    public void backtrace() {
        this.galleryListener.onBGSelectFragmentCancel();
    }

    @SuppressLint("MissingPermission")
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void getdata() {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get("https://androoceans.com/category", new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String photo = jsonObject.getString("category_small_thumb");
                        String zip = jsonObject.getString("category_zip_url");
                        String name = jsonObject.getString("category_name");
                        String bigthumb = jsonObject.getString("category_big_thumb");
                        try {
                            strings.add(name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dataHelper.Insert(photo, bigthumb, zip, name);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isNetworkConnected()) {
                    try {
                        for (int i = 0; i < strings.size(); i++) {
                            tabHost.addTab(tabHost.newTab().setText(strings.get(i)));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), "Please Check your Internet connection", Toast.LENGTH_SHORT).show();
                }

                tabHost.setTabGravity(TabLayout.GRAVITY_FILL);
                viewpagerAdaptrer = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), tabHost.getTabCount(), strings);
                int limit = (viewpagerAdaptrer.getCount() > 1 ? viewpagerAdaptrer.getCount() - 1 : 1);
                viewPager.setOffscreenPageLimit(limit);
                viewpagerAdaptrer.notifyDataSetChanged();
                viewPager.setAdapter(viewpagerAdaptrer);
                viewPager.setOffscreenPageLimit(3);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ViewAnimation.fadeOut(lyt_progress);
                    }
                }, 200);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFinish() {
                super.onFinish();

            }
        });
    }

}
