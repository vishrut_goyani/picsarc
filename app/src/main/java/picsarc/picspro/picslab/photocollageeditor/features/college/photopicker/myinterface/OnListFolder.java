package picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.myinterface;


import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.model.PictureModel;

public interface OnListFolder {
    void OnItemListAlbumClick(PictureModel pictureModel);
}
