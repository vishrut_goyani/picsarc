package picsarc.picspro.picslab.photocollageeditor.features.college;

import android.graphics.RectF;

import picsarc.picspro.picslab.photocollageeditor.features.college.slant.SlantCollegeLayout;
import picsarc.picspro.picslab.photocollageeditor.features.college.straight.StraightCollegeLayout;

public class CollegeLayoutParser {
    private CollegeLayoutParser() {
    }

    public static CollegeLayout parse(final CollegeLayout.Info info) {
        CollegeLayout collegeLayout;
        if (info.type == 0) {
            collegeLayout = new StraightCollegeLayout() {
                public CollegeLayout clone(CollegeLayout collegeLayout) {
                    return null;
                }

                public void layout() {
                    int size = info.steps.size();
                    int i = 0;
                    for (int i2 = 0; i2 < size; i2++) {
                        CollegeLayout.Step step = info.steps.get(i2);
                        switch (step.type) {
                            case 0:
                                addLine(step.position, step.lineDirection(), info.lines.get(i).getStartRatio());
                                break;
                            case 1:
                                addCross(step.position, step.hRatio, step.vRatio);
                                break;
                            case 2:
                                cutAreaEqualPart(step.position, step.hSize, step.vSize);
                                break;
                            case 3:
                                cutAreaEqualPart(step.position, step.part, step.lineDirection());
                                break;
                            case 4:
                                cutSpiral(step.position);
                                break;
                        }
                        i += step.numOfLine;
                    }
                }
            };
        } else {
            collegeLayout = new SlantCollegeLayout() {
                public CollegeLayout clone(CollegeLayout collegeLayout) {
                    return null;
                }

                public void layout() {
                    int size = info.steps.size();
                    for (int i = 0; i < size; i++) {
                        CollegeLayout.Step step = info.steps.get(i);
                        switch (step.type) {
                            case 0:
                                addLine(step.position, step.lineDirection(), info.lines.get(i).getStartRatio(), info.lines.get(i).getEndRatio());
                                break;
                            case 1:
                                addCross(step.position, 0.5f, 0.5f, 0.5f, 0.5f);
                                break;
                            case 2:
                                cutArea(step.position, step.hSize, step.vSize);
                                break;
                        }
                    }
                }
            };
        }
        collegeLayout.setOuterBounds(new RectF(info.left, info.top, info.right, info.bottom));
        collegeLayout.layout();
        collegeLayout.setColor(info.color);
        collegeLayout.setRadian(info.radian);
        collegeLayout.setPadding(info.padding);
        int size = info.lineInfos.size();
        for (int i = 0; i < size; i++) {
            CollegeLayout.LineInfo lineInfo = info.lineInfos.get(i);
            Line line = collegeLayout.getLines().get(i);
            line.startPoint().x = lineInfo.startX;
            line.startPoint().y = lineInfo.startY;
            line.endPoint().x = lineInfo.endX;
            line.endPoint().y = lineInfo.endY;
        }
        collegeLayout.sortAreas();
        collegeLayout.update();
        return collegeLayout;
    }
}
