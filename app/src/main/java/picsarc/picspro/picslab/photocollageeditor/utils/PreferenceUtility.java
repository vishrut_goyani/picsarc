package picsarc.picspro.picslab.photocollageeditor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtility {
    public static SharedPreferences prefs;
    private static PreferenceUtility mInstance;
    public static Context mContext;
    public static final String APP_PREFS = "BEST_PHOTO_EDITOR_PREFS";
    public static final String is_admob_personal = "is_admob_personal";
    public static final String is_adx = "is_adx";
    public static final String is_facebook_personal = "is_facebook_personal";

    public PreferenceUtility(Context context) {
        mContext = context;
        try {
            prefs = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized PreferenceUtility getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceUtility(context);
        }
        return mInstance;
    }

    public void setAdmobPersonal(int enabled) {
        prefs.edit().putInt(is_admob_personal, enabled).apply();
    }

    public int getAdmobPersonal() {
        return prefs.getInt(is_admob_personal, 1);
    }

    public void setFacebookPersonal(int enabled) {
        prefs.edit().putInt(is_facebook_personal, enabled).apply();
    }

    public int getFacebookPersonal() {
        return prefs.getInt(is_facebook_personal, 1);
    }

    public void setAdx(int enabled) {
        prefs.edit().putInt(is_adx, enabled).apply();
    }

    public int getAdx() {
        return prefs.getInt(is_adx, 1);
    }
}
