package picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor;

public interface OnPhotoEditorListener {
    void onAddViewListener();

    void onRemoveViewListener();

    void onStartViewChangeListener();

    void onStopViewChangeListener();
}
