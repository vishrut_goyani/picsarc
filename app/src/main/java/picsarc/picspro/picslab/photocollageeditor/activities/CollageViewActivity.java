package picsarc.picspro.picslab.photocollageeditor.activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.steelkiwi.cropiwa.AspectRatio;

import org.wysaid.nativePort.CGENativeLibrary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.base.BaseActivity;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.AddTextProperties;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.EditorTextDialogFragment;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeLayout;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeLayoutParser;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegePiece;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeUtils;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeView;
import picsarc.picspro.picslab.photocollageeditor.features.college.adapter.CollegeAdapter;
import picsarc.picspro.picslab.photocollageeditor.features.college.adapter.CollegeBGAdapter;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.activity.PickPhotoActivity;
import picsarc.picspro.picslab.photocollageeditor.features.crop.PicsartCropDialogFragment;
import picsarc.picspro.picslab.photocollageeditor.features.crop.adapter.AspectRatioPreviewAdapter;
import picsarc.picspro.picslab.photocollageeditor.filters.AdapterFilterView;
import picsarc.picspro.picslab.photocollageeditor.filters.FilterDialogFragment;
import picsarc.picspro.picslab.photocollageeditor.filters.FilterListener;
import picsarc.picspro.picslab.photocollageeditor.filters.UtilsFilter;
import picsarc.picspro.picslab.photocollageeditor.sticker.DrawableSticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.Sticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerIconBitmap;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerView;
import picsarc.picspro.picslab.photocollageeditor.sticker.TextSticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EditTextIconEvent;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventDeleteIconStickerIcon;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventStickerIconFlipHorizontally;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventZoomIconStickerIcon;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker.Sticker_interfce;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.fragment.StickFragment;
import picsarc.picspro.picslab.photocollageeditor.tools.EditingToolsAdapter;
import picsarc.picspro.picslab.photocollageeditor.tools.PieceToolsAdapter;
import picsarc.picspro.picslab.photocollageeditor.tools.ToolType;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.FileUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.SystemUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsSharePreference;

@SuppressLint("StaticFieldLeak")
public class CollageViewActivity extends BaseActivity implements EditingToolsAdapter.OnItemSelected, AspectRatioPreviewAdapter.OnNewSelectedListener,
        CollegeBGAdapter.BackgroundChangeListener, FilterListener, PicsartCropDialogFragment.OnCropPhoto, FilterDialogFragment.OnFilterSavePhoto,
        PieceToolsAdapter.OnPieceFuncItemSelected, CollegeAdapter.OnItemClickListener, Sticker_interfce {
    private static CollageViewActivity instance;
    public static CollageViewActivity puzzle;
    public ImageView addNewText;
    public ConstraintLayout changeBackgroundLayout;
    private LinearLayout changeBorder;
    public ConstraintLayout changeLayoutLayout;
    public AspectRatio currentAspect;
    public CollegeBGAdapter.SquareView currentBackgroundState;
    public ToolType currentMode;
    public int deviceWidth = 0;
    public ConstraintLayout filterLayout;
    private LottieAnimationView loadingView;
    public List<Bitmap> lstBitmapWithFilter = new ArrayList<>();
    public List<Drawable> lstOriginalDrawable = new ArrayList<>();
    public List<String> lstPaths;
    public float pieceBorderRadius;
    public float piecePadding;
    private PieceToolsAdapter pieceToolsAdapter = new PieceToolsAdapter(this);

    public CollegeLayout collegeLayout;
    private RecyclerView puzzleList;

    public CollegeView collegeView;
    private RecyclerView radiusLayout;
    private RecyclerView rvBackgroundBlur;
    private RecyclerView rvBackgroundColor;
    private RecyclerView rvBackgroundGradient;

    public RecyclerView rvFilterView;

    public RecyclerView rvPieceControl;
    private SeekBar sbChangeBorderRadius;
    private SeekBar sbChangeBorderSize;
    Toolbar toolbar;

    public ConstraintLayout stickerLayout;
    public List<Target> targets = new ArrayList();
    public EditorTextDialogFragment.TextEditor textEditor;
    public EditorTextDialogFragment TextEditorDialogFrag;
    public ConstraintLayout textLayout;
    private TextView tvChangeBackgroundBlur;
    private TextView tvChangeBackgroundColor;
    private TextView tvChangeBackgroundGradient;
    private TextView tvChangeBorder;
    private TextView tvChangeLayout;
    private TextView tvChangeRatio;
    private ConstraintLayout wrapPuzzleView;
    public static CollageViewActivity collageViewActivity;
    public FrameLayout sticker_grid_fragment_container;

    AlertDialog.Builder builder;
    LinearLayout adViewLL;
    View view;
    boolean isSaveDialog;

    StickFragment.BGSelectListener stickbgSlect;
    StickFragment stickFragment;

    ActivityResultLauncher<String> singleImagePicker
            = registerForActivityResult(new ActivityResultContracts.GetContent(),
            uri -> {
                replaceCurrentPic(SystemUtils.getRealPathFromUri(CollageViewActivity.this, uri));
            });

    private EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this, true);
    public CGENativeLibrary.LoadImageCallback mLoadImageCallback = new CGENativeLibrary.LoadImageCallback() {
        public Bitmap loadImage(String str, Object obj) {
            try {
                return BitmapFactory.decodeStream(getAssets().open(str));
            } catch (IOException io) {
                return null;
            }
        }

        public void loadImageOK(Bitmap bitmap, Object obj) {
            bitmap.recycle();
        }
    };

    public RecyclerView mRvTools;
    private ConstraintLayout mainActivity;
    public View.OnClickListener onClickListener = view -> {
        switch (view.getId()) {
            case R.id.imgCloseBackground:
            case R.id.imgCloseFilter:
            case R.id.imgCloseLayout:
            case R.id.imgCloseSticker:
            case R.id.imgCloseText:
                slideDownSaveView();
                onBackPressed();
                return;
            case R.id.imgSaveBackground:
                slideDown(changeBackgroundLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                showDownFunction();
                this.collegeView.setLocked(true);
                this.collegeView.setTouchEnable(true);
                if (this.collegeView.getBackgroundResourceMode() == 0) {
                    currentBackgroundState.isColor = true;
                    currentBackgroundState.isBitmap = false;
                    currentBackgroundState.drawableId = ((ColorDrawable) this.collegeView.getBackground()).getColor();
                    currentBackgroundState.drawable = null;
                } else if (this.collegeView.getBackgroundResourceMode() == 1) {
                    currentBackgroundState.isColor = false;
                    currentBackgroundState.isBitmap = false;
                    currentBackgroundState.drawable = this.collegeView.getBackground();
                } else {
                    currentBackgroundState.isColor = false;
                    currentBackgroundState.isBitmap = true;
                    currentBackgroundState.drawable = this.collegeView.getBackground();
                }
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveFilter:
                slideDown(filterLayout);
                slideUp(mRvTools);
                currentMode = ToolType.NONE;
                slideDownSaveView();
                this.collegeView.setTouchEnable(true);
                return;
            case R.id.imgSaveLayout:
                slideUp(mRvTools);
                slideDown(changeLayoutLayout);
                slideDownSaveView();
                showDownFunction();
                collegeLayout = this.collegeView.getCollegeLayout();
                pieceBorderRadius = this.collegeView.getPieceRadian();
                piecePadding = this.collegeView.getPiecePadding();
                this.collegeView.setLocked(true);
                this.collegeView.setTouchEnable(true);
                currentAspect = this.collegeView.getAspectRatio();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveSticker:
                this.collegeView.setHandlingSticker(null);

                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                collegeView.setLocked(true);
                collegeView.setTouchEnable(true);
                currentMode = ToolType.NONE;
                sticker_grid_fragment_container.setVisibility(View.GONE);

                return;
            case R.id.imgSaveText:
                this.collegeView.setHandlingSticker(null);
                this.collegeView.setLocked(true);
                addNewText.setVisibility(View.GONE);
                slideDown(this.textLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                this.collegeView.setLocked(true);
                this.collegeView.setTouchEnable(true);
                currentMode = ToolType.NONE;
                return;
            case R.id.tv_blur:
                selectBackgroundBlur();
                return;
            case R.id.tv_change_border:
                selectBorderTool();
                return;
            case R.id.tv_change_layout:
                selectLayoutTool();
                return;
            case R.id.tv_change_ratio:
                selectRadiusTool();
                return;
            case R.id.tv_color:
                selectBackgroundColorTab();
                return;
            case R.id.tv_radian:
                selectBackgroundGradientTab();
                return;
            default:
        }
    };
    public SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            switch (seekBar.getId()) {
                case R.id.sk_border:
                    collegeView.setPiecePadding((float) i);
                    break;
                case R.id.sk_border_radius:
                    collegeView.setPieceRadian((float) i);
                    break;
            }
            collegeView.invalidate();
        }
    };

    public static CollageViewActivity getInstance() {
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.college_layout);

        toolbar = findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.app_title)).setText("Collage Maker");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        AppUtils.string = "XYZ";
        collageViewActivity = this;
        init();
        this.sbChangeBorderSize.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.sbChangeBorderRadius.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.mRvTools.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        this.rvPieceControl.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        this.lstPaths = getIntent().getStringArrayListExtra(PickPhotoActivity.KEY_DATA_RESULT);
        this.collegeLayout = CollegeUtils.getPuzzleLayouts(lstPaths.size()).get(0);
        this.collegeView.setCollegeLayout(this.collegeLayout);
        this.collegeView.setTouchEnable(true);
        this.collegeView.setNeedDrawLine(false);
        this.collegeView.setNeedDrawOuterLine(false);
        this.collegeView.setLineSize(4);
        this.collegeView.setPiecePadding(6.0f);
        this.collegeView.setPieceRadian(15.0f);
        this.collegeView.setLineColor(ContextCompat.getColor(this, R.color.white));
        this.collegeView.setSelectedLineColor(ContextCompat.getColor(this, R.color.colorAccent));
        this.collegeView.setHandleBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        this.collegeView.setAnimateDuration(300);
        this.collegeView.setOnPieceSelectedListener((collegePiece, i) -> {
            slideDown(mRvTools);
            slideUp(rvPieceControl);
            slideUpSaveView();
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) rvPieceControl.getLayoutParams();
            layoutParams.bottomMargin = SystemUtils.dpToPx(getApplicationContext(), 10);
            rvPieceControl.setLayoutParams(layoutParams);
            currentMode = ToolType.PIECE;
        });
        this.collegeView.setOnPieceUnSelectedListener(() -> {
            slideDown(rvPieceControl);
            slideUp(mRvTools);
            slideDownSaveView();
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) rvPieceControl.getLayoutParams();
            layoutParams.bottomMargin = 0;
            rvPieceControl.setLayoutParams(layoutParams);
            currentMode = ToolType.NONE;
        });
        this.collegeView.post(() -> loadPhoto());
        findViewById(R.id.imgCloseLayout).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgSaveLayout).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgCloseSticker).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgCloseFilter).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgCloseBackground).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgSaveSticker).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgCloseText).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgSaveText).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgSaveFilter).setOnClickListener(this.onClickListener);
        findViewById(R.id.imgSaveBackground).setOnClickListener(this.onClickListener);
        this.changeLayoutLayout = findViewById(R.id.changeLayoutLayout);
        this.changeBorder = findViewById(R.id.change_border);
        this.tvChangeLayout = findViewById(R.id.tv_change_layout);
        this.tvChangeLayout.setOnClickListener(this.onClickListener);
        this.tvChangeBorder = findViewById(R.id.tv_change_border);
        this.tvChangeBorder.setOnClickListener(this.onClickListener);
        this.tvChangeRatio = findViewById(R.id.tv_change_ratio);
        this.tvChangeRatio.setOnClickListener(this.onClickListener);
        this.tvChangeBackgroundColor = findViewById(R.id.tv_color);
        this.tvChangeBackgroundColor.setOnClickListener(this.onClickListener);
        this.tvChangeBackgroundGradient = findViewById(R.id.tv_radian);
        this.tvChangeBackgroundGradient.setOnClickListener(this.onClickListener);
        this.tvChangeBackgroundBlur = findViewById(R.id.tv_blur);
        this.tvChangeBackgroundBlur.setOnClickListener(this.onClickListener);
        CollegeAdapter collegeAdapter = new CollegeAdapter();
        this.puzzleList = findViewById(R.id.puzzleList);
        this.puzzleList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        this.puzzleList.setAdapter(collegeAdapter);
        collegeAdapter.refreshData(CollegeUtils.getPuzzleLayouts(this.lstPaths.size()), null);
        collegeAdapter.setOnItemClickListener(this);
        AspectRatioPreviewAdapter aspectRatioPreviewAdapter = new AspectRatioPreviewAdapter();
        aspectRatioPreviewAdapter.setListener(this);
        this.radiusLayout = findViewById(R.id.radioLayout);
        this.radiusLayout.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        this.radiusLayout.setAdapter(aspectRatioPreviewAdapter);
        this.textLayout = findViewById(R.id.textControl);
        this.addNewText = findViewById(R.id.addNewText);
        this.addNewText.setVisibility(View.GONE);
        this.addNewText.setOnClickListener(view -> {
            collegeView.setHandlingSticker(null);
            openTextFragment();
        });
        this.stickerLayout = findViewById(R.id.stickerLayout);
        TextView saveBitmap = findViewById(R.id.imgsave);
        saveBitmap.setVisibility(View.VISIBLE);
        saveBitmap.setOnClickListener(view -> {
            if (TedPermission.canRequestPermission(CollageViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                isSaveDialog = true;
                saveDiscardDialog(isSaveDialog);
            }
        });

        StickerIconBitmap bitmapStickerIcon0 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_delete_sticker), 0, StickerIconBitmap.REMOVE);
        bitmapStickerIcon0.setIconEvent(new EventDeleteIconStickerIcon());
        StickerIconBitmap bitmapStickerIcon1 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_flip_sticker), 1, StickerIconBitmap.FLIP);
        bitmapStickerIcon1.setIconEvent(new EventStickerIconFlipHorizontally());
        StickerIconBitmap bitmapStickerIcon2 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_edit_sticker), 2, StickerIconBitmap.EDIT);
        bitmapStickerIcon2.setIconEvent(new EditTextIconEvent());
        StickerIconBitmap bitmapStickerIcon3 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_resize_sticker), 3, StickerIconBitmap.ZOOM);
        bitmapStickerIcon3.setIconEvent(new EventZoomIconStickerIcon());
        StickerIconBitmap bitmapStickerIcon4 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_rotate_sticker), 3, StickerIconBitmap.ROTATE);
        bitmapStickerIcon4.setIconEvent(new EventZoomIconStickerIcon());
        ArrayList<StickerIconBitmap> arrayList = new ArrayList<>();
        arrayList.add(bitmapStickerIcon0);
        arrayList.add(bitmapStickerIcon3);
        arrayList.add(bitmapStickerIcon1);
        arrayList.add(bitmapStickerIcon3);
        arrayList.add(bitmapStickerIcon4);
        collegeView.setIcons(arrayList);
//        collegeView.setIcons(Arrays.asList(bitmapStickerIcon0, bitmapStickerIcon3, bitmapStickerIcon1, bitmapStickerIcon3, bitmapStickerIcon4));
        collegeView.setBackgroundColor(-16777216);
        collegeView.setLocked(false);
        collegeView.setConstrained(true);
        collegeView.setOnStickerOperationListener(onStickerOperationListener);
        changeBackgroundLayout = findViewById(R.id.changeBackgroundLayout);
        mainActivity = findViewById(R.id.puzzle_layout);
        changeLayoutLayout.setAlpha(0.0f);
        stickerLayout.setAlpha(0.0f);
        textLayout.setAlpha(0.0f);
        filterLayout.setAlpha(0.0f);
        changeBackgroundLayout.setAlpha(0.0f);
        rvPieceControl.setAlpha(0.0f);
        mainActivity.post(() -> {
            slideDown(changeLayoutLayout);
            slideDown(stickerLayout);
            slideDown(textLayout);
            slideDown(changeBackgroundLayout);
            slideDown(filterLayout);
            slideDown(rvPieceControl);
        });
        new Handler().postDelayed(() -> {
            changeLayoutLayout.setAlpha(1.0f);
            stickerLayout.setAlpha(1.0f);
            textLayout.setAlpha(1.0f);
            filterLayout.setAlpha(1.0f);
            changeBackgroundLayout.setAlpha(1.0f);
            rvPieceControl.setAlpha(1.0f);
        }, 1000);
        UtilsSharePreference.setHeightOfKeyboard(getApplicationContext(), 0);

        showLoading(false);
        currentBackgroundState = new CollegeBGAdapter.SquareView(Color.parseColor("#ffffff"), "", true);
        rvBackgroundColor = findViewById(R.id.colorList);
        rvBackgroundColor.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        rvBackgroundColor.setHasFixedSize(true);
        rvBackgroundColor.setAdapter(new CollegeBGAdapter(getApplicationContext(), this));
        rvBackgroundGradient = findViewById(R.id.radianList);
        rvBackgroundGradient.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        rvBackgroundGradient.setHasFixedSize(true);
        rvBackgroundGradient.setAdapter(new CollegeBGAdapter(getApplicationContext(), this, true));
        rvBackgroundBlur = findViewById(R.id.backgroundList);
        rvBackgroundBlur.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false));
        rvBackgroundBlur.setHasFixedSize(true);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) collegeView.getLayoutParams();
        layoutParams.height = point.x;
        layoutParams.width = point.x;
        collegeView.setLayoutParams(layoutParams);
        currentAspect = new AspectRatio(1, 1);
        collegeView.setAspectRatio(new AspectRatio(1, 1));
        puzzle = this;
        currentMode = ToolType.NONE;
        CGENativeLibrary.setLoadImageCallback(this.mLoadImageCallback, null);
        instance = this;
    }

    private void init() {
        deviceWidth = getResources().getDisplayMetrics().widthPixels;
        loadingView = findViewById(R.id.loadingView);
        collegeView = findViewById(R.id.puzzle_view);
        wrapPuzzleView = findViewById(R.id.wrapPuzzleView);
        filterLayout = findViewById(R.id.filterLayout);
        rvFilterView = findViewById(R.id.rvFilterView);
        mRvTools = findViewById(R.id.rvConstraintTools);
        mRvTools.setAdapter(mEditingToolsAdapter);
        rvPieceControl = findViewById(R.id.rvPieceControl);
        rvPieceControl.setAdapter(pieceToolsAdapter);
        sbChangeBorderSize = findViewById(R.id.sk_border);
        sbChangeBorderRadius = findViewById(R.id.sk_border_radius);
        sticker_grid_fragment_container = findViewById(R.id.sticker_grid_fragment_container);

        view = LayoutInflater.from(this).inflate(R.layout.save_dialog, null);
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    StickerView.OnStickerOperationListener onStickerOperationListener = new StickerView.OnStickerOperationListener() {
        @Override
        public void onTouchDownForBeauty(float f, float f2) {
        }

        @Override
        public void onTouchDragForBeauty(float f, float f2) {
        }

        @Override
        public void onTouchUpForBeauty() {

        }

        @Override
        public void onStickerAdded(@NonNull Sticker sticker) {

        }

        @Override
        public void onStickerClicked(@NonNull Sticker sticker) {

        }

        @Override
        public void onStickerDeleted() {

        }

        @Override
        public void onStickerTouchOutside() {

        }

        @Override
        public void onStickerTouchedDown() {

        }

        @Override
        public void onStickerZoomFinished() {

        }

        public void onStickerDoubleTapped(@NonNull Sticker sticker) {
            if (sticker instanceof TextSticker) {
                sticker.setShow(false);
                collegeView.setHandlingSticker(null);
                TextEditorDialogFrag = TextEditorDialogFrag.show(CollageViewActivity.this, ((TextSticker) sticker).getTextAddProperties());
                textEditor = new EditorTextDialogFragment.TextEditor() {
                    public void onDone(AddTextProperties TextAddProperties) {
                        collegeView.getStickers().remove(collegeView.getLastHandlingSticker());
                        collegeView.addSticker(new TextSticker(CollageViewActivity.this, TextAddProperties));
                    }

                    public void onBackButton() {
                        collegeView.showLastHandlingSticker();
                    }
                };
                TextEditorDialogFrag.setOnTextEditorListener(textEditor);
            }
        }

        @Override
        public void onStickerDragFinished() {

        }

        @Override
        public void onStickerFlipped() {

        }
    };

    private void openTextFragment() {
        this.TextEditorDialogFrag = EditorTextDialogFragment.show(this);
        this.textEditor = new EditorTextDialogFragment.TextEditor() {
            public void onDone(AddTextProperties TextAddProperties) {
                collegeView.addSticker(new TextSticker(getApplicationContext(), TextAddProperties));
            }

            public void onBackButton() {
                if (collegeView.getStickers().isEmpty()) {
                    onBackPressed();
                }
            }
        };
        this.TextEditorDialogFrag.setOnTextEditorListener(this.textEditor);
    }

    public void showLoading(boolean z) {
        if (z) {
            getWindow().setFlags(16, 16);
            this.loadingView.setVisibility(View.VISIBLE);
            return;
        }
        getWindow().clearFlags(16);
        this.loadingView.setVisibility(View.GONE);
    }

    public void selectBackgroundBlur() {
        ArrayList arrayList = new ArrayList();
        for (CollegePiece drawable : this.collegeView.getCollegePieces()) {
            arrayList.add(drawable.getDrawable());
        }
        CollegeBGAdapter collegeBGAdapter = new CollegeBGAdapter(getApplicationContext(), this, (List<Drawable>) arrayList);
        collegeBGAdapter.setSelectedSquareIndex(-1);
        this.rvBackgroundBlur.setAdapter(collegeBGAdapter);
        this.rvBackgroundBlur.setVisibility(View.VISIBLE);
        this.tvChangeBackgroundBlur.setBackgroundResource(R.drawable.border_bottom);
        this.tvChangeBackgroundBlur.setTextColor(ContextCompat.getColor(this, R.color.black));
        this.rvBackgroundGradient.setVisibility(View.GONE);
        this.tvChangeBackgroundGradient.setBackgroundResource(0);
        this.tvChangeBackgroundGradient.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.rvBackgroundColor.setVisibility(View.GONE);
        this.tvChangeBackgroundColor.setBackgroundResource(0);
        this.tvChangeBackgroundColor.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
    }

    public void selectBackgroundColorTab() {
        this.rvBackgroundColor.setVisibility(View.VISIBLE);
        this.tvChangeBackgroundColor.setBackgroundResource(R.drawable.border_bottom);
        this.tvChangeBackgroundColor.setTextColor(ContextCompat.getColor(this, R.color.black));
        this.rvBackgroundColor.scrollToPosition(0);
        ((CollegeBGAdapter) this.rvBackgroundColor.getAdapter()).setSelectedSquareIndex(-1);
        this.rvBackgroundColor.getAdapter().notifyDataSetChanged();
        this.rvBackgroundGradient.setVisibility(View.GONE);
        this.tvChangeBackgroundGradient.setBackgroundResource(0);
        this.tvChangeBackgroundGradient.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.rvBackgroundBlur.setVisibility(View.GONE);
        this.tvChangeBackgroundBlur.setBackgroundResource(0);
        this.tvChangeBackgroundBlur.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
    }

    public void selectBackgroundGradientTab() {
        this.rvBackgroundGradient.setVisibility(View.VISIBLE);
        this.tvChangeBackgroundGradient.setBackgroundResource(R.drawable.border_bottom);
        this.tvChangeBackgroundGradient.setTextColor(ContextCompat.getColor(this, R.color.black));
        this.rvBackgroundGradient.scrollToPosition(0);
        ((CollegeBGAdapter) this.rvBackgroundGradient.getAdapter()).setSelectedSquareIndex(-1);
        this.rvBackgroundGradient.getAdapter().notifyDataSetChanged();
        this.rvBackgroundColor.setVisibility(View.GONE);
        this.tvChangeBackgroundColor.setBackgroundResource(0);
        this.tvChangeBackgroundColor.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.rvBackgroundBlur.setVisibility(View.GONE);
        this.tvChangeBackgroundBlur.setBackgroundResource(0);
        this.tvChangeBackgroundBlur.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
    }


    public void selectLayoutTool() {
        this.puzzleList.setVisibility(View.VISIBLE);
        this.tvChangeLayout.setBackgroundResource(R.drawable.border_bottom);
        this.tvChangeLayout.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        this.changeBorder.setVisibility(View.GONE);
        this.tvChangeBorder.setBackgroundResource(0);
        this.tvChangeBorder.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.radiusLayout.setVisibility(View.GONE);
        this.tvChangeRatio.setBackgroundResource(0);
        this.tvChangeRatio.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
    }


    public void selectRadiusTool() {
        this.radiusLayout.setVisibility(View.VISIBLE);
        this.tvChangeRatio.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        this.tvChangeRatio.setBackgroundResource(R.drawable.border_bottom);
        this.puzzleList.setVisibility(View.GONE);
        this.tvChangeLayout.setBackgroundResource(0);
        this.tvChangeLayout.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.changeBorder.setVisibility(View.GONE);
        this.tvChangeBorder.setBackgroundResource(0);
        this.tvChangeBorder.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
    }

    public void selectBorderTool() {
        this.changeBorder.setVisibility(View.VISIBLE);
        this.tvChangeBorder.setBackgroundResource(R.drawable.border_bottom);
        this.tvChangeBorder.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        this.puzzleList.setVisibility(View.GONE);
        this.tvChangeLayout.setBackgroundResource(0);
        this.tvChangeLayout.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.radiusLayout.setVisibility(View.GONE);
        this.tvChangeRatio.setBackgroundResource(0);
        this.tvChangeRatio.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        this.sbChangeBorderRadius.setProgress((int) this.collegeView.getPieceRadian());
        this.sbChangeBorderSize.setProgress((int) this.collegeView.getPiecePadding());
    }

    private void showUpFunction(View view) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.mainActivity);
        constraintSet.connect(this.wrapPuzzleView.getId(), 1, this.mainActivity.getId(), 1, 0);
        constraintSet.connect(this.wrapPuzzleView.getId(), 4, view.getId(), 3, 0);
        constraintSet.connect(this.wrapPuzzleView.getId(), 2, this.mainActivity.getId(), 2, 0);
        constraintSet.applyTo(this.mainActivity);
    }


    public void showDownFunction() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.mainActivity);
        constraintSet.connect(this.wrapPuzzleView.getId(), 1, this.mainActivity.getId(), 1, 0);
        constraintSet.connect(this.wrapPuzzleView.getId(), 4, this.mRvTools.getId(), 3, 0);
        constraintSet.connect(this.wrapPuzzleView.getId(), 2, this.mainActivity.getId(), 2, 0);
        constraintSet.applyTo(this.mainActivity);
    }

    @Override
    public void onToolSelected(ToolType toolType) {
        this.currentMode = toolType;
        switch (toolType) {
            case LAYOUT:
                this.collegeLayout = this.collegeView.getCollegeLayout();
                this.currentAspect = this.collegeView.getAspectRatio();
                this.pieceBorderRadius = this.collegeView.getPieceRadian();
                this.piecePadding = this.collegeView.getPiecePadding();
                this.puzzleList.scrollToPosition(0);
                ((CollegeAdapter) this.puzzleList.getAdapter()).setSelectedIndex(-1);
                this.puzzleList.getAdapter().notifyDataSetChanged();
                this.radiusLayout.scrollToPosition(0);
                ((AspectRatioPreviewAdapter) this.radiusLayout.getAdapter()).setLastSelectedView(-1);
                this.radiusLayout.getAdapter().notifyDataSetChanged();
                selectLayoutTool();
                slideUpSaveView();
                slideUp(this.changeLayoutLayout);
                slideDown(this.mRvTools);
                showUpFunction(this.changeLayoutLayout);
                this.collegeView.setLocked(false);
                this.collegeView.setTouchEnable(false);
                return;
            case BORDER:
                this.collegeLayout = this.collegeView.getCollegeLayout();
                this.currentAspect = this.collegeView.getAspectRatio();
                this.pieceBorderRadius = this.collegeView.getPieceRadian();
                this.piecePadding = this.collegeView.getPiecePadding();
                this.puzzleList.scrollToPosition(0);
                ((CollegeAdapter) this.puzzleList.getAdapter()).setSelectedIndex(-1);
                this.puzzleList.getAdapter().notifyDataSetChanged();
                this.radiusLayout.scrollToPosition(0);
                ((AspectRatioPreviewAdapter) this.radiusLayout.getAdapter()).setLastSelectedView(-1);
                this.radiusLayout.getAdapter().notifyDataSetChanged();
                selectBorderTool();
                slideUpSaveView();
                slideUp(this.changeLayoutLayout);
                slideDown(this.mRvTools);
                showUpFunction(this.changeLayoutLayout);
                this.collegeView.setLocked(false);
                this.collegeView.setTouchEnable(false);
                return;
            case RATIO:
                this.collegeLayout = this.collegeView.getCollegeLayout();
                this.currentAspect = this.collegeView.getAspectRatio();
                this.pieceBorderRadius = this.collegeView.getPieceRadian();
                this.piecePadding = this.collegeView.getPiecePadding();
                this.puzzleList.scrollToPosition(0);
                ((CollegeAdapter) this.puzzleList.getAdapter()).setSelectedIndex(-1);
                this.puzzleList.getAdapter().notifyDataSetChanged();
                this.radiusLayout.scrollToPosition(0);
                ((AspectRatioPreviewAdapter) this.radiusLayout.getAdapter()).setLastSelectedView(-1);
                this.radiusLayout.getAdapter().notifyDataSetChanged();
                selectRadiusTool();
                slideUpSaveView();
                slideUp(this.changeLayoutLayout);
                slideDown(this.mRvTools);
                showUpFunction(this.changeLayoutLayout);
                this.collegeView.setLocked(false);
                this.collegeView.setTouchEnable(false);
                return;
            case FILTER:
                if (this.lstOriginalDrawable.isEmpty()) {
                    for (CollegePiece drawable : this.collegeView.getCollegePieces()) {
                        this.lstOriginalDrawable.add(drawable.getDrawable());
                    }
                }
                new LoadFilterBitmap().execute();
                slideUpSaveView();
                return;
            case STICKER:
                slideDown(mRvTools);
                Stickselect();
                collegeView.setLocked(false);
                this.collegeView.setTouchEnable(false);
                this.collegeLayout = this.collegeView.getCollegeLayout();
                this.currentAspect = this.collegeView.getAspectRatio();
                this.pieceBorderRadius = this.collegeView.getPieceRadian();
                this.piecePadding = this.collegeView.getPiecePadding();
                this.puzzleList.scrollToPosition(0);
                slideUp(this.stickerLayout);
                sticker_grid_fragment_container.setVisibility(View.VISIBLE);
                return;
            case TEXT:
                this.collegeView.setTouchEnable(false);
                slideUpSaveView();
                this.collegeView.setLocked(false);
                openTextFragment();
                slideDown(this.mRvTools);
                slideUp(this.textLayout);
                this.addNewText.setVisibility(View.VISIBLE);
                return;
            case BACKGROUND:
                this.collegeView.setLocked(false);
                this.collegeView.setTouchEnable(false);
                slideUpSaveView();
                selectBackgroundColorTab();
                slideDown(this.mRvTools);
                slideUp(this.changeBackgroundLayout);
                showUpFunction(this.changeBackgroundLayout);
                if (this.collegeView.getBackgroundResourceMode() == 0) {
                    this.currentBackgroundState.isColor = true;
                    this.currentBackgroundState.isBitmap = false;
                    this.currentBackgroundState.drawableId = ((ColorDrawable) this.collegeView.getBackground()).getColor();
                    return;
                } else if (this.collegeView.getBackgroundResourceMode() == 2 || (this.collegeView.getBackground() instanceof ColorDrawable)) {
                    this.currentBackgroundState.isBitmap = true;
                    this.currentBackgroundState.isColor = false;
                    this.currentBackgroundState.drawable = this.collegeView.getBackground();
                    return;
                } else if (this.collegeView.getBackground() instanceof GradientDrawable) {
                    this.currentBackgroundState.isBitmap = false;
                    this.currentBackgroundState.isColor = false;
                    this.currentBackgroundState.drawable = this.collegeView.getBackground();
                    return;
                } else {
                    return;
                }
            default:
        }
    }


    public void loadPhoto() {
        final int i;
        final ArrayList arrayList = new ArrayList();
        if (this.lstPaths.size() > this.collegeLayout.getAreaCount()) {
            i = this.collegeLayout.getAreaCount();
        } else {
            i = this.lstPaths.size();
        }
        for (int i2 = 0; i2 < i; i2++) {
            Target r4 = new Target() {
                public void onBitmapFailed(Exception exc, Drawable drawable) {
                }

                public void onPrepareLoad(Drawable drawable) {
                }

                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                    int width = bitmap.getWidth();
                    float f = (float) width;
                    float height = (float) bitmap.getHeight();
                    float max = Math.max(f / f, height / f);
                    if (max > 1.0f) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (f / max), (int) (height / max), false);
                    }
                    arrayList.add(bitmap);
                    if (arrayList.size() == i) {
                        if (lstPaths.size() < collegeLayout.getAreaCount()) {
                            for (int i = 0; i < collegeLayout.getAreaCount(); i++) {
                                try {
                                    collegeView.addPiece((Bitmap) arrayList.get(i % i));
                                } catch (Exception e) {
                                    Toast.makeText(CollageViewActivity.this, "An error occurred while loading image", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            collegeView.addPieces(arrayList);
                        }
                    }
                    targets.remove(this);
                }
            };
            Picasso picasso = Picasso.get();
            picasso.load("file:///" + this.lstPaths.get(i2)).resize(this.deviceWidth, this.deviceWidth).centerInside().config(Bitmap.Config.RGB_565).into(r4);
            this.targets.add(r4);
        }
    }

    public void slideUp(View view) {
        ObjectAnimator.ofFloat(view, "translationY", new float[]{(float) view.getHeight(), 0.0f}).start();
    }

    @Override
    public void onDestroy() {
        try {
            this.collegeView.reset();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void slideUpSaveView() {
        this.toolbar.setVisibility(View.GONE);
    }

    public void slideDownSaveView() {
        this.toolbar.setVisibility(View.VISIBLE);
    }

    public void slideDown(View view) {
        ObjectAnimator.ofFloat(view, "translationY", 0.0f, (float) view.getHeight()).start();
    }

    @Override
    public void onBackPressed() {
        if (this.currentMode == null) {
            super.onBackPressed();
            return;
        }
        try {
            switch (this.currentMode) {
                case LAYOUT:
                case BORDER:
                case RATIO:
                    slideDown(this.changeLayoutLayout);
                    slideUp(this.mRvTools);
                    slideDownSaveView();
                    showDownFunction();
                    this.collegeView.updateLayout(this.collegeLayout);
                    this.collegeView.setPiecePadding(this.piecePadding);
                    this.collegeView.setPieceRadian(this.pieceBorderRadius);
                    this.currentMode = ToolType.NONE;
                    getWindowManager().getDefaultDisplay().getSize(new Point());
                    onNewAspectRatioSelected(this.currentAspect);
                    this.collegeView.setAspectRatio(this.currentAspect);
                    this.collegeView.setLocked(true);
                    this.collegeView.setTouchEnable(true);
                    return;
                case FILTER:
                    slideUp(this.mRvTools);
                    slideDown(this.filterLayout);
                    this.collegeView.setLocked(true);
                    this.collegeView.setTouchEnable(true);
                    for (int i = 0; i < this.lstOriginalDrawable.size(); i++) {
                        this.collegeView.getCollegePieces().get(i).setDrawable(this.lstOriginalDrawable.get(i));
                    }
                    this.collegeView.invalidate();
                    slideDownSaveView();
                    this.currentMode = ToolType.NONE;
                    return;
                case STICKER:
                    if (this.stickFragment != null && this.stickFragment.isVisible()) {
                        this.stickFragment.backtrace();
                        slideDown(stickerLayout);
                        slideUp(mRvTools);
                        collegeView.setLocked(false);
                    } else {
                        isSaveDialog = false;
                        saveDiscardDialog(isSaveDialog);
                    }
                    return;
                case TEXT:
                    if (!this.collegeView.getStickers().isEmpty()) {
                        this.collegeView.getStickers().clear();
                        this.collegeView.setHandlingSticker(null);
                    }
                    slideDown(this.textLayout);
                    this.addNewText.setVisibility(View.GONE);
                    this.collegeView.setHandlingSticker(null);
                    slideUp(this.mRvTools);
                    slideDownSaveView();
                    this.collegeView.setLocked(true);
                    this.currentMode = ToolType.NONE;
                    this.collegeView.setTouchEnable(true);
                    return;
                case BACKGROUND:
                    slideUp(this.mRvTools);
                    slideDown(this.changeBackgroundLayout);
                    this.collegeView.setLocked(true);
                    this.collegeView.setTouchEnable(true);
                    if (this.currentBackgroundState.isColor) {
                        this.collegeView.setBackgroundResourceMode(0);
                        this.collegeView.setBackgroundColor(this.currentBackgroundState.drawableId);
                    } else if (this.currentBackgroundState.isBitmap) {
                        this.collegeView.setBackgroundResourceMode(2);
                        this.collegeView.setBackground(this.currentBackgroundState.drawable);
                    } else {
                        this.collegeView.setBackgroundResourceMode(1);
                        if (this.currentBackgroundState.drawable != null) {
                            this.collegeView.setBackground(this.currentBackgroundState.drawable);
                        } else {
                            this.collegeView.setBackgroundResource(this.currentBackgroundState.drawableId);
                        }
                    }
                    slideDownSaveView();
                    showDownFunction();
                    this.currentMode = ToolType.NONE;
                    return;
                case PIECE:
                    slideDown(this.rvPieceControl);
                    slideUp(this.mRvTools);
                    slideDownSaveView();
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.rvPieceControl.getLayoutParams();
                    layoutParams.bottomMargin = 0;
                    this.rvPieceControl.setLayoutParams(layoutParams);
                    this.currentMode = ToolType.NONE;
                    this.collegeView.setHandlingPiece(null);
                    this.collegeView.setPreviousHandlingPiece(null);
                    this.collegeView.invalidate();
                    this.currentMode = ToolType.NONE;
                    return;
                case NONE:
                    isSaveDialog = false;
                    saveDiscardDialog(isSaveDialog);
                    return;
                default:
                    super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(CollegeLayout collegeLayout2, int i) {
        CollegeLayout parse = CollegeLayoutParser.parse(collegeLayout2.generateInfo());
        collegeLayout2.setRadian(this.collegeView.getPieceRadian());
        collegeLayout2.setPadding(this.collegeView.getPiecePadding());
        this.collegeView.updateLayout(parse);
    }

    @Override
    public void onNewAspectRatioSelected(AspectRatio aspectRatio) {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int[] calculateWidthAndHeight = calculateWidthAndHeight(aspectRatio, point);
        this.collegeView.setLayoutParams(new ConstraintLayout.LayoutParams(calculateWidthAndHeight[0], calculateWidthAndHeight[1]));
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.wrapPuzzleView);
        constraintSet.connect(this.collegeView.getId(), 3, this.wrapPuzzleView.getId(), 3, 0);
        constraintSet.connect(this.collegeView.getId(), 1, this.wrapPuzzleView.getId(), 1, 0);
        constraintSet.connect(this.collegeView.getId(), 4, this.wrapPuzzleView.getId(), 4, 0);
        constraintSet.connect(this.collegeView.getId(), 2, this.wrapPuzzleView.getId(), 2, 0);
        constraintSet.applyTo(this.wrapPuzzleView);
        this.collegeView.setAspectRatio(aspectRatio);
    }

    private int[] calculateWidthAndHeight(AspectRatio aspectRatio, Point point) {
        int height = this.wrapPuzzleView.getHeight();
        if (aspectRatio.getHeight() > aspectRatio.getWidth()) {
            int ratio = (int) (aspectRatio.getRatio() * ((float) height));
            if (ratio < point.x) {
                return new int[]{ratio, height};
            }
            return new int[]{point.x, (int) (((float) point.x) / aspectRatio.getRatio())};
        }
        int ratio2 = (int) (((float) point.x) / aspectRatio.getRatio());
        if (ratio2 > height) {
            return new int[]{(int) (((float) height) * aspectRatio.getRatio()), height};
        }
        return new int[]{point.x, ratio2};
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onBackgroundSelected(final CollegeBGAdapter.SquareView squareView) {
        if (squareView.isColor) {
            this.collegeView.setBackgroundColor(squareView.drawableId);
            this.collegeView.setBackgroundResourceMode(0);
        } else if (squareView.drawable != null) {
            this.collegeView.setBackgroundResourceMode(2);
            new AsyncTask<Void, Bitmap, Bitmap>() {

                public void onPreExecute() {
                    showLoading(true);
                }


                public Bitmap doInBackground(Void... voidArr) {
                    return UtilsFilter.getBlurImageFromBitmap(((BitmapDrawable) squareView.drawable).getBitmap(), 5.0f);
                }


                public void onPostExecute(Bitmap bitmap) {
                    showLoading(false);
                    collegeView.setBackground(new BitmapDrawable(getResources(), bitmap));
                }
            }.execute();
        } else {
            this.collegeView.setBackgroundResource(squareView.drawableId);
            this.collegeView.setBackgroundResourceMode(1);
        }
    }

    public void onFilterSelected(String str) {
        new LoadBitmapWithFilter().execute(new String[]{str});
    }


    public void finishCrop(Bitmap bitmap) {
        this.collegeView.replace(bitmap, "");
    }

    public void onSaveFilter(Bitmap bitmap) {
        this.collegeView.replace(bitmap, "");
    }

    @Override
    public void onPieceFuncSelected(ToolType toolType) {
        switch (toolType) {
            case REPLACE_IMG:
                singleImagePicker.launch("image/*");
                return;
            case H_FLIP:
                this.collegeView.flipHorizontally();
                return;
            case V_FLIP:
                this.collegeView.flipVertically();
                return;
            case ROTATE:
                this.collegeView.rotate(90.0f);
                return;
            case CROP:
                PicsartCropDialogFragment.show(this, this, ((BitmapDrawable) this.collegeView.getHandlingPiece().getDrawable()).getBitmap());
                return;
            case FILTER:
                new LoadFilterBitmapForCurrentPiece().execute();

        }
    }

    @Override
    public void getsticker() {
        collegeView.addSticker(new DrawableSticker(new BitmapDrawable(getResources(), AppUtils.Sticker_bitmap)));
    }

    class LoadFilterBitmap extends AsyncTask<Void, Void, Void> {
        LoadFilterBitmap() {
        }


        public void onPreExecute() {
            showLoading(true);
        }


        @SuppressLint("WrongThread")
        public Void doInBackground(Void... voidArr) {
            lstBitmapWithFilter.clear();
            lstBitmapWithFilter.addAll(UtilsFilter.getLstBitmapWithFilter(ThumbnailUtils.extractThumbnail(((BitmapDrawable) collegeView.getCollegePieces().get(0).getDrawable()).getBitmap(), 100, 100)));
            return null;
        }


        public void onPostExecute(Void voidR) {
            rvFilterView.setAdapter(new AdapterFilterView(lstBitmapWithFilter, CollageViewActivity.this, CollageViewActivity.this, Arrays.asList(UtilsFilter.EFFECT_CONFIGS)));
            slideDown(mRvTools);
            slideUp(filterLayout);
            showLoading(false);
            collegeView.setLocked(false);
            collegeView.setTouchEnable(false);
        }
    }

    class LoadFilterBitmapForCurrentPiece extends AsyncTask<Void, List<Bitmap>, List<Bitmap>> {
        LoadFilterBitmapForCurrentPiece() {
        }


        public void onPreExecute() {
            showLoading(true);
        }


        @SuppressLint("WrongThread")
        public List<Bitmap> doInBackground(Void... voidArr) {
            return UtilsFilter.getLstBitmapWithFilter(ThumbnailUtils.extractThumbnail(((BitmapDrawable) collegeView.getHandlingPiece().getDrawable()).getBitmap(), 100, 100));
        }


        public void onPostExecute(List<Bitmap> list) {
            showLoading(false);
            if (collegeView.getHandlingPiece() != null) {
                FilterDialogFragment.show(CollageViewActivity.this, CollageViewActivity.this, ((BitmapDrawable) collegeView.getHandlingPiece().getDrawable()).getBitmap(), list);
            }
        }
    }

    class LoadBitmapWithFilter extends AsyncTask<String, List<Bitmap>, List<Bitmap>> {
        LoadBitmapWithFilter() {
        }


        public void onPreExecute() {
            showLoading(true);
        }


        public List<Bitmap> doInBackground(String... strArr) {
            ArrayList arrayList = new ArrayList();
            for (Drawable drawable : lstOriginalDrawable) {
                arrayList.add(UtilsFilter.getBitmapWithFilter(((BitmapDrawable) drawable).getBitmap(), strArr[0]));
            }
            return arrayList;
        }


        public void onPostExecute(List<Bitmap> list) {
            for (int i = 0; i < list.size(); i++) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), list.get(i));
                bitmapDrawable.setAntiAlias(true);
                bitmapDrawable.setFilterBitmap(true);
                collegeView.getCollegePieces().get(i).setDrawable(bitmapDrawable);
            }
            collegeView.invalidate();
            showLoading(false);
        }
    }

    class SavePuzzleAsFile extends AsyncTask<Bitmap, String, String> {
        SavePuzzleAsFile() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public String doInBackground(Bitmap... bitmapArr) {
            Bitmap bitmap = bitmapArr[0];
            Bitmap bitmap2 = bitmapArr[1];
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            canvas.drawBitmap(bitmap, null, new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight()), null);
            canvas.drawBitmap(bitmap2, null, new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight()), null);
            bitmap.recycle();
            bitmap2.recycle();
            File saveBitmapAsFile = FileUtils.saveBitmapAsFile(createBitmap);
            if (saveBitmapAsFile == null) {
                return null;
            }
            try {
                MediaScannerConnection.scanFile(getApplicationContext(), new String[]{saveBitmapAsFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String str, Uri uri) {
                    }
                });
                createBitmap.recycle();
                return saveBitmapAsFile.getAbsolutePath();
            } catch (Exception e) {
                createBitmap.recycle();
                return null;
            } catch (Throwable th) {
                createBitmap.recycle();
                throw th;
            }
        }

        @Override
        public void onPostExecute(String str) {
            showLoading(false);
            Intent intent = new Intent(CollageViewActivity.this, SaveShare_ImageActivity.class);
            intent.putExtra("path", str);
            startActivity(intent);
        }
    }

    public void saveDiscardDialog(boolean isSaveDialog) {
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);

        if (isSaveDialog) {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(R.string.dialog_save_text);
        } else {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.dialog_discard_text));
        }

        builder = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            if (alertDialog.isShowing())
                alertDialog.dismiss();
            if (isSaveDialog) {
                collegeView.setHandlingSticker(null);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                collegeView.setLocked(true);
                collegeView.setTouchEnable(true);
                currentMode = ToolType.NONE;
                sticker_grid_fragment_container.setVisibility(View.GONE);

                Bitmap createBitmap = FileUtils.createBitmap(collegeView, 1920);
                Bitmap createBitmap2 = collegeView.createBitmap();
                new SavePuzzleAsFile().execute(new Bitmap[]{createBitmap, createBitmap2});
            } else {
                currentMode = null;
                finish();
            }
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            collegeView.setLocked(false);
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    public void Stickselect() {
        FragmentManager fm = getSupportFragmentManager();
        stickFragment = (StickFragment) fm.findFragmentByTag("mytag");
        if (this.stickFragment == null) {
            this.stickFragment = new StickFragment();
            Bundle bundle = new Bundle();
            bundle.putString("tabposition", "0");
            bundle.putString("hex", "no");
            this.stickFragment.setArguments(bundle);
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.sticker_grid_fragment_container, stickFragment);
            ft.addToBackStack(null);
            ft.commit();
            this.stickFragment.setBgSelectListner(creatStickSelectListner());
        } else {
            this.stickFragment.setBgSelectListner(creatStickSelectListner());
            getSupportFragmentManager().beginTransaction().show(this.stickFragment).commit();
        }
    }

    public void replaceCurrentPic(String str) {
        new loadBitmapFromUri().execute(new String[]{str});
    }

    class loadBitmapFromUri extends AsyncTask<String, Bitmap, Bitmap> {

        public void onPreExecute() {
            showLoading(true);
        }

        public Bitmap doInBackground(String... strArr) {
            try {
                Uri fromFile = Uri.fromFile(new File(strArr[0]));
                Bitmap rotateBitmap = SystemUtils.rotateBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), fromFile), new ExifInterface(
                        getContentResolver().openInputStream(fromFile)).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL));
                float width = (float) rotateBitmap.getWidth();
                float height = (float) rotateBitmap.getHeight();
                float max = Math.max(width / 1280.0f, height / 1280.0f);
                return max > 1.0f ? Bitmap.createScaledBitmap(rotateBitmap, (int) (width / max), (int) (height / max), false) : rotateBitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void onPostExecute(Bitmap bitmap) {
            showLoading(false);
            collegeView.replace(bitmap, "");
        }
    }

    StickFragment.BGSelectListener creatStickSelectListner() {
        if (stickbgSlect == null) {
            stickbgSlect = () -> getSupportFragmentManager().beginTransaction().hide(stickFragment).commit();
        }
        return this.stickbgSlect;
    }

}