package picsarc.picspro.picslab.photocollageeditor.features.college.layout.slant;


import picsarc.picspro.picslab.photocollageeditor.features.college.Line;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeLayout;
import picsarc.picspro.picslab.photocollageeditor.features.college.slant.SlantCollegeLayout;

public class OneSlantLayout extends NumberSlantLayout {
    public int getThemeCount() {
        return 4;
    }

    public OneSlantLayout(SlantCollegeLayout slantPuzzleLayout, boolean z) {
        super(slantPuzzleLayout, z);
    }

    public OneSlantLayout(int i) {
        super(i);
    }

    public void layout() {
        switch (this.theme) {
            case 0:
                addLine(0, Line.Direction.HORIZONTAL, 0.56f, 0.44f);
                return;
            case 1:
                addLine(0, Line.Direction.VERTICAL, 0.56f, 0.44f);
                return;
            case 2:
                addCross(0, 0.56f, 0.44f, 0.56f, 0.44f);
                return;
            case 3:
                cutArea(0, 1, 2);
                return;
            default:
        }
    }

    public CollegeLayout clone(CollegeLayout collegeLayout) {
        return new OneSlantLayout((SlantCollegeLayout) collegeLayout, true);
    }
}
