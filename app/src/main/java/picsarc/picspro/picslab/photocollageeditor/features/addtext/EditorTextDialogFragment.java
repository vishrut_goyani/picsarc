package picsarc.picspro.picslab.photocollageeditor.features.addtext;

import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.BitmapShader;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.features.CarouselPicker;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.adapter.AdapterFonts;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.adapter.AdapterShadow;
import picsarc.picspro.picslab.photocollageeditor.utils.SystemUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsFont;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsSharePreference;

public class EditorTextDialogFragment extends DialogFragment implements View.OnClickListener, AdapterFonts.ItemClickListener, AdapterShadow.ShadowItemClickListener {
    public static final String EXTRA_COLOR_CODE = "extra_color_code";
    public static final String EXTRA_INPUT_TEXT = "extra_input_text";
    public static final String TAG = "TextEditorDialogFrag";
    LinearLayout addTextBottomToolbar;

    public AddTextProperties addTextProperties;
    ImageView arrowBackgroundColorDown;
    ImageView arrowTextTexture;
    SeekBar backgroundBorder;
    CarouselPicker backgroundColorCarousel;
    AppCompatCheckBox backgroundFullScreen;
    SeekBar backgroundHeight;
    SeekBar backgroundTransparent;
    SeekBar backgroundWidth;
    ImageView changeAlign;
    ImageView changeColor;
    ScrollView changeColorLayout;
    ImageView changeFont;
    ScrollView changeFontLayout;
    //    ImageView colorArrow;
    int selectedColorBG;

    //    public List<CarouselPicker.PickerItem> colorItems;
    CarouselPicker colorPicker;
    private AdapterFonts adapterFonts;
    View highlightBackgroundColor;
    View highlightColor;
//    View highlightTextTexture;
    RecyclerView lstFonts;
    RecyclerView lstShadows;
    CustomEditText mAddTextEditText;
    private InputMethodManager mInputMethodManager;
    TextView previewText;
    ImageView saveChange;
    private AdapterShadow adapterShadow;
    ImageView showKeyboard;
    SwitchCompat switchBackgroundTexture;
    private TextEditor textEditor;
    private List<ImageView> textFunctions;
    SeekBar textSize;
    Button btn_color_text, btn_color_bg;

    public List<CarouselPicker.PickerItem> textTextureItems;
    CarouselPicker textTexturePicker;
    SeekBar textTransparent;

    public interface TextEditor {
        void onBackButton();

        void onDone(AddTextProperties addTextProperties);
    }

    public void initView(View view) {
        this.mAddTextEditText = view.findViewById(R.id.add_text_edit_text);
        this.showKeyboard = view.findViewById(R.id.showKeyboard);
        this.changeFont = view.findViewById(R.id.changeFont);
        this.changeColor = view.findViewById(R.id.changeColor);
        this.changeAlign = view.findViewById(R.id.changeAlign);
        this.saveChange = view.findViewById(R.id.saveChange);
        this.changeFontLayout = view.findViewById(R.id.change_font_layout);
        this.addTextBottomToolbar = view.findViewById(R.id.add_text_toolbar);
        this.lstFonts = view.findViewById(R.id.fonts);
        this.lstShadows = view.findViewById(R.id.shadows);
        this.changeColorLayout = view.findViewById(R.id.changeColorLayout);
        this.colorPicker = view.findViewById(R.id.colorCarousel);
        this.textTexturePicker = view.findViewById(R.id.textTextureSlider);
        this.arrowTextTexture = view.findViewById(R.id.arrow_text_texture);
//        this.colorArrow = view.findViewById(R.id.arrow_color_down);
        this.highlightColor = view.findViewById(R.id.highlightColor);
//        this.highlightTextTexture = view.findViewById(R.id.highlightTextTexture);
        this.textTransparent = view.findViewById(R.id.textTransparent);
        this.previewText = view.findViewById(R.id.previewEffectText);
        this.switchBackgroundTexture = view.findViewById(R.id.switchBackgroundTexture);
        this.arrowBackgroundColorDown = view.findViewById(R.id.arrowBackgroundColorDown);
        this.highlightBackgroundColor = view.findViewById(R.id.highlightBackgroundColor);
        this.backgroundColorCarousel = view.findViewById(R.id.backgroundColorCarousel);
        this.backgroundWidth = view.findViewById(R.id.backgroundWidth);
        this.backgroundHeight = view.findViewById(R.id.backgroundHeight);
        this.backgroundFullScreen = view.findViewById(R.id.backgroundFullScreen);
        this.backgroundTransparent = view.findViewById(R.id.backgroundTransparent);
        this.textSize = view.findViewById(R.id.textSize);
        this.backgroundBorder = view.findViewById(R.id.backgroundBorderRadius);
        this.btn_color_text = view.findViewById(R.id.btn_color_text);
        this.btn_color_bg = view.findViewById(R.id.btn_color_bg);

        btn_color_text.setOnClickListener(v -> ColorPickerDialogBuilder
                .with(getActivity(), R.style.Widget_Discard_DialogTheme)
                .setTitle("Choose color")
                .initialColor(getResources().getColor(R.color.white))
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(12)
                .showColorPreview(true)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        selectedColorBG = selectedColor;
                    }
                })
                .setPositiveButton("ok", (dialog, selectedColor, allColors) -> {
                    btn_color_text.setBackgroundColor(selectedColorBG);
                    previewText.getPaint().setShader(null);
                    addTextProperties.setTextShader(null);
                    previewText.setTextColor(selectedColorBG);
                    addTextProperties.setTextColorIndex(Math.round(selectedColorBG));
                    addTextProperties.setTextColor(selectedColorBG);
                })
                .setNegativeButton("cancel", (dialog, which) -> {
                    dialog.dismiss();
                })
                .build()
                .show());

        btn_color_bg.setOnClickListener(v -> {
            if (!switchBackgroundTexture.isChecked()) {
                Toast.makeText(getContext(), "Enable the Background first", Toast.LENGTH_SHORT).show();
            } else {
                ColorPickerDialogBuilder
                        .with(getActivity(), R.style.Widget_Discard_DialogTheme)
                        .setTitle("Choose color")
                        .initialColor(getResources().getColor(R.color.white))
                        .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                        .density(12)
                        .showColorPreview(true)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                selectedColorBG = selectedColor;
                            }
                        })
                        .setPositiveButton("ok", (dialog, selectedColor, allColors) -> {
                            btn_color_bg.setBackgroundColor(selectedColorBG);
                            if (switchBackgroundTexture.isChecked()) {
                                int parseColor = selectedColorBG;
                                int red = Color.red(parseColor);
                                int green = Color.green(parseColor);
                                int blue = Color.blue(parseColor);
                                GradientDrawable gradientDrawable = new GradientDrawable();
                                gradientDrawable.setColor(Color.argb(addTextProperties.getBackgroundAlpha(), red, green, blue));
                                gradientDrawable.setCornerRadius((float) SystemUtils.dpToPx(Objects.requireNonNull(getContext()), addTextProperties.getBackgroundBorder()));
                                previewText.setBackground(gradientDrawable);
                                addTextProperties.setBackgroundColor(parseColor);
                                addTextProperties.setBackgroundColorIndex(Math.round(parseColor));
                                backgroundBorder.setEnabled(true);
                            }
                        })
                        .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                        .build()
                        .show();
            }
        });
    }

    @Override
    public void onItemClick(int i) {
        UtilsFont.setFontByName(Objects.requireNonNull(getContext()), this.previewText, UtilsFont.getListFonts().get(i));
        this.addTextProperties.setFontName(UtilsFont.getListFonts().get(i));
        this.addTextProperties.setFontIndex(i);
    }

    @Override
    public void onShadowItemClick(int i) {
        AddTextProperties.TextShadow textShadow = AddTextProperties.getLstTextShadow().get(i);
        this.previewText.setShadowLayer((float) textShadow.getRadius(), (float) textShadow.getDx(), (float) textShadow.getDy(), textShadow.getColorShadow());
        this.previewText.invalidate();
        this.addTextProperties.setTextShadow(textShadow);
        this.addTextProperties.setTextShadowIndex(i);
    }

    public static EditorTextDialogFragment show(@NonNull AppCompatActivity appCompatActivity, @NonNull String str, @ColorInt int i) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_INPUT_TEXT, str);
        bundle.putInt(EXTRA_COLOR_CODE, i);
        EditorTextDialogFragment editorTextDialogFragment = new EditorTextDialogFragment();
        editorTextDialogFragment.setArguments(bundle);
        editorTextDialogFragment.show(appCompatActivity.getSupportFragmentManager(), TAG);
        return editorTextDialogFragment;
    }

    public static EditorTextDialogFragment show(@NonNull AppCompatActivity appCompatActivity, AddTextProperties addTextProperties2) {
        EditorTextDialogFragment editorTextDialogFragment = new EditorTextDialogFragment();
        editorTextDialogFragment.setAddTextProperties(addTextProperties2);
        editorTextDialogFragment.show(appCompatActivity.getSupportFragmentManager(), TAG);
        return editorTextDialogFragment;
    }

    public static EditorTextDialogFragment show(@NonNull AppCompatActivity appCompatActivity) {
        return show(appCompatActivity, "Test", ContextCompat.getColor(appCompatActivity, R.color.white));
    }

    public void setAddTextProperties(AddTextProperties addTextProperties2) {
        this.addTextProperties = addTextProperties2;
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setFlags(1024, 1024);
        return layoutInflater.inflate(R.layout.add_text_dialog, viewGroup, false);
    }

    public void dismissAndShowSticker() {
        if (this.textEditor != null) {
            this.textEditor.onBackButton();
        }
        dismiss();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        initView(view);
        if (this.addTextProperties == null) {
            this.addTextProperties = AddTextProperties.getDefaultProperties();
        }
        this.mAddTextEditText.setDialogFragment(this);
        initAddTextLayout();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        this.mInputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        setDefaultStyleForEdittext();
        this.mInputMethodManager.toggleSoftInput(InputMethodManager.RESULT_SHOWN, 0);
        highlightFunction(this.showKeyboard);
        this.lstFonts.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        this.adapterFonts = new AdapterFonts(getContext(), UtilsFont.getListFonts());
        this.adapterFonts.setClickListener(this);
        this.lstFonts.setAdapter(this.adapterFonts);
        this.lstShadows.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        this.adapterShadow = new AdapterShadow(getContext(), AddTextProperties.getLstTextShadow());
        this.adapterShadow.setClickListener(this);
        this.lstShadows.setAdapter(this.adapterShadow);
//        this.colorPicker.setAdapter(new CarouselPicker.CarouselViewAdapter(getContext(), this.colorItems, 0));
//        this.colorPicker.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            public void onPageScrollStateChanged(int i) {
//            }
//
//            public void onPageSelected(int i) {
//            }
//
//            public void onPageScrolled(int i, float f, int i2) {
//                if (f > 0.0f) {
//                    if (colorArrow.getVisibility() == View.INVISIBLE) {
//                        colorArrow.setVisibility(View.VISIBLE);
//                        highlightColor.setVisibility(View.VISIBLE);
//                        highlightTextTexture.setVisibility(View.GONE);
//                    }
//                    previewText.getPaint().setShader(null);
//                    int i3 = -1;
//                    float f2 = ((float) i) + f;
//                    if (Math.round(f2) < colorItems.size()) {
//                        i3 = Color.parseColor((colorItems.get(Math.round(f2))).getColor());
//                    }
//                    previewText.setTextColor(i3);
//                    addTextProperties.setTextColorIndex(Math.round(f2));
//                    addTextProperties.setTextColor(i3);
//                    addTextProperties.setTextShader(null);
//                }
//            }
//        });
        textTextureItems = new ArrayList<>();
        textTextureItems = getTextTextures();
        textTexturePicker.setAdapter(new CarouselPicker.CarouselViewAdapter(getContext(), textTextureItems, 0));
        textTexturePicker.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int i) {
            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrolled(int i, float positionOffset, int positionOffsetPixels) {
                if (positionOffset > 0.0f) {
                    if (arrowTextTexture.getVisibility() == View.INVISIBLE) {
                        arrowTextTexture.setVisibility(View.VISIBLE);
//                        highlightTextTexture.setVisibility(View.VISIBLE);
//                        colorArrow.setVisibility(View.INVISIBLE);
                        highlightColor.setVisibility(View.GONE);
                    }
                    float f2 = ((float) i) + positionOffset;
                    BitmapShader bitmapShader = new BitmapShader((textTextureItems.get(Math.round(f2))).getBitmap(), Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
                    previewText.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    previewText.getPaint().setShader(bitmapShader);
                    addTextProperties.setTextShader(bitmapShader);
                    addTextProperties.setTextShaderIndex(Math.round(f2));
                }
            }
        });
        this.textTransparent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                int i2 = 255 - i;
                addTextProperties.setTextAlpha(i2);
                previewText.setTextColor(Color.argb(i2, Color.red(addTextProperties.getTextColor()), Color.green(addTextProperties.getTextColor()), Color.blue(addTextProperties.getTextColor())));
            }
        });
        this.mAddTextEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                previewText.setText(charSequence.toString());
                addTextProperties.setText(charSequence.toString());
            }
        });
        this.switchBackgroundTexture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (!z) {
                    addTextProperties.setShowBackground(false);
                    previewText.setBackgroundResource(0);
                    previewText.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                } else if (switchBackgroundTexture.isPressed() || addTextProperties.isShowBackground()) {
                    addTextProperties.setShowBackground(true);
                    initPreviewText();
                } else {
                    switchBackgroundTexture.setChecked(false);
                    addTextProperties.setShowBackground(false);
                    initPreviewText();
                }
            }
        });
//        this.backgroundColorCarousel.setAdapter(new CarouselPicker.CarouselViewAdapter(getContext(), this.colorItems, 0));
//        this.backgroundColorCarousel.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            public void onPageScrollStateChanged(int i) {
//            }
//
//            public void onPageSelected(int i) {
//            }
//
//            public void onPageScrolled(int i, float f, int i2) {
//                if (f > 0.0f) {
//                    int i3 = 0;
//                    if (arrowBackgroundColorDown.getVisibility() == View.INVISIBLE) {
//                        arrowBackgroundColorDown.setVisibility(View.VISIBLE);
//                        highlightBackgroundColor.setVisibility(View.VISIBLE);
//                    }
//                    addTextProperties.setShowBackground(true);
//                    if (!switchBackgroundTexture.isChecked()) {
//                        switchBackgroundTexture.setChecked(true);
//                    }
//                    float f2 = ((float) i) + f;
//                    int round = Math.round(f2);
//                    if (round >= colorItems.size()) {
//                        i3 = colorItems.size() - 1;
//                    } else if (round >= 0) {
//                        i3 = round;
//                    }
//                    int parseColor = Color.parseColor((colorItems.get(i3)).getColor());
//                    int red = Color.red(parseColor);
//                    int green = Color.green(parseColor);
//                    int blue = Color.blue(parseColor);
//                    GradientDrawable gradientDrawable = new GradientDrawable();
//                    gradientDrawable.setColor(Color.argb(addTextProperties.getBackgroundAlpha(), red, green, blue));
//                    gradientDrawable.setCornerRadius((float) SystemUtils.dpToPx(Objects.requireNonNull(getContext()), addTextProperties.getBackgroundBorder()));
//                    previewText.setBackground(gradientDrawable);
//                    addTextProperties.setBackgroundColor(parseColor);
//                    addTextProperties.setBackgroundColorIndex(Math.round(f2));
//                    backgroundBorder.setEnabled(true);
//                }
//            }
//        });
        this.backgroundWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                previewText.setPadding(SystemUtils.dpToPx(Objects.requireNonNull(getContext()), i), previewText.getPaddingTop(), SystemUtils.dpToPx(getContext(), i), previewText.getPaddingBottom());
                addTextProperties.setPaddingWidth(i);
            }
        });
        this.backgroundHeight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                previewText.setPadding(previewText.getPaddingLeft(), SystemUtils.dpToPx(Objects.requireNonNull(getContext()), i), previewText.getPaddingRight(), SystemUtils.dpToPx(getContext(), i));
                addTextProperties.setPaddingHeight(i);
            }
        });
        this.backgroundFullScreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (z) {
                    previewText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                } else {
                    previewText.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                }
                addTextProperties.setFullScreen(z);
            }
        });
        this.backgroundTransparent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                addTextProperties.setBackgroundAlpha(255 - i);
                if (addTextProperties.isShowBackground()) {
                    int red = Color.red(addTextProperties.getBackgroundColor());
                    int green = Color.green(addTextProperties.getBackgroundColor());
                    int blue = Color.blue(addTextProperties.getBackgroundColor());
                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setColor(Color.argb(addTextProperties.getBackgroundAlpha(), red, green, blue));
                    gradientDrawable.setCornerRadius((float) SystemUtils.dpToPx(Objects.requireNonNull(getContext()), addTextProperties.getBackgroundBorder()));
                    previewText.setBackground(gradientDrawable);
                }
            }
        });
        this.textSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                int i2 = 15;
                if (i >= 15) {
                    i2 = i;
                }
                previewText.setTextSize((float) i2);
                addTextProperties.setTextSize(i2);
            }
        });
        this.backgroundBorder.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                addTextProperties.setBackgroundBorder(i);
                if (addTextProperties.isShowBackground()) {
                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setCornerRadius((float) SystemUtils.dpToPx(Objects.requireNonNull(getContext()), i));
                    gradientDrawable.setColor(Color.argb(addTextProperties.getBackgroundAlpha(), Color.red(addTextProperties.getBackgroundColor()), Color.green(addTextProperties.getBackgroundColor()), Color.blue(addTextProperties.getBackgroundColor())));
                    previewText.setBackground(gradientDrawable);
                }
            }
        });
        if (UtilsSharePreference.getHeightOfKeyboard(Objects.requireNonNull(getContext())) > 0) {
            updateAddTextBottomToolbarHeight(UtilsSharePreference.getHeightOfKeyboard(getContext()));
        }
        initPreviewText();
    }

    public void initPreviewText() {
        if (this.addTextProperties.isFullScreen()) {
            this.previewText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        }
        if (this.addTextProperties.isShowBackground()) {
            if (this.addTextProperties.getBackgroundColor() != 0) {
                this.previewText.setBackgroundColor(this.addTextProperties.getBackgroundColor());
            }
            if (this.addTextProperties.getBackgroundAlpha() < 255) {
                this.previewText.setBackgroundColor(Color.argb(this.addTextProperties.getBackgroundAlpha(), Color.red(this.addTextProperties.getBackgroundColor()), Color.green(this.addTextProperties.getBackgroundColor()), Color.blue(this.addTextProperties.getBackgroundColor())));
            }
            if (this.addTextProperties.getBackgroundBorder() > 0) {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius((float) SystemUtils.dpToPx(Objects.requireNonNull(getContext()), this.addTextProperties.getBackgroundBorder()));
                gradientDrawable.setColor(Color.argb(this.addTextProperties.getBackgroundAlpha(), Color.red(this.addTextProperties.getBackgroundColor()), Color.green(this.addTextProperties.getBackgroundColor()), Color.blue(this.addTextProperties.getBackgroundColor())));
                this.previewText.setBackground(gradientDrawable);
            }
        }
        if (this.addTextProperties.getPaddingHeight() > 0) {
            this.previewText.setPadding(this.previewText.getPaddingLeft(), this.addTextProperties.getPaddingHeight(), this.previewText.getPaddingRight(), this.addTextProperties.getPaddingHeight());
            this.backgroundHeight.setProgress(this.addTextProperties.getPaddingHeight());
        }
        if (this.addTextProperties.getPaddingWidth() > 0) {
            this.previewText.setPadding(this.addTextProperties.getPaddingWidth(), this.previewText.getPaddingTop(), this.addTextProperties.getPaddingWidth(), this.previewText.getPaddingBottom());
            this.backgroundWidth.setProgress(this.addTextProperties.getPaddingWidth());
        }
        if (this.addTextProperties.getText() != null) {
            this.previewText.setText(this.addTextProperties.getText());
            this.mAddTextEditText.setText(this.addTextProperties.getText());
        }
        if (this.addTextProperties.getTextShader() != null) {
            this.previewText.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            this.previewText.getPaint().setShader(this.addTextProperties.getTextShader());
        }
        if (this.addTextProperties.getTextAlign() == 4) {
            this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_center));
        } else if (this.addTextProperties.getTextAlign() == 3) {
            this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_right));
        } else if (this.addTextProperties.getTextAlign() == 2) {
            this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_left));
        }
        this.previewText.setPadding(SystemUtils.dpToPx(getContext(), this.addTextProperties.getPaddingWidth()), this.previewText.getPaddingTop(), SystemUtils.dpToPx(getContext(), this.addTextProperties.getPaddingWidth()), this.previewText.getPaddingBottom());
        this.previewText.setTextColor(this.addTextProperties.getTextColor());
        this.previewText.setTextAlignment(this.addTextProperties.getTextAlign());
        this.previewText.setTextSize((float) this.addTextProperties.getTextSize());
        UtilsFont.setFontByName(getContext(), this.previewText, this.addTextProperties.getFontName());
        if (this.addTextProperties.getTextShadow() != null) {
            AddTextProperties.TextShadow textShadow = this.addTextProperties.getTextShadow();
            this.previewText.setShadowLayer((float) textShadow.getRadius(), (float) textShadow.getDx(), (float) textShadow.getDy(), textShadow.getColorShadow());
        }
        this.previewText.invalidate();
    }

    private void setDefaultStyleForEdittext() {
        this.mAddTextEditText.requestFocus();
        this.mAddTextEditText.setTextSize(20.0f);
        this.mAddTextEditText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        this.mAddTextEditText.setTextColor(Color.parseColor("#424949"));
    }

    private void initAddTextLayout() {
        textFunctions = getTextFunctions();
        showKeyboard.setOnClickListener(this);
        changeFont.setOnClickListener(this);
        changeColor.setOnClickListener(this);
        changeAlign.setOnClickListener(this);
        saveChange.setOnClickListener(this);
        changeFontLayout.setVisibility(View.GONE);
        changeColorLayout.setVisibility(View.GONE);

//        highlightTextTexture.setVisibility(View.GONE);
        backgroundWidth.setProgress(addTextProperties.getPaddingWidth());
//        colorItems = getColorItems();
//        textTextureItems = getTextTextures();
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewCompat.setOnApplyWindowInsetsListener(getDialog().getWindow().getDecorView(), (view, windowInsetsCompat) -> ViewCompat.onApplyWindowInsets(
                getDialog().getWindow().getDecorView(),
                windowInsetsCompat.inset(windowInsetsCompat.getSystemWindowInsetLeft(), 0, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom())));

    }

    public void updateAddTextBottomToolbarHeight(final int i) {
        new Handler().post(new Runnable() {
            public void run() {
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) addTextBottomToolbar.getLayoutParams();
                layoutParams.bottomMargin = i;
                addTextBottomToolbar.setLayoutParams(layoutParams);
                addTextBottomToolbar.invalidate();
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) changeFontLayout.getLayoutParams();
                layoutParams2.height = i;
                changeFontLayout.setLayoutParams(layoutParams2);
                changeFontLayout.invalidate();
                ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) changeColorLayout.getLayoutParams();
                layoutParams3.height = i;
                changeColorLayout.setLayoutParams(layoutParams3);
                changeColorLayout.invalidate();

            }
        });
    }

    public void setOnTextEditorListener(TextEditor textEditor2) {
        this.textEditor = textEditor2;
    }

    private void highlightFunction(ImageView imageView) {
        for (ImageView next : this.textFunctions) {
            if (next == imageView) {
                imageView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.highlight));
            } else {
                next.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.fake_highlight));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.showKeyboard:
                toggleTextEditEditable(true);
                mAddTextEditText.setVisibility(View.VISIBLE);
                mAddTextEditText.requestFocus();
                highlightFunction(showKeyboard);
                changeFontLayout.setVisibility(View.GONE);
                changeColorLayout.setVisibility(View.GONE);
                addTextBottomToolbar.invalidate();
                mInputMethodManager.toggleSoftInput(InputMethodManager.RESULT_SHOWN, 0);
                break;
            case R.id.changeFont:
                mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                changeFontLayout.setVisibility(View.VISIBLE);
                changeColorLayout.setVisibility(View.GONE);
                mAddTextEditText.setVisibility(View.GONE);
                toggleTextEditEditable(false);
                highlightFunction(changeFont);
                textSize.setProgress(addTextProperties.getTextSize());
                adapterFonts.setSelectedItem(addTextProperties.getFontIndex());
                adapterShadow.setSelectedItem(addTextProperties.getTextShadowIndex());
                break;
            case R.id.changeColor:
                mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                changeColorLayout.setVisibility(View.VISIBLE);
                toggleTextEditEditable(false);
                highlightFunction(changeColor);
                changeFontLayout.setVisibility(View.GONE);
                mAddTextEditText.setVisibility(View.GONE);
                colorPicker.setCurrentItem(addTextProperties.getTextColorIndex());
                textTexturePicker.setCurrentItem(addTextProperties.getTextShaderIndex());
                textTransparent.setProgress(255 - addTextProperties.getTextAlpha());
                switchBackgroundTexture.setChecked(addTextProperties.isShowBackground());
                backgroundColorCarousel.setCurrentItem(addTextProperties.getBackgroundColorIndex());
                backgroundTransparent.setProgress(255 - addTextProperties.getBackgroundAlpha());
                backgroundFullScreen.setChecked(addTextProperties.isFullScreen());
                backgroundBorder.setProgress(addTextProperties.getBackgroundBorder());
                backgroundWidth.setProgress(addTextProperties.getPaddingWidth());
                backgroundHeight.setProgress(addTextProperties.getPaddingHeight());
                switchBackgroundTexture.setChecked(addTextProperties.isShowBackground());
                if (addTextProperties.getTextShader() != null && arrowTextTexture.getVisibility() == View.INVISIBLE) {
                    arrowTextTexture.setVisibility(View.VISIBLE);
//                    highlightTextTexture.setVisibility(View.VISIBLE);
//                    colorArrow.setVisibility(View.INVISIBLE);
                    highlightColor.setVisibility(View.GONE);
                    break;
                }
                break;
            case R.id.changeAlign:
                if (this.addTextProperties.getTextAlign() == 4) {
                    this.addTextProperties.setTextAlign(3);
                    this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_right));
                } else if (this.addTextProperties.getTextAlign() == 3) {
                    this.addTextProperties.setTextAlign(2);
                    this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_left));
                } else if (this.addTextProperties.getTextAlign() == 2) {
                    this.addTextProperties.setTextAlign(4);
                    this.changeAlign.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_alignment_center));
                }
                this.previewText.setTextAlignment(this.addTextProperties.getTextAlign());
                TextView textView = this.previewText;
                textView.setText(this.previewText.getText().toString().trim() + " ");
                this.previewText.setText(this.previewText.getText().toString().trim());
                break;
            case R.id.saveChange:
                if (addTextProperties.getText() == null || addTextProperties.getText().length() == 0) {
                    mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    textEditor.onBackButton();
                    dismiss();
                    break;
                }
                addTextProperties.setTextWidth(previewText.getMeasuredWidth());
                addTextProperties.setTextHeight(previewText.getMeasuredHeight());
                mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                textEditor.onDone(addTextProperties);
                dismiss();
                break;
            default:
                break;
        }
    }

    private void toggleTextEditEditable(boolean z) {
        this.mAddTextEditText.setFocusable(z);
        this.mAddTextEditText.setFocusableInTouchMode(z);
        this.mAddTextEditText.setClickable(z);
    }

    private List<ImageView> getTextFunctions() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.showKeyboard);
        arrayList.add(this.changeFont);
        arrayList.add(this.changeColor);
        arrayList.add(this.changeAlign);
        arrayList.add(this.saveChange);
        return arrayList;
    }

    public List<CarouselPicker.PickerItem> getTextTextures() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 15; i++) {
            try {
                AssetManager assets = getContext().getAssets();
                arrayList.add(new CarouselPicker.DrawableItem(Drawable.createFromStream(assets.open("text_texture/" + (i + 1) + ".jpg"), null)));
            } catch (Exception e) {
            }
        }
        return arrayList;
    }

    public List<CarouselPicker.PickerItem> getColorItems() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CarouselPicker.ColorItem("#f1948a"));
        arrayList.add(new CarouselPicker.ColorItem("#e74c3c"));
        arrayList.add(new CarouselPicker.ColorItem("#DC143C"));
        arrayList.add(new CarouselPicker.ColorItem("#FF0000"));
        arrayList.add(new CarouselPicker.ColorItem("#bb8fce"));
        arrayList.add(new CarouselPicker.ColorItem("#8e44ad"));
        arrayList.add(new CarouselPicker.ColorItem("#6c3483"));
        arrayList.add(new CarouselPicker.ColorItem("#FF00FF"));
        arrayList.add(new CarouselPicker.ColorItem("#3498db"));
        arrayList.add(new CarouselPicker.ColorItem("#2874a6"));
        arrayList.add(new CarouselPicker.ColorItem("#1b4f72"));
        arrayList.add(new CarouselPicker.ColorItem("#0000FF"));
        arrayList.add(new CarouselPicker.ColorItem("#73c6b6"));
        arrayList.add(new CarouselPicker.ColorItem("#16a085"));
        arrayList.add(new CarouselPicker.ColorItem("#117a65"));
        arrayList.add(new CarouselPicker.ColorItem("#0b5345"));
        arrayList.add(new CarouselPicker.ColorItem("#ffffff"));
        arrayList.add(new CarouselPicker.ColorItem("#d7dbdd"));
        arrayList.add(new CarouselPicker.ColorItem("#bdc3c7"));
        arrayList.add(new CarouselPicker.ColorItem("#909497"));
        arrayList.add(new CarouselPicker.ColorItem("#626567"));
        arrayList.add(new CarouselPicker.ColorItem("#000000"));
        arrayList.add(new CarouselPicker.ColorItem("#239b56"));
        arrayList.add(new CarouselPicker.ColorItem("#186a3b"));
        arrayList.add(new CarouselPicker.ColorItem("#f8c471"));
        arrayList.add(new CarouselPicker.ColorItem("#f39c12"));
        arrayList.add(new CarouselPicker.ColorItem("#FFA500"));
        arrayList.add(new CarouselPicker.ColorItem("#FFFF00"));
        arrayList.add(new CarouselPicker.ColorItem("#7e5109"));
        arrayList.add(new CarouselPicker.ColorItem("#e59866"));
        arrayList.add(new CarouselPicker.ColorItem("#d35400"));
        arrayList.add(new CarouselPicker.ColorItem("#a04000"));
        arrayList.add(new CarouselPicker.ColorItem("#6e2c00"));
        arrayList.add(new CarouselPicker.ColorItem("#808b96"));
        arrayList.add(new CarouselPicker.ColorItem("#2c3e50"));
        arrayList.add(new CarouselPicker.ColorItem("#212f3d"));
        arrayList.add(new CarouselPicker.ColorItem("#17202a"));
        return arrayList;
    }
}
