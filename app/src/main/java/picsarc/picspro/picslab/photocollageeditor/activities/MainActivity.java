package picsarc.picspro.picslab.photocollageeditor.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import dev.yuganshtyagi.smileyrating.SmileyRatingView;
import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.base.BaseActivity;
import picsarc.picspro.picslab.photocollageeditor.dialog.AppSettingsDialog;
import picsarc.picspro.picslab.photocollageeditor.nativead.PersonalAds;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.SystemUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsAds;
import picsarc.picspro.picslab.photocollageeditor.widgets.FeedbackSheetBuilder;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    public static final String KEY_DATA_RESULT = "KEY_DATA_RESULT";
    public static final String KEY_SELECTED_PHOTOS = "SELECTED_PHOTOS";
    private static final int CHECK_UPDATE_REQUEST_CODE = 369;
    AppUpdateManager appUpdateManager;

    private String photoPath;
    private Uri photoUri;

    ActivityResultLauncher<String> singleImagePicker
            = registerForActivityResult(new ActivityResultContracts.GetContent(),
            uri -> {
                String imagePath = SystemUtils.getRealPathFromUri(MainActivity.this, uri);
                Intent intent = new Intent(MainActivity.this, Image_Edit_Activity.class);
                intent.putExtra(KEY_SELECTED_PHOTOS, imagePath);
                startActivity(intent);
            });

    ActivityResultLauncher<String> multiImagePicker
            = registerForActivityResult(new ActivityResultContracts.GetMultipleContents(),
            uriList -> {
                ArrayList<String> pathList = new ArrayList<>();
                for (Uri uri : uriList) {
                    pathList.add(SystemUtils.getRealPathFromUri(MainActivity.this, uri));
                }
                Intent intent = new Intent(MainActivity.this, CollageViewActivity.class);
                intent.putStringArrayListExtra(KEY_DATA_RESULT, pathList);
                startActivity(intent);
            });

    ActivityResultLauncher<Uri> imageCapture = registerForActivityResult(
            new ActivityResultContracts.TakePicture(),
            result -> {
                if (result) {
                    Intent intent2 = new Intent(getApplicationContext(), Image_Edit_Activity.class);
                    intent2.putExtra(KEY_SELECTED_PHOTOS, photoPath);
                    startActivity(intent2);
                }
            });

    private DrawerLayout mDrawerLayout;
    ConstraintLayout nav_settings, rateApp, shareApp;

    FrameLayout adFrame;
    View view;
    BottomSheetDialog mDialog;

    FeedbackSheetBuilder feedbackSheetBuilder;
    SmileyRatingView smiley_view;
    AppCompatRatingBar ratingBar;
    TextView tv_submit, tv_maybe_later, rate_text;
    float rating;

    ImageView adsimg;
    TextView adsname;
    LinearLayout drawerads;
    String link;
    int count = 0;
    ArrayList<PersonalAds> adsList;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setImages();
            handler.postDelayed(this, 2000);
        }
    };

    private void setImages() {
        if (adsList.size() != 0) {
            Glide.with(this).load(adsList.get(count).getAds_icon()).into(adsimg);
            adsname.setText(adsList.get(count).getAds_name());
            link = adsList.get(count).getAds_url();
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            drawerads.startAnimation(shake);
            drawerads.setOnClickListener(view -> {
                try {
                    Uri uri = Uri.parse(link);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            count++;
            if (count == adsList.size()) {
                count = 0;
            }
        }
    }

    View.OnClickListener onClickListener = view -> {
        int id = view.getId();
        if (id == R.id.btnCamera) {
            Dexter.withContext(MainActivity.this).withPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        openCamera();
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                        AppSettingsDialog.showSettingDialog(MainActivity.this);
                    }
                }

                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                    permissionToken.continuePermissionRequest();
                }
            }).withErrorListener(dexterError -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show()).onSameThread().check();
        } else if (id == R.id.btnEdit) {
            Dexter.withContext(MainActivity.this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        singleImagePicker.launch("image/*");
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                        AppSettingsDialog.showSettingDialog(MainActivity.this);
                    }
                }

                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                    permissionToken.continuePermissionRequest();
                }
            }).withErrorListener(dexterError -> Toast.makeText(MainActivity.this, "Error occurred! ", Toast.LENGTH_SHORT).show()).onSameThread().check();
        } else if (id == R.id.btncreation) {
            Dexter.withContext(MainActivity.this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        startActivity(new Intent(MainActivity.this, MyCreationActivity.class));
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                        AppSettingsDialog.showSettingDialog(MainActivity.this);
                    }
                }

                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                    permissionToken.continuePermissionRequest();
                }
            }).withErrorListener(dexterError -> Toast.makeText(MainActivity.this, "Error occurred! ", Toast.LENGTH_SHORT).show()).onSameThread().check();
        } else if (id == R.id.btnCollege) {
            Dexter.withContext(MainActivity.this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                    if (multiplePermissionsReport.areAllPermissionsGranted()) {
                        multiImagePicker.launch("image/*");
                    }
                    if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                        AppSettingsDialog.showSettingDialog(MainActivity.this);
                    }
                }

                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                    permissionToken.continuePermissionRequest();
                }
            }).withErrorListener(dexterError -> Toast.makeText(MainActivity.this, "Error occurred! ", Toast.LENGTH_SHORT).show()).onSameThread().check();
        } else if (id == R.id.drawer_handle) {
            mDrawerLayout.openDrawer(GravityCompat.END);
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(bundle);
        makeFullScreen();
        getWindow().setNavigationBarColor(Color.parseColor("#424160"));
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnEdit).setOnClickListener(this.onClickListener);
        findViewById(R.id.btnCamera).setOnClickListener(this.onClickListener);
        findViewById(R.id.btnCollege).setOnClickListener(this.onClickListener);
        findViewById(R.id.btncreation).setOnClickListener(this.onClickListener);
        findViewById(R.id.drawer_handle).setOnClickListener(this.onClickListener);
        view = LayoutInflater.from(this).inflate(R.layout.app_exit_dialog, null);
        adFrame = view.findViewById(R.id.adFrame);
        mDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
        mDialog.requestWindowFeature(1);
        mDialog.setContentView(view);
        init();
        initPersonalAds();

        AppUtils.getInstance(MainActivity.this).setCount(AppUtils.getInstance(MainActivity.this).getCounts() + 1);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
        );
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        photoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void initPersonalAds() {
        adsList = new ArrayList<>();
        PersonalAds personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.free_vpn_icon);
        personalAds.setAds_name("Free VPN Hub");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=free.smart.vpn.vpnhubfree");
        adsList.add(personalAds);

        personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.video_downloader_icon);
        personalAds.setAds_name("Best Photo Editor");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=all.hd.downloader.videodownloader");
        adsList.add(personalAds);

        personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.sort_ball_icon);
        personalAds.setAds_name("Sort Ball Puzzle");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=com.sort.ball.puzzle");
        adsList.add(personalAds);

        handler.postDelayed(runnable, 1000);
    }

    private void init() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        nav_settings = findViewById(R.id.shareApp);
        rateApp = findViewById(R.id.rateApp);
        shareApp = findViewById(R.id.feedbackApp);

        adsimg = findViewById(R.id.adsimg);
        adsname = findViewById(R.id.adsname);
        drawerads = findViewById(R.id.drawerads);

        nav_settings.setOnClickListener(this);
        rateApp.setOnClickListener(this);
        shareApp.setOnClickListener(this);

        ViewTreeObserver treeObserver = mDrawerLayout.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mDrawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                SystemUtils.updateGestureExclusion(MainActivity.this);
            }
        });
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void checkForAppUpdate() {
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
            if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            result,
                            AppUpdateType.IMMEDIATE,
                            MainActivity.this,
                            CHECK_UPDATE_REQUEST_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.Widget_Discard_DialogTheme)
                        .setCancelable(false)
                        .setTitle("Try again!")
                        .setMessage("Update failed\nDo you want to try again?")
                        .setPositiveButton("Yes", (dialog, which) -> {
                            checkForAppUpdate();
                            dialog.dismiss();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateCancelBar = Snackbar.make(findViewById(R.id.drawer_layout), "You can still update this app from Playstore.", 3000);
                            updateCancelBar.setAction("Ok", v -> {
                                updateCancelBar.dismiss();
                            });
                            updateCancelBar.show();
                        })
                        .setNeutralButton("Later", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateLaterBar = Snackbar.make(findViewById(R.id.drawer_layout), "As you wish!", 3000);
                            updateLaterBar.setAction(":)", v -> {
                                updateLaterBar.dismiss();
                            });
                            updateLaterBar.show();
                        })
                        .create();
                alertDialog.show();
            } else {
                Snackbar updateSuccessBar = Snackbar.make(findViewById(R.id.drawer_layout), "App updated successfully!", 3000);
                updateSuccessBar.setAction("\uD83D\uDC4D", v -> {
                    updateSuccessBar.dismiss();
                });
                updateSuccessBar.show();
            }
        }
    }

    private void showExitSheet() {
        mDialog.findViewById(R.id.btn_exit).setOnClickListener(view -> {
            mDialog.dismiss();
        });
        mDialog.setOnDismissListener(dialog -> {
            finishAffinity();
            System.exit(0);
        });
        mDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (UtilsAds.isNetworkAvailable(getApplicationContext())) {
            checkForAppUpdate();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File file = null;
            try {
                file = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file == null)
                return;
            photoPath = file.getAbsolutePath();
            photoUri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
            imageCapture.launch(photoUri);
        } else Toast.makeText(
                this,
                "No Activity found to handle the request",
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        showExitSheet();
    }

    private void showRatingDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.best_rating_dialog, null);
        initDialog(view);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setView(view);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        rate_text.setText(getResources().getString(R.string.rating_desc));

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            smiley_view.setSmiley(rating <= 4.5 ? rating : rating - 1);
            this.rating = rating;
        });

        tv_submit.setOnClickListener(v -> {
            if (rating >= 4) {
                AppUtils.getInstance(this).setRated(true);
                alertDialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            } else if (rating > 0) {
                alertDialog.dismiss();
            } else {
                Toast.makeText(MainActivity.this, "Ratings can't be empty!", Toast.LENGTH_SHORT).show();
            }
        });

        tv_maybe_later.setOnClickListener(v -> {
            AppUtils.setCount(0);
            alertDialog.dismiss();
        });
    }

    private void initDialog(View view) {
        rate_text = view.findViewById(R.id.rate_text);
        smiley_view = view.findViewById(R.id.smiley_view);
        ratingBar = view.findViewById(R.id.ratingBar);
        tv_submit = view.findViewById(R.id.tv_submit);
        tv_maybe_later = view.findViewById(R.id.tv_later);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shareApp:
                mDrawerLayout.closeDrawer(GravityCompat.END);
                AppUtils.ShareApp(MainActivity.this);
                break;
            case R.id.rateApp:
                if (!AppUtils.getInstance(MainActivity.this).getRated())
                    showRatingDialog();
                else
                    Toast.makeText(this, "You have already rated!", Toast.LENGTH_SHORT).show();
                mDrawerLayout.closeDrawer(GravityCompat.END);
                break;
            case R.id.feedbackApp:
                mDrawerLayout.closeDrawer(GravityCompat.END);
                break;
        }
    }
}