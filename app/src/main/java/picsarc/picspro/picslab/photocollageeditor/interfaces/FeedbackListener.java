package picsarc.picspro.picslab.photocollageeditor.interfaces;

public interface FeedbackListener {
    void onFeedbackGiven();
}
