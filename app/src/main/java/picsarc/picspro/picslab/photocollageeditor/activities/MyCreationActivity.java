package picsarc.picspro.picslab.photocollageeditor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.util.ArrayList;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.adapters.FilesListAdapter;
import picsarc.picspro.picslab.photocollageeditor.interfaces.PhotoClickListener;
import picsarc.picspro.picslab.photocollageeditor.model.MyFiles;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;

public class MyCreationActivity extends AppCompatActivity implements PhotoClickListener {

    ArrayList<MyFiles> myFiles = new ArrayList<>();
    File[] listFile;
    RecyclerView rv_files;
    Toolbar toolbar;
    MaterialButton imDelete, imShare;
    TextView no_img_text;
    FilesListAdapter filesListAdapter;

    RelativeLayout banner_container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycreation);

        init();
        AppUtils.isDeleted = false;
        myFiles = new ArrayList<>();
        myFiles = getFromSdcard();
        if (myFiles.size() <= 0) {
            no_img_text.setVisibility(View.VISIBLE);
        } else no_img_text.setVisibility(View.GONE);
        filesListAdapter = new FilesListAdapter(this, myFiles, this);
        rv_files.setAdapter(filesListAdapter);
    }

    private void init() {
        rv_files = findViewById(R.id.rv_files);
        no_img_text = findViewById(R.id.no_img_text);
        toolbar = findViewById(R.id.toolbar);
        imDelete = findViewById(R.id.btnDelete);
        imShare = findViewById(R.id.btnShare);
        banner_container = findViewById(R.id.banner_container);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rv_files.setLayoutManager(gridLayoutManager);
    }

    public ArrayList<MyFiles> getFromSdcard() {
        ArrayList<MyFiles> filesList = new ArrayList<>();
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "PicsArc");
        if (file.isDirectory()) {
            listFile = file.listFiles();
            for (int i = 0; i < listFile.length; i++) {
                MyFiles myFiles = new MyFiles();
                myFiles.setName(listFile[i].getName());
                myFiles.setPath(listFile[i].getAbsolutePath());
                myFiles.setSelected(false);
                filesList.add(myFiles);
            }
        }
        return filesList;
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (AppUtils.isDeleted) {
            myFiles = new ArrayList<>();
            myFiles = getFromSdcard();
            if (myFiles.size() <= 0) {
                no_img_text.setVisibility(View.VISIBLE);
            } else no_img_text.setVisibility(View.GONE);
            filesListAdapter = new FilesListAdapter(this, myFiles, this);
            rv_files.setAdapter(filesListAdapter);
        }

        if (filesListAdapter.getItemCount() <= 0)
            no_img_text.setVisibility(View.VISIBLE);
        else
            no_img_text.setVisibility(View.GONE);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPhotoClick(int position) {
        startActivity(new Intent(this, FullViewActivity.class)
                .putExtra("Position", position));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
