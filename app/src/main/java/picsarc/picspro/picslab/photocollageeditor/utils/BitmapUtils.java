package picsarc.picspro.picslab.photocollageeditor.utils;

import android.content.Context;
import android.net.Uri;

import androidx.exifinterface.media.ExifInterface;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.IOException;
import java.io.InputStream;

public class BitmapUtils {
    public static int getOrientation(Uri uri, Context ctx) {

        try (InputStream in = ctx.getContentResolver().openInputStream(uri)) {
            if (in == null) {
                return 0;
            }
            ExifInterface exif = new ExifInterface(in);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return SubsamplingScaleImageView.ORIENTATION_180;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return SubsamplingScaleImageView.ORIENTATION_90;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return SubsamplingScaleImageView.ORIENTATION_270;
                default:
                    return SubsamplingScaleImageView.ORIENTATION_0;
            }
        } catch (IOException e) {
            return 0;
        }
    }
}
