package picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor;

interface BrushViewChangeListener {
    void onStartDrawing();

    void onStopDrawing();

    void onViewAdd(BrushDrawView brushDrawView);

    void onViewRemoved();
}
