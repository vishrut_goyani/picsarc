package picsarc.picspro.picslab.photocollageeditor.sticker_module.fragment;


import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.activities.CollageViewActivity;
import picsarc.picspro.picslab.photocollageeditor.activities.Image_Edit_Activity;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.DataHelper;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker.Offline_Adapter;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker.Sticker_interfce;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class All_Sticker_Fragment extends Fragment {
    RecyclerView mrecyclerView;
    ArrayList<String> strings = new ArrayList<>();
    private TextView rvvis;
    ImageView btnad;
    private String path;
    private String ziprealpath;
    TextView tvPercentage;
    ProgressBar progressBar;
    RelativeLayout rlDownload;
    ImageView bigthumb;
    DataHelper dataHelper;
    String catName;
    Sticker_interfce sticker_interfce;

    public All_Sticker_Fragment() {

    }

    public static Fragment getInstance(int position, String s) {
        String categoryName = s;
        All_Sticker_Fragment bakgrndFragment = new All_Sticker_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("categoryName", categoryName);
        bundle.putInt("positionIs", position);
        bakgrndFragment.setArguments(bundle);
        return bakgrndFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sticker_fragment, container, false);
        this.catName = getArguments().getString("categoryName");
        dataHelper = new DataHelper(getActivity());
        mrecyclerView = view.findViewById(R.id.Frame_Grid);
        rvvis = view.findViewById(R.id.rvvis);
        btnad = view.findViewById(R.id.btnad);
        bigthumb = view.findViewById(R.id.bigthumb);
        progressBar = view.findViewById(R.id.progressBar);
        tvPercentage = view.findViewById(R.id.tvPercentage);
        rlDownload = view.findViewById(R.id.rlDownload);
        progressBar.setMax(100);
        progressBar.setProgress(0);

        if (AppUtils.string.equals("ABC")) {
            sticker_interfce = Image_Edit_Activity.activity;
        } else if (AppUtils.string.equals("XYZ")) {
            sticker_interfce = CollageViewActivity.collageViewActivity;
        }

        File file = new File(getActivity().getFilesDir().getAbsolutePath(), "Sticker Module/" + dataHelper.getAllNotes(catName).get(0).getCategory_name());

        if (file.exists()) {
            getdata(dataHelper.getAllNotes(catName).get(0).getCategory_name());
            btnad.setVisibility(View.GONE);
            bigthumb.setVisibility(View.GONE);
        } else {
            bigthumb.setVisibility(View.VISIBLE);
            Glide.with(this).load(dataHelper.getAllNotes(catName).get(0).getCategory_big_thumb()).into(bigthumb);
        }

        btnad.setOnClickListener(v -> {
            if (isNetworkConnected()) {
                new DownloadFileAsync().execute();
            } else {
                Toast.makeText(getActivity(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    public void getdata(String path) {
        if (strings != null) {
            strings.clear();
        }
        File file = new File(getActivity().getFilesDir().getAbsolutePath(), "Sticker Module/" + path);
        if (file.isDirectory()) {
            File[] listFile = file.listFiles();
            for (int i = 0; i < listFile.length; i++) {
                strings.add(listFile[i].getAbsolutePath());
            }
            if (strings.size() == 0) {
                rvvis.setVisibility(View.VISIBLE);
            }
            Offline_Adapter offline_adapter = new Offline_Adapter(getActivity(), sticker_interfce, strings);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            mrecyclerView.setAdapter(offline_adapter);
            mrecyclerView.setLayoutManager(gridLayoutManager);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) Objects.requireNonNull(getActivity()).getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }


    class DownloadFileAsync extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnad.setVisibility(View.GONE);
            rlDownload.setVisibility(View.VISIBLE);
            bigthumb.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... aurl) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(dataHelper.getAllNotes(catName).get(0).getCategory_zip_url()); // Your given URL.
                url = new URL(url.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                int lenghtOfFile = urlConnection.getContentLength();
                String PATH = getActivity().getFilesDir().getAbsolutePath() + "/Sticker Module/";

                File file = new File(PATH);

                if (!file.exists()) {
                    file.mkdirs();
                }
                File outputFile = new File(file, dataHelper.getAllNotes(catName).get(0).getCategory_name() + ".zip");
                path = getActivity().getFilesDir().getAbsolutePath() + "/Sticker Module/" + dataHelper.getAllNotes(catName).get(0).getCategory_name() + ".zip";
                ziprealpath = getActivity().getFilesDir().getAbsolutePath() + "/Sticker Module/" + dataHelper.getAllNotes(catName).get(0).getCategory_name() + "/";
                FileOutputStream fos = new FileOutputStream(outputFile);


                InputStream is = urlConnection.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    publishProgress((int) ((total * 100) / lenghtOfFile));

                    fos.write(buffer, 0, len1);
                }
                fos.flush();
                fos.close();
                is.close();
            } catch (Exception e) {
            }
            return null;

        }

        protected void onProgressUpdate(Integer... progress) {
            progressBar.setProgress(progress[0]);
            tvPercentage.setText(progress[0].toString() + "%");
        }

        @Override
        protected void onPostExecute(String unused) {
            File file = new File(ziprealpath);
            if (!file.exists()) {
                unzip1(path, ziprealpath);
            }
            rlDownload.setVisibility(View.GONE);
            try {
                getdata(dataHelper.getAllNotes(catName).get(0).getCategory_name());
            } catch (NullPointerException ee) {
                ee.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Boolean unzip1(String sourceFile, String destinationFolder) {
        ZipInputStream zis = null;

        try {
            zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(sourceFile)));
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                String fileName = ze.getName();
                fileName = fileName.substring(fileName.indexOf("/") + 1);
                File file = new File(destinationFolder, fileName);
                File dir = ze.isDirectory() ? file : file.getParentFile();

                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Invalid path: " + dir.getAbsolutePath());
                if (ze.isDirectory()) continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }

            }
        } catch (IOException ioe) {
            return false;
        } finally {
            if (zis != null)
                try {
                    zis.close();
                } catch (IOException e) {

                }
        }
        return true;
    }

}
