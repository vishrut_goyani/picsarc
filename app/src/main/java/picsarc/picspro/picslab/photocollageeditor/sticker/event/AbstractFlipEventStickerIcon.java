package picsarc.picspro.picslab.photocollageeditor.sticker.event;

import android.view.MotionEvent;

import picsarc.picspro.picslab.photocollageeditor.sticker.StickerView;

public abstract class AbstractFlipEventStickerIcon implements EventStickerIcon {
    protected abstract int getFlipDirection();

    public void onActionDown(StickerView paramStickerView, MotionEvent paramMotionEvent) {
    }

    public void onActionMove(StickerView paramStickerView, MotionEvent paramMotionEvent) {
    }

    public void onActionUp(StickerView paramStickerView, MotionEvent paramMotionEvent) {
        paramStickerView.flipCurrentSticker(getFlipDirection());
    }
}
