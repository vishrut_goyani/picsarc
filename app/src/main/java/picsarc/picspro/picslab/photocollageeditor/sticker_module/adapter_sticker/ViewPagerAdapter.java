package picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import picsarc.picspro.picslab.photocollageeditor.sticker_module.fragment.All_Sticker_Fragment;

import java.util.ArrayList;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int numoftabs;
    ArrayList<String> strings;

    public ViewPagerAdapter(FragmentManager fm, int numoftabs, ArrayList<String> strings) {
        super(fm);
        this.numoftabs = numoftabs;
        this.strings = strings;
    }

    public Fragment getItem(int num) {
        return All_Sticker_Fragment.getInstance(num, strings.get(num));
    }

    public int getCount() {
        return numoftabs;
    }


    public CharSequence getPageTitle(int position) {
        return strings.get(position);
    }
}
