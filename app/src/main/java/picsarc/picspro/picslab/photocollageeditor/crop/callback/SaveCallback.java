package picsarc.picspro.picslab.photocollageeditor.crop.callback;

import android.net.Uri;


public interface SaveCallback extends Callback {
  void onSuccess(Uri uri);
}
