package picsarc.picspro.picslab.photocollageeditor.activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.core.internal.view.SupportMenu;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.gun0912.tedpermission.TedPermission;

import org.wysaid.nativePort.CGENativeLibrary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.base.BaseActivity;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.AddTextProperties;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.EditorTextDialogFragment;
import picsarc.picspro.picslab.photocollageeditor.features.adjust.AdapterAdjust;
import picsarc.picspro.picslab.photocollageeditor.features.adjust.AdjustListener;
import picsarc.picspro.picslab.photocollageeditor.features.crop.PicsartCropDialogFragment;
import picsarc.picspro.picslab.photocollageeditor.features.draw.AdapterColor;
import picsarc.picspro.picslab.photocollageeditor.features.draw.AdapterMagicBrush;
import picsarc.picspro.picslab.photocollageeditor.features.draw.BrushColorListener;
import picsarc.picspro.picslab.photocollageeditor.features.draw.BrushMagicListener;
import picsarc.picspro.picslab.photocollageeditor.features.insta.InstaDialog;
import picsarc.picspro.picslab.photocollageeditor.features.mosaic.MosaicDialog;
import picsarc.picspro.picslab.photocollageeditor.features.splash.SplashDialog;
import picsarc.picspro.picslab.photocollageeditor.filters.AdapterFilterView;
import picsarc.picspro.picslab.photocollageeditor.filters.FilterListener;
import picsarc.picspro.picslab.photocollageeditor.filters.UtilsFilter;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.DrawBitmapModel;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.OnPhotoEditorListener;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.PhotoEditor;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.PhotoEditorView;
import picsarc.picspro.picslab.photocollageeditor.sticker.DrawableSticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.Sticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerIconBitmap;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerView;
import picsarc.picspro.picslab.photocollageeditor.sticker.TextSticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EditTextIconEvent;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventDeleteIconStickerIcon;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventStickerIconFlipHorizontally;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventZoomIconStickerIcon;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker.Sticker_interfce;
import picsarc.picspro.picslab.photocollageeditor.sticker_module.fragment.StickFragment;
import picsarc.picspro.picslab.photocollageeditor.tools.EditingToolsAdapter;
import picsarc.picspro.picslab.photocollageeditor.tools.ToolType;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.SystemUtils;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsFile;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsSharePreference;
import picsarc.picspro.picslab.photocollageeditor.widgets.SpinProgressSeekBar;

@SuppressLint("StaticFieldLeak")
public class Image_Edit_Activity extends BaseActivity implements OnPhotoEditorListener, View.OnClickListener, Sticker_interfce,
        PicsartCropDialogFragment.OnCropPhoto, BrushColorListener, BrushMagicListener, InstaDialog.InstaSaveListener, SplashDialog.SplashDialogListener,
        MosaicDialog.MosaicDialogListener, EditingToolsAdapter.OnItemSelected, FilterListener, AdjustListener {
    public ImageView addNewSticker;
    private ImageView addNewText;
    private ConstraintLayout adjustLayout;
    private SpinProgressSeekBar spinSeekBarAdjustLevel;
    private TextView brush;
    private TextView brushBlur;
    private ConstraintLayout brushLayout;
    private SpinProgressSeekBar spinSeekBarBrushSize;
    private ImageView compareAdjust;
    public ImageView compareFilter;
    public ImageView compareOverlay;
    public ToolType currentMode = ToolType.NONE;
    private ImageView erase;
    private SpinProgressSeekBar spinSeekBarEraseSize;
    public SpinProgressSeekBar spinSeekBarFilterIntensity;
    TextView txtDegreeFilter, txtDegreeOverlay, txtDegreeAdjust, txtDegreeSticker;
    public ConstraintLayout filterLayout;
    private LottieAnimationView loadingView;
    public ArrayList<Bitmap> lstBitmapWithFilter = new ArrayList<>();
    public List<Bitmap> lstBitmapWithOverlay = new ArrayList<>();
    public AdapterAdjust mAdapterAdjust;
    private RecyclerView mColorBush;
    public static Image_Edit_Activity activity;
    private final EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this);
    FrameLayout sticker_grid_fragment_container;

    AlertDialog.Builder builder;
    LinearLayout adViewLL;
    View view;
    boolean isSaveDialog;
    ArrayList<Bitmap> allBitmaps;

    public CGENativeLibrary.LoadImageCallback mLoadImageCallback = new CGENativeLibrary.LoadImageCallback() {
        public Bitmap loadImage(String str, Object obj) {
            try {
                return BitmapFactory.decodeStream(getAssets().open(str));
            } catch (IOException io) {
                return null;
            }
        }

        public void loadImageOK(Bitmap bitmap, Object obj) {
            bitmap.recycle();
        }
    };
    private RecyclerView mMagicBrush;
    public PhotoEditor mPhotoEditor;
    public PhotoEditorView mPhotoEditorView;
    private ConstraintLayout mRootView;
    private RecyclerView mRvAdjust;
    public RecyclerView mRvFilters;
    public RecyclerView mRvOverlays;
    public RecyclerView mRvTools;
    private TextView magicBrush;
    public SpinProgressSeekBar spinSeekBarOverlayIntensity;
    public ConstraintLayout overlayLayout;
    private ImageView redo;
    public SpinProgressSeekBar spinSeekBarStickerAlpha;
    private ConstraintLayout stickerLayout;
    public EditorTextDialogFragment.TextEditor textEditor;
    public EditorTextDialogFragment editorTextDialogFragment;
    private ConstraintLayout textLayout;
    private ImageView undo;
    public RelativeLayout wrapPhotoView;
    private StickFragment stickFragment;
    private StickFragment.BGSelectListener stickbgSlect;

    @SuppressLint("ClickableViewAccessibility")
    View.OnTouchListener onCompareTouchListener = (view, motionEvent) -> {
        switch (motionEvent.getAction()) {
            case 0:
                mPhotoEditorView.getGLSurfaceView().setAlpha(0.0f);
                return true;
            case 1:
                mPhotoEditorView.getGLSurfaceView().setAlpha(1.0f);
                return false;
            default:
                return true;
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        makeFullScreen();
        setContentView(R.layout.activity_image_edit);
        initViews();
        activity = this;
        CGENativeLibrary.setLoadImageCallback(mLoadImageCallback, null);
        AppUtils.string = "ABC";
        allBitmaps = new ArrayList<>();
        mRvTools.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mRvTools.setAdapter(mEditingToolsAdapter);
        mRvFilters.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mRvFilters.setHasFixedSize(true);
        mRvOverlays.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mRvOverlays.setHasFixedSize(true);
        new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        mRvAdjust.setLayoutManager(new GridLayoutManager(this, 4));
        mRvAdjust.setHasFixedSize(true);
        mAdapterAdjust = new AdapterAdjust(getApplicationContext(), this);
        mRvAdjust.setAdapter(mAdapterAdjust);
        mColorBush.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mColorBush.setHasFixedSize(true);
        mColorBush.setAdapter(new AdapterColor(getApplicationContext(), this));
        mMagicBrush.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mMagicBrush.setHasFixedSize(true);
        mMagicBrush.setAdapter(new AdapterMagicBrush(getApplicationContext(), this));
        mPhotoEditor = new PhotoEditor.Builder(mPhotoEditorView).setPinchTextScalable(true).build();
        mPhotoEditor.setOnPhotoEditorListener(this);
        toogleDrawBottomToolbar(false);
        brushLayout.setAlpha(0.0f);
        adjustLayout.setAlpha(0.0f);
        filterLayout.setAlpha(0.0f);
        stickerLayout.setAlpha(0.0f);
        textLayout.setAlpha(0.0f);
        overlayLayout.setAlpha(0.0f);
        findViewById(R.id.activitylayout).post(() -> {
            slideDown(brushLayout);
            slideDown(adjustLayout);
            slideDown(filterLayout);
            slideDown(stickerLayout);
            slideDown(textLayout);
            slideDown(overlayLayout);
        });
        new Handler().postDelayed(() -> {
            brushLayout.setAlpha(1.0f);
            adjustLayout.setAlpha(1.0f);
            filterLayout.setAlpha(1.0f);
            stickerLayout.setAlpha(1.0f);
            textLayout.setAlpha(1.0f);
            overlayLayout.setAlpha(1.0f);
        }, 1000);
        UtilsSharePreference.setHeightOfKeyboard(getApplicationContext(), 0);
        ((ConstraintLayout.LayoutParams) wrapPhotoView.getLayoutParams()).topMargin = SystemUtils.dpToPx(getApplicationContext(), 5);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            new loadBitmapFromUri().execute(extras.getString(MainActivity.KEY_SELECTED_PHOTOS));
        }

        findViewById(R.id.undo_count).setOnClickListener(v -> {
            mPhotoEditorView.post(() -> {
                if (mPhotoEditorView.undo_count > 0) {
                    mPhotoEditorView.setImageSource(mPhotoEditorView.getUndoBitmap());
                }
            });
        });

        findViewById(R.id.redo_count).setOnClickListener(v -> {
            mPhotoEditorView.post(() -> {
                if (mPhotoEditorView.redo_count > 0) {
                    mPhotoEditorView.setImageSource(mPhotoEditorView.getRedoBitmap());
                }
            });
        });
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void toogleDrawBottomToolbar(boolean toggle) {
        int i = !toggle ? View.GONE : View.VISIBLE;
        brush.setVisibility(i);
        magicBrush.setVisibility(i);
        brushBlur.setVisibility(i);
        erase.setVisibility(i);
        undo.setVisibility(i);
        redo.setVisibility(i);
    }

    public void showEraseBrush() {
        spinSeekBarBrushSize.setVisibility(View.GONE);
        mColorBush.setVisibility(View.GONE);
        spinSeekBarEraseSize.setVisibility(View.VISIBLE);
        mMagicBrush.setVisibility(View.GONE);
        brush.setBackgroundResource(0);
        brush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        magicBrush.setBackgroundResource(0);
        magicBrush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        brushBlur.setBackgroundResource(0);
        brushBlur.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        erase.setImageResource(R.drawable.eraser);
        mPhotoEditor.brushEraser();
        spinSeekBarEraseSize.setCurrentDegree(20);
    }


    public void showColorBlurBrush() {
        spinSeekBarBrushSize.setVisibility(View.VISIBLE);
        mColorBush.setVisibility(View.VISIBLE);
        AdapterColor adapterColor = (AdapterColor) mColorBush.getAdapter();
        if (adapterColor != null) {
            adapterColor.setSelectedColorIndex(0);
        }
        mColorBush.scrollToPosition(0);
        if (adapterColor != null) {
            adapterColor.notifyDataSetChanged();
        }
        spinSeekBarEraseSize.setVisibility(View.GONE);
        mMagicBrush.setVisibility(View.GONE);
        erase.setImageResource(R.drawable.eraser);
        magicBrush.setBackgroundResource(0);
        magicBrush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        brush.setBackgroundResource(0);
        brush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        brushBlur.setBackground(ContextCompat.getDrawable(this, R.drawable.border_bottom));
        brushBlur.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        mPhotoEditor.setBrushMode(2);
        mPhotoEditor.setBrushDrawingMode(true);
        spinSeekBarBrushSize.setCurrentDegree(20);
    }


    public void showColorBrush() {
        spinSeekBarBrushSize.setVisibility(View.VISIBLE);
        mColorBush.setVisibility(View.VISIBLE);
        mColorBush.scrollToPosition(0);
        AdapterColor adapterColor = (AdapterColor) mColorBush.getAdapter();
        if (adapterColor != null) {
            adapterColor.setSelectedColorIndex(0);
        }
        if (adapterColor != null) {
            adapterColor.notifyDataSetChanged();
        }
        spinSeekBarEraseSize.setVisibility(View.GONE);
        mMagicBrush.setVisibility(View.GONE);
        erase.setImageResource(R.drawable.eraser);
        magicBrush.setBackgroundResource(0);
        magicBrush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        brush.setBackground(ContextCompat.getDrawable(this, R.drawable.border_bottom));
        brush.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        brushBlur.setBackgroundResource(0);
        brushBlur.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        mPhotoEditor.setBrushMode(1);
        mPhotoEditor.setBrushDrawingMode(true);
        spinSeekBarBrushSize.setCurrentDegree(20);
    }

    public void showMagicBrush() {
        spinSeekBarBrushSize.setVisibility(View.VISIBLE);
        mColorBush.setVisibility(View.GONE);
        spinSeekBarEraseSize.setVisibility(View.GONE);
        mMagicBrush.setVisibility(View.VISIBLE);
        erase.setImageResource(R.drawable.eraser);
        magicBrush.setBackground(ContextCompat.getDrawable(this, R.drawable.border_bottom));
        magicBrush.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
        brush.setBackgroundResource(0);
        brush.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        brushBlur.setBackgroundResource(0);
        brushBlur.setTextColor(ContextCompat.getColor(this, R.color.unselected_color));
        mPhotoEditor.setBrushMagic(AdapterMagicBrush.lstDrawBitmapModel(getApplicationContext()).get(0));
        mPhotoEditor.setBrushMode(3);
        mPhotoEditor.setBrushDrawingMode(true);
        AdapterMagicBrush adapterMagicBrush = (AdapterMagicBrush) mMagicBrush.getAdapter();
        if (adapterMagicBrush != null) {
            adapterMagicBrush.setSelectedColorIndex(0);
        }
        mMagicBrush.scrollToPosition(0);
        if (adapterMagicBrush != null) {
            adapterMagicBrush.notifyDataSetChanged();
        }
    }

    private void initViews() {
        mPhotoEditorView = findViewById(R.id.photoEditorView);
        sticker_grid_fragment_container = findViewById(R.id.sticker_grid_fragment_container);
        mPhotoEditorView.setVisibility(View.INVISIBLE);
        mRvTools = findViewById(R.id.rvConstraintTools);
        mRvFilters = findViewById(R.id.rvFilterView);
        mRvOverlays = findViewById(R.id.rvOverlayView);
        mRvAdjust = findViewById(R.id.rvAdjustView);
        mRootView = findViewById(R.id.rootView);
        filterLayout = findViewById(R.id.filterLayout);
        overlayLayout = findViewById(R.id.overlayLayout);
        adjustLayout = findViewById(R.id.adjustLayout);
        stickerLayout = findViewById(R.id.stickerLayout);
        textLayout = findViewById(R.id.textControl);
        spinSeekBarFilterIntensity = findViewById(R.id.spinSeekBarFilterIntensity);
        spinSeekBarAdjustLevel = findViewById(R.id.spinSeekBarAdjustLevel);
        spinSeekBarAdjustLevel.setDegreeRange(0, 100);
        spinSeekBarAdjustLevel.setCurrentDegree(50);
        spinSeekBarFilterIntensity.setDegreeRange(0, 100);
        spinSeekBarFilterIntensity.setCurrentDegree(100);
        txtDegreeFilter = findViewById(R.id.txtDegreeFilter);
        txtDegreeOverlay = findViewById(R.id.txtDegreeOverlay);
        txtDegreeAdjust = findViewById(R.id.txtDegreeAdjust);
        txtDegreeSticker = findViewById(R.id.txtDegreeSticker);
        spinSeekBarOverlayIntensity = findViewById(R.id.spinSeekBarOverlayIntensity);
        spinSeekBarOverlayIntensity.setDegreeRange(0, 100);
        spinSeekBarOverlayIntensity.setCurrentDegree(100);
        spinSeekBarStickerAlpha = findViewById(R.id.spinSeekBarStickerAlpha);
        spinSeekBarStickerAlpha.setDegreeRange(0, 255);
        spinSeekBarStickerAlpha.setCurrentDegree(255);
        spinSeekBarStickerAlpha.setVisibility(View.GONE);
        brushLayout = findViewById(R.id.brushLayout);
        mColorBush = findViewById(R.id.rvColorBush);
        mMagicBrush = findViewById(R.id.rvMagicBush);
        wrapPhotoView = findViewById(R.id.wrap_photo_view);
        brush = findViewById(R.id.draw);
        magicBrush = findViewById(R.id.brush_magic);
        erase = findViewById(R.id.erase);
        undo = findViewById(R.id.undo);
        undo.setVisibility(View.GONE);
        redo = findViewById(R.id.redo);
        redo.setVisibility(View.GONE);
        brushBlur = findViewById(R.id.brush_blur);
        spinSeekBarBrushSize = findViewById(R.id.brushSize);
        spinSeekBarBrushSize.setDegreeRange(0, 60);
        spinSeekBarEraseSize = findViewById(R.id.eraseSize);
        spinSeekBarEraseSize.setDegreeRange(0, 100);
        loadingView = findViewById(R.id.loadingView);

        view = LayoutInflater.from(this).inflate(R.layout.save_dialog, null);

        loadingView.setVisibility(View.VISIBLE);


        findViewById(R.id.ic_back).setOnClickListener(v -> onBackPressed());

        findViewById(R.id.imgsave).setOnClickListener(view -> {
            if (TedPermission.canRequestPermission(Image_Edit_Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                try {
                    isSaveDialog = true;
                    saveDiscardDialog(isSaveDialog);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        compareAdjust = findViewById(R.id.compareAdjust);
        compareAdjust.setOnTouchListener(onCompareTouchListener);
        compareAdjust.setVisibility(View.GONE);

        compareFilter = findViewById(R.id.compareFilter);
        compareFilter.setOnTouchListener(onCompareTouchListener);
        compareFilter.setVisibility(View.GONE);

        compareOverlay = findViewById(R.id.compareOverlay);
        compareOverlay.setOnTouchListener(onCompareTouchListener);
        compareOverlay.setVisibility(View.GONE);
        erase.setOnClickListener(view -> showEraseBrush());
        findViewById(R.id.imgSaveSticker).setOnClickListener(this);
        brush.setOnClickListener(view -> showColorBrush());
        magicBrush.setOnClickListener(view -> showMagicBrush());
        brushBlur.setOnClickListener(view -> showColorBlurBrush());
        spinSeekBarEraseSize.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {
            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                if (spinSeekBarEraseSize.getCurrentDegrees() >= 255) {
                    progress = 255;
                    mPhotoEditor.setBrushEraserSize((float) progress);
                    txtDegreeSticker.setText((100 * progress) / 255 + "");
                } else if (spinSeekBarEraseSize.getCurrentDegrees() <= 0) {
                    progress = 0;
                    mPhotoEditor.setBrushEraserSize((float) progress);
                    txtDegreeSticker.setText(progress + "");
                } else {
                    progress = (100 * spinSeekBarEraseSize.getCurrentDegrees()) / 255;
                    mPhotoEditor.setBrushEraserSize((float) progress);
                    txtDegreeSticker.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });
        spinSeekBarBrushSize.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {

            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                if (spinSeekBarBrushSize.getCurrentDegrees() >= 255) {
                    progress = 255;
                    mPhotoEditor.setBrushSize((float) (progress + 10));
                    txtDegreeSticker.setText(progress + "");
                } else if (spinSeekBarBrushSize.getCurrentDegrees() <= 0) {
                    progress = 0;
                    mPhotoEditor.setBrushSize((float) (progress + 10));
                    txtDegreeSticker.setText(progress + "");
                } else {
                    progress = spinSeekBarBrushSize.getCurrentDegrees();
                    mPhotoEditor.setBrushSize((float) (progress + 10));
                    txtDegreeSticker.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });
        spinSeekBarStickerAlpha.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {

            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                Sticker currentSticker = mPhotoEditorView.getCurrentSticker();

                if (spinSeekBarAdjustLevel.getCurrentDegrees() >= 255) {
                    progress = 255;
                    if (currentSticker != null) {
                        currentSticker.setAlpha(currentDegrees);
                    }
                    txtDegreeSticker.setText(progress + "");
                } else if (spinSeekBarAdjustLevel.getCurrentDegrees() <= 0) {
                    progress = 0;
                    if (currentSticker != null) {
                        currentSticker.setAlpha(currentDegrees);
                    }
                    txtDegreeSticker.setText(progress + "");
                } else {
                    progress = spinSeekBarAdjustLevel.getCurrentDegrees();
                    if (currentSticker != null) {
                        currentSticker.setAlpha(currentDegrees);
                    }
                    txtDegreeSticker.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });
        addNewSticker = findViewById(R.id.addNewSticker);
        addNewSticker.setVisibility(View.GONE);
        addNewSticker.setOnClickListener(view -> {
            addNewSticker.setVisibility(View.GONE);
        });
        addNewText = findViewById(R.id.addNewText);
        addNewText.setVisibility(View.GONE);
        addNewText.setOnClickListener(view -> {
            mPhotoEditorView.setHandlingSticker(null);
            openTextFragment();
        });
        spinSeekBarAdjustLevel.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {

            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                if (spinSeekBarAdjustLevel.getCurrentDegrees() >= 100) {
                    progress = 100;
                    mAdapterAdjust.getCurrentAdjustModel().setIntensity(mPhotoEditor, ((float) progress) / ((float) 100), true);
                    txtDegreeAdjust.setText(progress + "");
                } else if (spinSeekBarAdjustLevel.getCurrentDegrees() <= 0) {
                    progress = 0;
                    mAdapterAdjust.getCurrentAdjustModel().setIntensity(mPhotoEditor, ((float) progress) / ((float) 100), true);
                    txtDegreeAdjust.setText(progress + "");
                } else {
                    progress = spinSeekBarAdjustLevel.getCurrentDegrees();
                    mAdapterAdjust.getCurrentAdjustModel().setIntensity(mPhotoEditor, ((float) progress) / ((float) 100), true);
                    txtDegreeAdjust.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });

        StickerIconBitmap bitmapStickerIcon0 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_delete_sticker), 0, StickerIconBitmap.REMOVE);
        bitmapStickerIcon0.setIconEvent(new EventDeleteIconStickerIcon());
        StickerIconBitmap bitmapStickerIcon1 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_flip_sticker), 1, StickerIconBitmap.FLIP);
        bitmapStickerIcon1.setIconEvent(new EventStickerIconFlipHorizontally());
        StickerIconBitmap bitmapStickerIcon2 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_edit_sticker), 2, StickerIconBitmap.EDIT);
        bitmapStickerIcon2.setIconEvent(new EditTextIconEvent());
        StickerIconBitmap bitmapStickerIcon3 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_resize_sticker), 3, StickerIconBitmap.ZOOM);
        bitmapStickerIcon3.setIconEvent(new EventZoomIconStickerIcon());
        StickerIconBitmap bitmapStickerIcon4 = new StickerIconBitmap(ContextCompat.getDrawable(this, R.drawable.ic_rotate_sticker), 3, StickerIconBitmap.ROTATE);
        bitmapStickerIcon4.setIconEvent(new EventZoomIconStickerIcon());
        ArrayList<StickerIconBitmap> arrayList = new ArrayList<>();
        arrayList.add(bitmapStickerIcon0);
        arrayList.add(bitmapStickerIcon3);
        arrayList.add(bitmapStickerIcon1);
        arrayList.add(bitmapStickerIcon3);
        arrayList.add(bitmapStickerIcon4);
        mPhotoEditorView.setIcons(arrayList);
//        mPhotoEditorView.setIcons(Arrays.asList(bitmapStickerIcon0, bitmapStickerIcon3, bitmapStickerIcon1, bitmapStickerIcon3, bitmapStickerIcon4));
        mPhotoEditorView.setBackgroundColor(-16777216);
        mPhotoEditorView.setLocked(false);
        mPhotoEditorView.setConstrained(true);
        mPhotoEditorView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            @Override
            public void onTouchDownForBeauty(float f, float f2) {
            }

            @Override
            public void onTouchDragForBeauty(float f, float f2) {
            }

            @Override
            public void onTouchUpForBeauty() {

            }

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                spinSeekBarStickerAlpha.setVisibility(View.VISIBLE);
                spinSeekBarStickerAlpha.setCurrentDegree(sticker.getAlpha());
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                if (sticker instanceof TextSticker) {
                    ((TextSticker) sticker).setTextColor(SupportMenu.CATEGORY_MASK);
                    mPhotoEditorView.replace(sticker);
                    mPhotoEditorView.invalidate();
                }
                spinSeekBarStickerAlpha.setVisibility(View.VISIBLE);
                spinSeekBarStickerAlpha.setCurrentDegree(sticker.getAlpha());
            }

            @Override
            public void onStickerDeleted() {

            }

            @Override
            public void onStickerTouchOutside() {
                spinSeekBarStickerAlpha.setVisibility(View.GONE);
            }

            @Override
            public void onStickerTouchedDown() {

            }

            @Override
            public void onStickerZoomFinished() {

            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                if (sticker instanceof TextSticker) {
                    sticker.setShow(false);
                    mPhotoEditorView.setHandlingSticker(null);
                    editorTextDialogFragment = EditorTextDialogFragment.show(Image_Edit_Activity.this, ((TextSticker) sticker).getTextAddProperties());
                    textEditor = new EditorTextDialogFragment.TextEditor() {
                        public void onDone(AddTextProperties addTextProperties) {
                            mPhotoEditorView.getStickers().remove(mPhotoEditorView.getLastHandlingSticker());
                            mPhotoEditorView.addSticker(new TextSticker(Image_Edit_Activity.this, addTextProperties));
                        }

                        public void onBackButton() {
                            mPhotoEditorView.showLastHandlingSticker();
                        }
                    };
                    editorTextDialogFragment.setOnTextEditorListener(textEditor);
                }
            }

            @Override
            public void onStickerDragFinished() {

            }

            @Override
            public void onStickerFlipped() {

            }
        });

        spinSeekBarFilterIntensity.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {

            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                if (spinSeekBarFilterIntensity.getCurrentDegrees() >= 100) {
                    progress = 100;
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeFilter.setText(progress + "");
                } else if (spinSeekBarFilterIntensity.getCurrentDegrees() <= 0) {
                    progress = 0;
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeFilter.setText(progress + "");
                } else {
                    progress = spinSeekBarFilterIntensity.getCurrentDegrees();
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeFilter.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });

        spinSeekBarOverlayIntensity.setScrollingListener(new SpinProgressSeekBar.ScrollingListener() {
            @Override
            public void onScrollStart() {

            }

            @Override
            public void onScroll(int currentDegrees) {
                int progress;
                if (spinSeekBarOverlayIntensity.getCurrentDegrees() >= 100) {
                    progress = 100;
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeOverlay.setText(progress + "");
                } else if (spinSeekBarOverlayIntensity.getCurrentDegrees() <= 0) {
                    progress = 0;
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeOverlay.setText(progress + "");
                } else {
                    progress = spinSeekBarOverlayIntensity.getCurrentDegrees();
                    mPhotoEditorView.setFilterIntensity(((float) progress) / 100.0f);
                    txtDegreeOverlay.setText(progress + "");
                }
            }

            @Override
            public void onScrollEnd() {

            }
        });
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
    }

    public void showLoading(boolean z) {
        if (z) {
            getWindow().setFlags(16, 16);
            loadingView.setVisibility(View.VISIBLE);
            return;
        }
        getWindow().clearFlags(16);
        loadingView.setVisibility(View.GONE);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
    }

    @Override
    public void onAddViewListener() {

    }

    @Override
    public void onRemoveViewListener() {
    }

    @Override
    public void onStartViewChangeListener() {
    }

    @Override
    public void onStopViewChangeListener() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCloseAdjust:
                spinSeekBarAdjustLevel.setCurrentDegree(50);
                mPhotoEditor.setFilterEffect("");
                compareAdjust.setVisibility(View.GONE);
                slideDown(adjustLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgCloseBrush:
                findViewById(R.id.stickerControl).setVisibility(View.GONE);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                return;
            case R.id.imgCloseFilter:
                spinSeekBarFilterIntensity.setCurrentDegree(100);
                findViewById(R.id.stickerControl).setVisibility(View.GONE);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                return;
            case R.id.imgCloseOverlay:
                spinSeekBarOverlayIntensity.setCurrentDegree(100);
                findViewById(R.id.stickerControl).setVisibility(View.GONE);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                return;
            case R.id.imgCloseSticker:
                findViewById(R.id.stickerControl).setVisibility(View.GONE);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                if (stickFragment != null && stickFragment.isVisible()) {
                    stickFragment.backtrace();
                }
                return;
            case R.id.imgCloseText:
                slideDownSaveView();
                onBackPressed();
                return;
            case R.id.imgSaveAdjust:
                new SaveFilterAsBitmap().execute();
                compareAdjust.setVisibility(View.GONE);
                slideDown(adjustLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveBrush:
                showLoading(true);
                runOnUiThread(() -> {
                    mPhotoEditor.setBrushDrawingMode(false);
                    undo.setVisibility(View.GONE);
                    redo.setVisibility(View.GONE);
                    erase.setVisibility(View.GONE);
                    slideDown(brushLayout);
                    slideUp(mRvTools);
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(mRootView);
                    constraintSet.connect(wrapPhotoView.getId(), 3, mRootView.getId(), 3, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 1, mRootView.getId(), 1, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 4, mRvTools.getId(), 3, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 2, mRootView.getId(), 2, 0);
                    constraintSet.applyTo(mRootView);
                    mPhotoEditorView.setImageSource(getMergedBitmap(mPhotoEditorView.getCurrentBitmap(),
                            mPhotoEditor.getBrushDrawView().getDrawBitmap(mPhotoEditorView.getCurrentBitmap())));
                    mPhotoEditorView.redo_count = 0;
                    mPhotoEditorView.addNewBitmap(getMergedBitmap(mPhotoEditorView.getCurrentBitmap(),
                            mPhotoEditor.getBrushDrawView().getDrawBitmap(mPhotoEditorView.getCurrentBitmap())));
                    mPhotoEditor.clearBrushAllViews();
                    showLoading(false);
                    updateLayout();
                });
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveFilter:
                new SaveFilterAsBitmap().execute();
                compareFilter.setVisibility(View.GONE);
                slideDown(filterLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveOverlay:
                new SaveFilterAsBitmap().execute();
                slideDown(overlayLayout);
                slideUp(mRvTools);
                compareOverlay.setVisibility(View.GONE);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveSticker:
                mPhotoEditorView.setHandlingSticker(null);
                mPhotoEditorView.setLocked(true);
                spinSeekBarStickerAlpha.setVisibility(View.GONE);
                addNewSticker.setVisibility(View.GONE);
                if (!mPhotoEditorView.getStickers().isEmpty()) {
                    new SaveStickerAsBitmap().execute();
                }
                sticker_grid_fragment_container.setVisibility(View.GONE);
                slideDown(stickerLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.imgSaveText:
                mPhotoEditorView.setHandlingSticker(null);
                mPhotoEditorView.setLocked(true);
                addNewText.setVisibility(View.GONE);
                if (!mPhotoEditorView.getStickers().isEmpty()) {
                    new SaveStickerAsBitmap().execute();
                }
                slideDown(textLayout);
                slideUp(mRvTools);
                slideDownSaveView();
                currentMode = ToolType.NONE;
                return;
            case R.id.redo:
                mPhotoEditor.redoBrush();
                return;
            case R.id.undo:
                mPhotoEditor.undoBrush();
                return;
            default:
                super.onBackPressed();
        }
    }

    private Bitmap getMergedBitmap(Bitmap bitmap1, Bitmap bitmap2) {
        Bitmap bitmap = null;
        try {

            bitmap = Bitmap.createBitmap(bitmap1.getWidth(), bitmap1.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            Drawable drawable1 = new BitmapDrawable(bitmap1);
            Drawable drawable2 = new BitmapDrawable(bitmap2);

            drawable1.setBounds(0, 0, bitmap1.getWidth(), bitmap1.getHeight());
            drawable2.setBounds(0, 0, bitmap1.getWidth(), bitmap1.getHeight());
            drawable1.draw(c);
            drawable2.draw(c);

        } catch (Exception e) {
        }
        return bitmap;

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void isPermissionGranted(boolean z) {
        if (z) {
            new saveBitmapAsFile().execute();
        }
    }

    @Override
    public void onFilterSelected(String str) {
        mPhotoEditor.setFilterEffect(str);
        spinSeekBarFilterIntensity.setCurrentDegree(100);
        txtDegreeFilter.setText("" + 100);
        txtDegreeOverlay.setText("" + 70);
        if (currentMode == ToolType.OVERLAY) {
            mPhotoEditorView.getGLSurfaceView().setFilterIntensity(0.7f);
        }
    }

    public void openTextFragment() {
        editorTextDialogFragment = EditorTextDialogFragment.show(this);
        textEditor = new EditorTextDialogFragment.TextEditor() {
            public void onDone(AddTextProperties addTextProperties) {
                mPhotoEditorView.addSticker(new TextSticker(getApplicationContext(), addTextProperties));
            }

            @Override
            public void onBackButton() {
                if (mPhotoEditorView.getStickers().isEmpty()) {
                    onBackPressed();
                }
            }
        };
        editorTextDialogFragment.setOnTextEditorListener(textEditor);
    }

    @Override
    public void onToolSelected(ToolType toolType) {
        currentMode = toolType;
        switch (toolType) {
            case BRUSH:
                showColorBrush();
                mPhotoEditor.setBrushDrawingMode(true);
                slideDown(mRvTools);
                slideUp(brushLayout);
                slideUpSaveControl();
                toogleDrawBottomToolbar(true);
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(mRootView);
                constraintSet.connect(wrapPhotoView.getId(), 3, mRootView.getId(), 3, 0);
                constraintSet.connect(wrapPhotoView.getId(), 1, mRootView.getId(), 1, 0);
                constraintSet.connect(wrapPhotoView.getId(), 4, brushLayout.getId(), 3, 0);
                constraintSet.connect(wrapPhotoView.getId(), 2, mRootView.getId(), 2, 0);
                constraintSet.applyTo(mRootView);
                mPhotoEditor.setBrushMode(1);
                updateLayout();
                break;
            case TEXT:
                findViewById(R.id.toolbar).setVisibility(View.INVISIBLE);
                mPhotoEditorView.setLocked(false);
                openTextFragment();
                slideDown(mRvTools);
                slideUp(textLayout);
                addNewText.setVisibility(View.VISIBLE);
                break;
            case ADJUST:
                slideUpSaveView();
                spinSeekBarAdjustLevel.setCurrentDegree(50);
                compareAdjust.setVisibility(View.VISIBLE);
                mAdapterAdjust = new AdapterAdjust(getApplicationContext(), this);
                mRvAdjust.setAdapter(mAdapterAdjust);
                mAdapterAdjust.setSelectedAdjust(0);
                mPhotoEditor.setAdjustFilter(mAdapterAdjust.getFilterConfig());
                slideUp(adjustLayout);
                slideDown(mRvTools);
                break;
            case FILTER:
                slideUpSaveView();
                new LoadFilterBitmap().execute();
                break;
            case STICKER:
                findViewById(R.id.stickerControl).setVisibility(View.VISIBLE);
                sticker_grid_fragment_container.setVisibility(View.VISIBLE);
                slideUpSaveView();
                slideUp(stickerLayout);
                slideUp(findViewById(R.id.stickerControl));
                slideDown(mRvTools);
                mPhotoEditorView.setLocked(false);
                newStickerselect();
                new SaveStickerAsBitmap().execute();
                break;
            case OVERLAY:
                slideUpSaveView();
                new LoadOverlayBitmap().execute();
                break;
            case INSTA:
                new ShowInstaDialog().execute();
                break;
            case SPLASH:
                new ShowSplashDialog(true).execute();
                break;
            case BLUR:
                new ShowSplashDialog(false).execute();
                break;
            case MOSAIC:
                new ShowMosaicDialog().execute();
                break;
            case CROP:
                PicsartCropDialogFragment.show(this, this, mPhotoEditorView.getCurrentBitmap());
                break;
        }
        mPhotoEditorView.setHandlingSticker(null);
    }

    public void slideUp(View view) {
        ObjectAnimator.ofFloat(view, "translationY", (float) view.getHeight(), 0.0f).start();
    }

    public void slideUpSaveView() {
        findViewById(R.id.toolbar).setVisibility(View.GONE);
    }

    public void slideUpSaveControl() {
        findViewById(R.id.toolbar).setVisibility(View.GONE);
    }

    public void slideDownSaveControl() {
        findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }

    public void slideDownSaveView() {
        findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
    }

    public void slideDown(View view) {
        ObjectAnimator.ofFloat(view, "translationY", 0.0f, (float) view.getHeight()).start();
    }

    @Override
    public void onBackPressed() {
        if (currentMode == null) {
            super.onBackPressed();
            return;
        }
        try {
            switch (currentMode) {
                case BRUSH:
                    slideDown(brushLayout);
                    slideUp(mRvTools);
                    slideDownSaveControl();
                    undo.setVisibility(View.GONE);
                    redo.setVisibility(View.GONE);
                    erase.setVisibility(View.GONE);
                    mPhotoEditor.setBrushDrawingMode(false);
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(mRootView);
                    constraintSet.connect(wrapPhotoView.getId(), 3, mRootView.getId(), 3, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 1, mRootView.getId(), 1, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 4, mRvTools.getId(), 3, 0);
                    constraintSet.connect(wrapPhotoView.getId(), 2, mRootView.getId(), 2, 0);
                    constraintSet.applyTo(mRootView);
                    mPhotoEditor.clearBrushAllViews();
                    slideDownSaveView();
                    currentMode = ToolType.NONE;
                    updateLayout();
                    return;
                case TEXT:
                    if (!mPhotoEditorView.getStickers().isEmpty()) {
                        mPhotoEditorView.getStickers().clear();
                        mPhotoEditorView.setHandlingSticker(null);
                    }
                    slideDown(textLayout);
                    addNewText.setVisibility(View.GONE);
                    mPhotoEditorView.setHandlingSticker(null);
                    slideUp(mRvTools);
                    mPhotoEditorView.setLocked(true);
                    slideDownSaveView();
                    currentMode = ToolType.NONE;
                    return;
                case ADJUST:
                    mPhotoEditor.setFilterEffect("");
                    compareAdjust.setVisibility(View.GONE);
                    slideDown(adjustLayout);
                    slideUp(mRvTools);
                    slideDownSaveView();
                    currentMode = ToolType.NONE;
                    return;
                case FILTER:
                    slideDown(filterLayout);
                    slideUp(mRvTools);
                    slideDownSaveView();
                    mPhotoEditor.setFilterEffect("");
                    compareFilter.setVisibility(View.GONE);
                    lstBitmapWithFilter.clear();
                    if (mRvFilters.getAdapter() != null) {
                        mRvFilters.getAdapter().notifyDataSetChanged();
                    }
                    currentMode = ToolType.NONE;
                    return;
                case STICKER:
                    if (stickFragment != null && stickFragment.isVisible()) {
                        stickFragment.backtrace();
                        findViewById(R.id.stickerControl).setVisibility(View.GONE);
                        slideDown(stickerLayout);
                        slideUp(mRvTools);
                        slideDownSaveView();
                    } else {
                        isSaveDialog = false;
                        saveDiscardDialog(isSaveDialog);
                    }
                    return;
                case OVERLAY:
                    mPhotoEditor.setFilterEffect("");
                    compareOverlay.setVisibility(View.GONE);
                    lstBitmapWithOverlay.clear();
                    slideUp(mRvTools);
                    slideDown(overlayLayout);
                    slideDownSaveView();
                    mRvOverlays.getAdapter().notifyDataSetChanged();
                    currentMode = ToolType.NONE;
                    return;
                case SPLASH:
                case BLUR:
                case MOSAIC:
                case CROP:
                    isSaveDialog = false;
                    saveDiscardDialog(isSaveDialog);
                    return;
                case NONE:
                    isSaveDialog = false;
                    saveDiscardDialog(isSaveDialog);
                    return;
                default:
                    finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveDiscardDialog(boolean isSaveDialog) {
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);

        if (isSaveDialog) {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(R.string.dialog_save_text);
        } else {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.dialog_discard_text));
        }

        builder = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            if (isSaveDialog) {
                mPhotoEditorView.setLocked(true);
                new saveBitmapAsFile().execute();
            } else {
                currentMode = null;
                finish();
            }
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            mPhotoEditorView.setLocked(false);
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAdjustSelected(AdapterAdjust.AdjustModel adjustModel) {
        spinSeekBarAdjustLevel.setCurrentDegree((int) (adjustModel.sliderIntensity * ((float) 100)));
    }

    @Override
    public void finishCrop(Bitmap bitmap) {
        mPhotoEditorView.setImageSource(bitmap);
        currentMode = ToolType.NONE;
        mPhotoEditorView.redo_count = 0;
        mPhotoEditorView.addNewBitmap(bitmap);

        updateLayout();
    }

    @Override
    public void onColorChanged(String str) {
        mPhotoEditor.setBrushColor(Color.parseColor(str));
    }

    @Override
    public void onMagicChanged(DrawBitmapModel drawBitmapModel) {
        mPhotoEditor.setBrushMagic(drawBitmapModel);
    }

    @Override
    public void instaSavedBitmap(Bitmap bitmap) {
        mPhotoEditorView.setImageSource(bitmap);
        currentMode = ToolType.NONE;
        mPhotoEditorView.redo_count = 0;
        mPhotoEditorView.addNewBitmap(bitmap);

        updateLayout();
    }

    @Override
    public void onSaveSplash(Bitmap bitmap) {
        mPhotoEditorView.setImageSource(bitmap);
        currentMode = ToolType.NONE;
        mPhotoEditorView.redo_count = 0;
        mPhotoEditorView.addNewBitmap(bitmap);

    }

    @Override
    public void onSaveMosaic(Bitmap bitmap) {
        mPhotoEditorView.setImageSource(bitmap);
        currentMode = ToolType.NONE;
        mPhotoEditorView.redo_count = 0;
        mPhotoEditorView.addNewBitmap(bitmap);

    }

    @Override
    public void getsticker() {
        mPhotoEditorView.addSticker(new DrawableSticker(new BitmapDrawable(getResources(), AppUtils.Sticker_bitmap)));
    }

    class LoadFilterBitmap extends AsyncTask<Void, Void, Void> {
        LoadFilterBitmap() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public Void doInBackground(Void... voidArr) {
            lstBitmapWithFilter.clear();
            lstBitmapWithFilter.addAll(UtilsFilter.getLstBitmapWithFilter(ThumbnailUtils.extractThumbnail(mPhotoEditorView.getCurrentBitmap(), 100, 100)));
            return null;
        }

        @Override
        public void onPostExecute(Void voidR) {
            mRvFilters.setAdapter(new AdapterFilterView(lstBitmapWithFilter,
                    Image_Edit_Activity.this, Image_Edit_Activity.this, Arrays.asList(UtilsFilter.EFFECT_CONFIGS)));
            slideDown(mRvTools);
            slideUp(filterLayout);
            compareFilter.setVisibility(View.VISIBLE);
            spinSeekBarFilterIntensity.setCurrentDegree(100);
            txtDegreeFilter.setText("" + 100);
            showLoading(false);
        }
    }

    class ShowInstaDialog extends AsyncTask<Void, Bitmap, Bitmap> {
        ShowInstaDialog() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public Bitmap doInBackground(Void... voidArr) {
            return UtilsFilter.getBlurImageFromBitmap(mPhotoEditorView.getCurrentBitmap(), 5.0f);
        }

        @Override
        public void onPostExecute(Bitmap bitmap) {
            showLoading(false);
            InstaDialog.show(Image_Edit_Activity.this, Image_Edit_Activity.this, mPhotoEditorView.getCurrentBitmap(), bitmap);
        }
    }

    class ShowMosaicDialog extends AsyncTask<Void, List<Bitmap>, List<Bitmap>> {
        ShowMosaicDialog() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public List<Bitmap> doInBackground(Void... voidArr) {
            List<Bitmap> arrayList = new ArrayList<>();
            arrayList.add(UtilsFilter.cloneBitmap(mPhotoEditorView.getCurrentBitmap()));
            arrayList.add(UtilsFilter.getBlurImageFromBitmap(mPhotoEditorView.getCurrentBitmap(), 8.0f));
            return arrayList;
        }

        @Override
        public void onPostExecute(List<Bitmap> list) {
            showLoading(false);
            MosaicDialog.show(Image_Edit_Activity.this, list.get(0), list.get(1), Image_Edit_Activity.this);
        }
    }

    class LoadOverlayBitmap extends AsyncTask<Void, Void, Void> {
        LoadOverlayBitmap() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public Void doInBackground(Void... voidArr) {
            lstBitmapWithOverlay.clear();
            lstBitmapWithOverlay.addAll(UtilsFilter.getLstBitmapWithOverlay(ThumbnailUtils.extractThumbnail(mPhotoEditorView.getCurrentBitmap(), 100, 100)));
            return null;
        }

        @Override
        public void onPostExecute(Void voidR) {
            mRvOverlays.setAdapter(new AdapterFilterView(lstBitmapWithOverlay,
                    Image_Edit_Activity.this, Image_Edit_Activity.this, Arrays.asList(UtilsFilter.OVERLAY_CONFIG)));
            slideDown(mRvTools);
            slideUp(overlayLayout);
            compareOverlay.setVisibility(View.VISIBLE);
            spinSeekBarOverlayIntensity.setCurrentDegree(100);
            showLoading(false);
        }
    }

    class ShowSplashDialog extends AsyncTask<Void, List<Bitmap>, List<Bitmap>> {
        boolean isSplash;

        public ShowSplashDialog(boolean z) {
            isSplash = z;
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public List<Bitmap> doInBackground(Void... voidArr) {
            Bitmap currentBitmap = mPhotoEditorView.getCurrentBitmap();
            List<Bitmap> arrayList = new ArrayList<>();
            arrayList.add(currentBitmap);
            if (isSplash) {
                arrayList.add(UtilsFilter.getBlackAndWhiteImageFromBitmap(currentBitmap));
            } else {
                arrayList.add(UtilsFilter.getBlurImageFromBitmap(currentBitmap, 3.0f));
            }
            return arrayList;
        }

        @Override
        public void onPostExecute(List<Bitmap> list) {
            if (isSplash) {
                SplashDialog.show(Image_Edit_Activity.this, list.get(0), null, list.get(1), Image_Edit_Activity.this, true);
            } else {
                SplashDialog.show(Image_Edit_Activity.this, list.get(0), list.get(1), null, Image_Edit_Activity.this, false);
            }
            showLoading(false);
        }
    }

    class SaveFilterAsBitmap extends AsyncTask<Void, Void, Bitmap> {
        SaveFilterAsBitmap() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public Bitmap doInBackground(Void... voidArr) {
            final Bitmap[] bitmapArr = {null};
            mPhotoEditorView.saveGLSurfaceViewAsBitmap(bitmap -> bitmapArr[0] = bitmap);
            while (bitmapArr[0] == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return bitmapArr[0];
        }

        @Override
        public void onPostExecute(Bitmap bitmap) {
            mPhotoEditorView.post(() -> {
                mPhotoEditorView.setImageSource(bitmap);
            });
            mPhotoEditorView.redo_count = 0;
            mPhotoEditorView.addNewBitmap(bitmap);

            mPhotoEditorView.setFilterEffect("");
            showLoading(false);
        }
    }

    class SaveStickerAsBitmap extends AsyncTask<Void, Void, Bitmap> {
        SaveStickerAsBitmap() {
        }

        @Override
        public void onPreExecute() {
            mPhotoEditorView.getGLSurfaceView().setAlpha(0.0f);
            showLoading(true);
        }

        @Override
        public Bitmap doInBackground(Void... voidArr) {
            final Bitmap[] bitmapArr = {null};
            while (bitmapArr[0] == null) {
                try {
                    mPhotoEditor.saveStickerAsBitmap(bitmap -> bitmapArr[0] = bitmap);
                    while (bitmapArr[0] == null) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
            }
            return bitmapArr[0];
        }

        @Override
        public void onPostExecute(Bitmap bitmap) {
            mPhotoEditorView.post(() -> {
                mPhotoEditorView.setImageSource(bitmap);
            });
            mPhotoEditorView.redo_count = 0;
            mPhotoEditorView.addNewBitmap(bitmap);

            mPhotoEditorView.getStickers().clear();
            mPhotoEditorView.getGLSurfaceView().setAlpha(1.0f);
            showLoading(false);
            updateLayout();
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.N)
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 123) {
//            if (resultCode == RESULT_OK) {
//                try {
//                    InputStream openInputStream = getContentResolver().openInputStream(data.getData());
//                    decodeStream = BitmapFactory.decodeStream(openInputStream);
//                    float width = (float) decodeStream.getWidth();
//                    float height = (float) decodeStream.getHeight();
//                    float max = Math.max(width / 1280.0f, height / 1280.0f);
//                    if (max > 1.0f) {
//                        decodeStream = Bitmap.createScaledBitmap(decodeStream, (int) (width / max), (int) (height / max), false);
//                    }
//                    if (SystemUtils.rotateBitmap(decodeStream, new ExifInterface(openInputStream).getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)) != decodeStream) {
//                        decodeStream.recycle();
//                        decodeStream = null;
//                    }
//                    mPhotoEditorView.post(() -> {
//                        mPhotoEditorView.setImageSource(decodeStream);
//                    });
//                    updateLayout();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    MsgUtil.toastMsg(this, "Error: Can not open image");
//                }
//            } else {
//                finish();
//            }
//        }
//    }

    class loadBitmapFromUri extends AsyncTask<String, Bitmap, Bitmap> {
        loadBitmapFromUri() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public Bitmap doInBackground(String... strArr) {
            try {
                Uri fromFile = Uri.fromFile(new File(strArr[0]));
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fromFile);
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float max = Math.max(width / 1280.0f, height / 1280.0f);
                if (max > 1.0f) {
                    bitmap = Bitmap.createScaledBitmap(bitmap, (int) (width / max), (int) (height / max), false);
                }
                Bitmap rotateBitmap = SystemUtils.rotateBitmap(bitmap, new ExifInterface(getContentResolver().openInputStream(fromFile)).getAttributeInt(ExifInterface.TAG_ORIENTATION, 1));
                if (rotateBitmap != bitmap) {
                    bitmap.recycle();
                }
                return rotateBitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void onPostExecute(Bitmap bitmap) {
            mPhotoEditorView.post(() -> {
                mPhotoEditorView.setImageSource(bitmap);
            });
            mPhotoEditorView.redo_count = 0;
            mPhotoEditorView.addNewBitmap(bitmap);

            updateLayout();
        }
    }

    public void updateLayout() {
        mPhotoEditorView.postDelayed(() -> {
            try {
                Display defaultDisplay = getWindowManager().getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                int i = point.x;
                int height = wrapPhotoView.getHeight();
                int i2 = mPhotoEditorView.getGLSurfaceView().getRenderViewport().width;
                float f = (float) mPhotoEditorView.getGLSurfaceView().getRenderViewport().height;
                float f2 = (float) i2;
                if (((int) ((((float) i) * f) / f2)) <= height) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams.addRule(13);
                    mPhotoEditorView.setLayoutParams(layoutParams);
                    mPhotoEditorView.setVisibility(View.VISIBLE);
                } else {
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) ((((float) height) * f2) / f), -1);
                    layoutParams2.addRule(13);
                    mPhotoEditorView.setLayoutParams(layoutParams2);
                    mPhotoEditorView.setVisibility(View.VISIBLE);
                }
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
            showLoading(false);
        }, 300);
    }


    class saveBitmapAsFile extends AsyncTask<Void, String, String> {
        saveBitmapAsFile() {
        }

        @Override
        public void onPreExecute() {
            showLoading(true);
        }

        @Override
        public String doInBackground(Void... voidArr) {
            File saveBitmapAsFile = UtilsFile.saveBitmapAsFile(mPhotoEditorView.getCurrentBitmap());
            try {
                MediaScannerConnection.scanFile(getApplicationContext(), new String[]{saveBitmapAsFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                    @Override
                    public void onMediaScannerConnected() {

                    }

                    @Override
                    public void onScanCompleted(String path, Uri uri) {

                    }
                });

                return saveBitmapAsFile.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public void onPostExecute(String str) {
            showLoading(false);
            if (str == null) {
                Toast.makeText(getApplicationContext(), "Oop! Something went wrong", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(Image_Edit_Activity.this, SaveShare_ImageActivity.class);
            intent.putExtra("path", str);
            startActivity(intent);
        }
    }

    public void newStickerselect() {
        FragmentManager fm = getSupportFragmentManager();
        stickFragment = (StickFragment) fm.findFragmentByTag("mytag");
        if (stickFragment == null) {
            stickFragment = new StickFragment();
            Bundle bundle = new Bundle();
            bundle.putString("tabposition", "0");
            bundle.putString("hex", "no");
            stickFragment.setArguments(bundle);
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.sticker_grid_fragment_container, stickFragment);
            ft.addToBackStack(null);
            ft.commit();
            stickFragment.setBgSelectListner(creatStickSelectListner());
        } else {
            stickFragment.setBgSelectListner(creatStickSelectListner());
            getSupportFragmentManager().beginTransaction().show(stickFragment).commit();
        }
    }

    StickFragment.BGSelectListener creatStickSelectListner() {
        if (stickbgSlect == null) {
            stickbgSlect = () -> getSupportFragmentManager().beginTransaction().hide(stickFragment).commit();
        }
        return stickbgSlect;
    }
}