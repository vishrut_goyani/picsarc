package picsarc.picspro.picslab.photocollageeditor.interfaces;

public interface PhotoClickListener {
    void onPhotoClick(int position);
}
