package picsarc.picspro.picslab.photocollageeditor.crop.callback;

import android.graphics.Bitmap;


public interface CropCallback extends Callback {
  void onSuccess(Bitmap cropped);
}
