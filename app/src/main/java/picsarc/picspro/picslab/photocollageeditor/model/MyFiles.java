package picsarc.picspro.picslab.photocollageeditor.model;

public class MyFiles {
    String name, path;
    boolean selected;

    public MyFiles() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean isSelected() {
        return selected;
    }
}
