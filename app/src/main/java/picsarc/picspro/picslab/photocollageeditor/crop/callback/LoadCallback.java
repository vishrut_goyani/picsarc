package picsarc.picspro.picslab.photocollageeditor.crop.callback;


public interface LoadCallback extends Callback {
  void onSuccess();
}
