package picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.activities.CollageViewActivity;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.adapter.GalleryAlbumAdapter;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.adapter.GalleryListFoldersAdapter;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.model.PictureModel;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.myinterface.OnFolder;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.myinterface.OnListFolder;
import picsarc.picspro.picslab.photocollageeditor.utils.Constants;

public class PickPhotoActivity extends AppCompatActivity implements View.OnClickListener, OnFolder, OnListFolder {
    public static final String KEY_DATA_RESULT = "KEY_DATA_RESULT";
    public static final String KEY_LIMIT_MAX_IMAGE = "KEY_LIMIT_MAX_IMAGE";
    public static final String KEY_LIMIT_MIN_IMAGE = "KEY_LIMIT_MIN_IMAGE";
    GalleryAlbumAdapter galleryAlbumAdapter;
    ArrayList<PictureModel> dataAlbum = new ArrayList<>();
    ArrayList<PictureModel> dataListPhoto = new ArrayList<>();
    GridView gridViewAlbum;
    GridView gridViewListAlbum;
    HorizontalScrollView horizontalScrollView;
    LinearLayout layoutListItemSelect;
    int limitImageMax = 30;
    int limitImageMin = 2;
    GalleryListFoldersAdapter galleryListFoldersAdapter;
    ArrayList<PictureModel> listItemSelect = new ArrayList<>();
    int pWHBtnDelete;
    int pWHItemSelected;
    ArrayList<String> pathList = new ArrayList<>();
    public int position = 0;
    AlertDialog sortDialog;
    TextView txtTotalImage;
    Toolbar toolbar;
    RelativeLayout banner_container;

    private class GetItemAlbum extends AsyncTask<Void, Void, String> {
        public void onPreExecute() {
        }


        public void onProgressUpdate(Void... voidArr) {
        }

        private GetItemAlbum() {
        }

        public String doInBackground(Void... voidArr) {
            Cursor query = PickPhotoActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_data", "bucket_display_name"}, null, null, null);
            if (query == null) {
                return "";
            }
            int columnIndexOrThrow = query.getColumnIndexOrThrow("_data");
            while (query.moveToNext()) {
                String string = query.getString(columnIndexOrThrow);
                File file = new File(string);
                if (file.exists()) {
                    boolean access$000 = PickPhotoActivity.this.checkFile(file);
                    if (!PickPhotoActivity.this.Check(file.getParent(), PickPhotoActivity.this.pathList) && access$000) {
                        PickPhotoActivity.this.pathList.add(file.getParent());
                        PickPhotoActivity.this.dataAlbum.add(new PictureModel(file.getParentFile().getName(), string, file.getParent()));
                    }
                }
            }
            Collections.sort(PickPhotoActivity.this.dataAlbum);
            query.close();
            return "";
        }

        public void onPostExecute(String str) {
            PickPhotoActivity.this.gridViewAlbum.setAdapter(PickPhotoActivity.this.galleryAlbumAdapter);
        }
    }

    private class GetItemListAlbum extends AsyncTask<Void, Void, String> {
        String pathAlbum;

        public void onPreExecute() {
        }

        public void onProgressUpdate(Void... voidArr) {
        }

        GetItemListAlbum(String str) {
            this.pathAlbum = str;
        }

        @Override
        public String doInBackground(Void... voidArr) {
            File file = new File(this.pathAlbum);
            if (!file.isDirectory()) {
                return "";
            }
            for (File file2 : file.listFiles()) {
                if (file2.exists()) {
                    boolean access$000 = PickPhotoActivity.this.checkFile(file2);
                    if (!file2.isDirectory() && access$000) {
                        dataListPhoto.add(new PictureModel(file2.getName(), file2.getAbsolutePath(), file2.getAbsolutePath()));
                        publishProgress(new Void[0]);
                    }
                }
            }
            return "";
        }

        @Override
        public void onPostExecute(String str) {
            try {
                Collections.sort(PickPhotoActivity.this.dataListPhoto, new Comparator<PictureModel>() {
                    @Override
                    public int compare(PictureModel pictureModel, PictureModel pictureModel2) {
                        File file = new File(pictureModel.getPathFolder());
                        File file2 = new File(pictureModel2.getPathFolder());
                        if (file.lastModified() > file2.lastModified()) {
                            return -1;
                        }
                        return file.lastModified() < file2.lastModified() ? 1 : 0;
                    }
                });
            } catch (Exception e) {
            }
            galleryListFoldersAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.piclist_activity_album);

        init();
        ((TextView) toolbar.findViewById(R.id.app_title)).setText("Albums");
        toolbar.findViewById(R.id.imgsave).setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.limitImageMax = extras.getInt(KEY_LIMIT_MAX_IMAGE, 9);
            this.limitImageMin = extras.getInt(KEY_LIMIT_MIN_IMAGE, 2);
            if (this.limitImageMin > this.limitImageMax) {
                finish();
            }
            if (this.limitImageMin < 1) {
                finish();
            }
        }
        this.pWHItemSelected = (((int) ((((float) getDisplayInfo(this).heightPixels) / 100.0f) * 25.0f)) / 100) * 80;
        this.pWHBtnDelete = (this.pWHItemSelected / 100) * 25;
        getSupportActionBar().setTitle(R.string.text_title_activity_album);
        this.gridViewListAlbum = findViewById(R.id.gridViewListAlbum);
        this.txtTotalImage = findViewById(R.id.txtTotalImage);
        findViewById(R.id.btnDone).setOnClickListener(this);
        this.layoutListItemSelect = findViewById(R.id.layoutListItemSelect);
        this.horizontalScrollView = findViewById(R.id.horizontalScrollView);
        this.horizontalScrollView.getLayoutParams().height = this.pWHItemSelected;
        this.gridViewAlbum = findViewById(R.id.gridViewAlbum);

        try {
            Collections.sort(this.dataAlbum, (pictureModel, pictureModel2) -> pictureModel.getName().compareToIgnoreCase(pictureModel2.getName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.galleryAlbumAdapter = new GalleryAlbumAdapter(this, R.layout.piclist_row_album, this.dataAlbum);
        this.galleryAlbumAdapter.setOnItem(this);
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new GetItemAlbum().execute(new Void[0]);
        } else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 1001);
        }
        updateTxtTotalImage();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        banner_container = findViewById(R.id.banner_container);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean isPermissionGranted(String str) {
        return ContextCompat.checkSelfPermission(this, str) == 0;
    }

    private void requestPermission(String str, int i) {
        ActivityCompat.shouldShowRequestPermissionRationale(this, str);
        ActivityCompat.requestPermissions(this, new String[]{str}, i);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i == 1001) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                finish();
            } else {
                new GetItemAlbum().execute(new Void[0]);
            }
        } else if (i == 1002 && iArr.length > 0) {
            int i2 = iArr[0];
        }
    }


    public boolean Check(String str, ArrayList<String> arrayList) {
        return !arrayList.isEmpty() && arrayList.contains(str);
    }

    public void showDialogSortAlbum() {
        String[] stringArray = getResources().getStringArray(R.array.array_sort_value);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.text_title_dialog_sort_by_album));
        builder.setSingleChoiceItems(stringArray, position, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    position = i;
                    Collections.sort(dataAlbum, (pictureModel, pictureModel2) -> pictureModel.getName().compareToIgnoreCase(pictureModel2.getName()));
                    refreshGridViewAlbum();
                    break;
                case 1:
                    position = i;
                    doinBackground();
                    break;
                case 2:
                    position = i;
                    Collections.sort(dataAlbum, (pictureModel, pictureModel2) -> {
                        int i1 = (PickPhotoActivity.getFolderSize(new File(pictureModel.getPathFolder())) > PickPhotoActivity.getFolderSize(new File(pictureModel2.getPathFolder())) ? 1 : (PickPhotoActivity.getFolderSize(new File(pictureModel.getPathFolder())) == PickPhotoActivity.getFolderSize(new File(pictureModel2.getPathFolder())) ? 0 : -1));
                        if (i1 > 0) {
                            return -1;
                        }
                        return i1 < 0 ? 1 : 0;
                    });
                    refreshGridViewAlbum();
                    break;
            }
            PickPhotoActivity.this.sortDialog.dismiss();
        });
        this.sortDialog = builder.create();
        this.sortDialog.show();
    }

    public void refreshGridViewAlbum() {
        this.galleryAlbumAdapter = new GalleryAlbumAdapter(this, R.layout.piclist_row_album, this.dataAlbum);
        this.galleryAlbumAdapter.setOnItem(this);
        this.gridViewAlbum.setAdapter(this.galleryAlbumAdapter);
        this.gridViewAlbum.setVisibility(View.GONE);
        this.gridViewAlbum.setVisibility(View.VISIBLE);
    }

    public void showDialogSortListAlbum() {
        String[] stringArray = getResources().getStringArray(R.array.array_sort_value);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.text_title_dialog_sort_by_photo));
        builder.setSingleChoiceItems(stringArray, this.position, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    position = i;
                    doinBackgroundPhoto(i);
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
        });
        this.sortDialog = builder.create();
        this.sortDialog.show();
    }

    public void refreshGridViewListAlbum() {
        this.galleryListFoldersAdapter = new GalleryListFoldersAdapter(this, R.layout.piclist_row_list_album, this.dataListPhoto);
        this.galleryListFoldersAdapter.setOnListFolder(this);
        this.gridViewListAlbum.setAdapter(this.galleryListFoldersAdapter);
        this.gridViewListAlbum.setVisibility(View.GONE);
        this.gridViewListAlbum.setVisibility(View.VISIBLE);
    }

    public static long getFolderSize(File file) {
        File[] listFiles;
        boolean z;
        if (file == null || !file.exists() || (listFiles = file.listFiles()) == null || listFiles.length <= 0) {
            return 0;
        }
        long j = 0;
        for (File file2 : listFiles) {
            if (file2.isFile()) {
                int i = 0;
                while (true) {
                    if (i >= Constants.FORMAT_IMAGE.size()) {
                        z = false;
                        break;
                    } else if (file2.getName().endsWith(Constants.FORMAT_IMAGE.get(i))) {
                        z = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (z) {
                    j++;
                }
            }
        }
        return j;
    }


    public void addItemSelect(final PictureModel pictureModel) {
        pictureModel.setId(this.listItemSelect.size());
        this.listItemSelect.add(pictureModel);
        updateTxtTotalImage();
        final View inflate = View.inflate(this, R.layout.piclist_item_selected, null);
        ImageView imageView = inflate.findViewById(R.id.imageItem);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ((RequestBuilder) Glide.with((Activity) this).load(pictureModel.getPathFile()).placeholder(R.drawable.piclist_icon_default)).into(imageView);
        inflate.findViewById(R.id.btnDelete).setOnClickListener(view -> {
            layoutListItemSelect.removeView(inflate);
            listItemSelect.remove(pictureModel);
            updateTxtTotalImage();
        });
        this.layoutListItemSelect.addView(inflate);
        inflate.startAnimation(AnimationUtils.loadAnimation(this, R.anim.abc_fade_in));
        sendScroll();
    }


    public void updateTxtTotalImage() {
        this.txtTotalImage.setText(String.format(getResources().getString(R.string.text_images), new Object[]{Integer.valueOf(this.listItemSelect.size())}));
    }

    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(() -> handler.post(() -> horizontalScrollView.fullScroll(66))).start();
    }

    public void showListAlbum(String str) {
        getSupportActionBar().setTitle(new File(str).getName());
        this.galleryListFoldersAdapter = new GalleryListFoldersAdapter(this, R.layout.piclist_row_list_album, this.dataListPhoto);
        this.galleryListFoldersAdapter.setOnListFolder(this);
        this.gridViewListAlbum.setAdapter(this.galleryListFoldersAdapter);
        this.gridViewListAlbum.setVisibility(View.VISIBLE);
        new GetItemListAlbum(str).execute(new Void[0]);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnDone) {
            ArrayList<String> listString = getListString(listItemSelect);
            if (listString.size() >= limitImageMin) {
                done(listString);
                return;
            }
            Toast.makeText(this, "Please select at lease " + this.limitImageMin + " images", Toast.LENGTH_SHORT).show();
        }
    }

    private void done(ArrayList<String> arrayList) {
        Intent intent = new Intent(this, CollageViewActivity.class);
        intent.putStringArrayListExtra(KEY_DATA_RESULT, arrayList);
        startActivity(intent);
    }

    public ArrayList<String> getListString(ArrayList<PictureModel> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList2.add(arrayList.get(i).getPathFile());
        }
        return arrayList2;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pick_image, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.btnSort) {
            if (this.gridViewListAlbum.getVisibility() == View.GONE) {
                showDialogSortAlbum();
            } else {
                showDialogSortListAlbum();
            }
        } else if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public boolean checkFile(File file) {
        if (file == null) {
            return false;
        }
        if (!file.isFile()) {
            return true;
        }
        String name = file.getName();
        if (name.startsWith(".") || file.length() == 0) {
            return false;
        }
        for (int i = 0; i < Constants.FORMAT_IMAGE.size(); i++) {
            if (name.endsWith(Constants.FORMAT_IMAGE.get(i))) {
                return true;
            }
        }
        return false;
    }

    public void onBackPressed() {
        if (this.gridViewListAlbum.getVisibility() == View.VISIBLE) {
            this.dataListPhoto.clear();
            this.galleryListFoldersAdapter.notifyDataSetChanged();
            this.gridViewListAlbum.setVisibility(View.GONE);
            getSupportActionBar().setTitle(getResources().getString(R.string.text_title_activity_album));
            return;
        }
        super.onBackPressed();
    }

    public static DisplayMetrics getDisplayInfo(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }


    public void doinBackgroundPhoto(final int i) {
        new AsyncTask<String, String, Void>() {

            public void onPreExecute() {
                super.onPreExecute();
            }

            public Void doInBackground(String... strArr) {
                if (i == 0) {
                    try {
                        Collections.sort(PickPhotoActivity.this.dataListPhoto, new Comparator<PictureModel>() {
                            public int compare(PictureModel pictureModel, PictureModel pictureModel2) {
                                return pictureModel.getName().compareToIgnoreCase(pictureModel2.getName());
                            }
                        });
                        return null;
                    } catch (Exception e) {
                        return null;
                    }
                } else if (i == 1) {
                    Collections.sort(PickPhotoActivity.this.dataListPhoto, new Comparator<PictureModel>() {
                        public int compare(PictureModel pictureModel, PictureModel pictureModel2) {
                            int i = (PickPhotoActivity.getFolderSize(new File(pictureModel.getPathFolder())) > PickPhotoActivity.getFolderSize(new File(pictureModel2.getPathFolder())) ? 1 : (PickPhotoActivity.getFolderSize(new File(pictureModel.getPathFolder())) == PickPhotoActivity.getFolderSize(new File(pictureModel2.getPathFolder())) ? 0 : -1));
                            if (i > 0) {
                                return -1;
                            }
                            return i < 0 ? 1 : 0;
                        }
                    });
                    return null;
                } else if (i != 2) {
                    return null;
                } else {
                    Collections.sort(PickPhotoActivity.this.dataListPhoto, new Comparator<PictureModel>() {
                        public int compare(PictureModel pictureModel, PictureModel pictureModel2) {
                            File file = new File(pictureModel.getPathFolder());
                            File file2 = new File(pictureModel2.getPathFolder());
                            if (file.lastModified() > file2.lastModified()) {
                                return -1;
                            }
                            return file.lastModified() < file2.lastModified() ? 1 : 0;
                        }
                    });
                    return null;
                }
            }


            public void onPostExecute(Void voidR) {
                super.onPostExecute(voidR);
                PickPhotoActivity.this.refreshGridViewListAlbum();
            }
        }.execute(new String[0]);
    }


    public void doinBackground() {
        new AsyncTask<String, String, Void>() {

            public void onPreExecute() {
                super.onPreExecute();
            }

            public Void doInBackground(String... strArr) {
                Collections.sort(PickPhotoActivity.this.dataAlbum, new Comparator<PictureModel>() {
                    public int compare(PictureModel pictureModel, PictureModel pictureModel2) {
                        File file = new File(pictureModel.getPathFolder());
                        File file2 = new File(pictureModel2.getPathFolder());
                        if (file.lastModified() > file2.lastModified()) {
                            return -1;
                        }
                        return file.lastModified() < file2.lastModified() ? 1 : 0;
                    }
                });
                return null;
            }


            public void onPostExecute(Void voidR) {
                super.onPostExecute(voidR);
                PickPhotoActivity.this.refreshGridViewAlbum();
            }
        }.execute(new String[0]);
    }

    public void OnItemAlbumClick(int i) {
        showListAlbum(this.dataAlbum.get(i).getPathFolder());
    }

    public void OnItemListAlbumClick(PictureModel pictureModel) {
        if (this.listItemSelect.size() < this.limitImageMax) {
            addItemSelect(pictureModel);
            return;
        }
        Toast.makeText(this, "Limit " + this.limitImageMax + " images", Toast.LENGTH_SHORT).show();
    }
}
