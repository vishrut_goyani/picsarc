package picsarc.picspro.picslab.photocollageeditor.tools;

public enum ToolType {
    NONE,
    BRUSH,
    TEXT,
    FILTER,
    CROP,
    STICKER,
    ADJUST,
    OVERLAY,
    INSTA,
    SPLASH,
    BLUR,
    MOSAIC,
    PIECE,
    LAYOUT,
    BORDER,
    RATIO,
    REPLACE_IMG,
    ROTATE,
    H_FLIP,
    V_FLIP,
    BACKGROUND
}
