package picsarc.picspro.picslab.photocollageeditor.sticker.event;

public class EventStickerIconFlipHorizontally extends AbstractFlipEventStickerIcon {
    protected int getFlipDirection() {
        return 1;
    }
}
