package picsarc.picspro.picslab.photocollageeditor.features.draw;


import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.DrawBitmapModel;

public interface BrushMagicListener {
    void onMagicChanged(DrawBitmapModel drawBitmapModel);
}
