package picsarc.picspro.picslab.photocollageeditor.features.beauty;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.ItemTouchHelper;

import org.wysaid.nativePort.CGEDeformFilterWrapper;
import org.wysaid.nativePort.CGEImageHandler;
import org.wysaid.texUtils.TextureRenderer;
import org.wysaid.view.ImageGLSurfaceView;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.OnSaveBitmap;
import picsarc.picspro.picslab.photocollageeditor.picsart_photoeditor.PhotoEditorView;
import picsarc.picspro.picslab.photocollageeditor.sticker.BeautyPicSticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.Sticker;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerIconBitmap;
import picsarc.picspro.picslab.photocollageeditor.sticker.StickerView;
import picsarc.picspro.picslab.photocollageeditor.sticker.event.EventZoomIconStickerIcon;
import picsarc.picspro.picslab.photocollageeditor.utils.SystemUtils;
import picsarc.picspro.picslab.photocollageeditor.widgets.SpinProgressSeekBar;

public class PicsArtBeautyDialog extends DialogFragment {
    private Bitmap bitmap;
    private ImageView boobs;
    private ImageView compare;
    public int currentType = 7;
    private ImageView face;
    public ImageGLSurfaceView glSurfaceView;
    public SeekBar intensitySmooth;
    private SpinProgressSeekBar intensityTwoDirection;
    public RelativeLayout loadingView;
    private List<Retouch> lstRetouch;
    public CGEDeformFilterWrapper mDeformWrapper;
    private float mTouchRadiusForWaist;
    private LinearLayout magicLayout;
    public OnBeautySave onBeautySave;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.resetWaist) {
                PicsArtBeautyDialog.this.glSurfaceView.flush(true, new Runnable() {
                    public void run() {
                        if (PicsArtBeautyDialog.this.mDeformWrapper != null) {
                            PicsArtBeautyDialog.this.mDeformWrapper.restore();
                            PicsArtBeautyDialog.this.glSurfaceView.requestRender();
                        }
                    }
                });
            } else if (id == R.id.wrapBoobs) {
                PicsArtBeautyDialog.this.showAdjustBoobs();
            } else if (id == R.id.wrapFace) {
                PicsArtBeautyDialog.this.showAdjustFace();
            } else if (id == R.id.wrapHip) {
                PicsArtBeautyDialog.this.showAdjustHipOne();
            } else if (id == R.id.wrapWaist) {
                PicsArtBeautyDialog.this.showWaist();
            }
        }
    };
    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (PicsArtBeautyDialog.this.intensitySmooth.getProgress() == 0) {
                PicsArtBeautyDialog.this.glSurfaceView.setFilterWithConfig("");
                return;
            }
            ImageGLSurfaceView imageGLSurfaceView = PicsArtBeautyDialog.this.glSurfaceView;
            imageGLSurfaceView.setFilterWithConfig(MessageFormat.format("@beautify face 1 {0} 640", new Object[]{PicsArtBeautyDialog.this.intensitySmooth.getProgress() + ""}));
        }
    };
    public PhotoEditorView photoEditorView;
    private RelativeLayout resetWaist;
    private ImageView seat;
    public float startX;
    public float startY;
    private TextView tvBoobs;
    private TextView tvFace;
    private TextView tvSeat;
    private TextView tvWaise;
    private ViewGroup viewGroup;
    private ImageView waise;
    private RelativeLayout wrapBoobs;
    private RelativeLayout wrapFace;
    private RelativeLayout wrapHip;
    private RelativeLayout wrapWaist;

    public interface OnBeautySave {
        void onBeautySave(Bitmap bitmap);
    }

    public void setBitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public void showWaist() {
        saveCurrentState();
        selectFunction(1);
        this.magicLayout.setVisibility(View.GONE);
        this.photoEditorView.setHandlingSticker(null);
        this.photoEditorView.setDrawCirclePoint(true);
        this.resetWaist.setVisibility(View.VISIBLE);
        this.intensityTwoDirection.setVisibility(View.GONE);
        this.currentType = 3;
        this.intensityTwoDirection.setDegreeRange(0, 100);
        this.mTouchRadiusForWaist = (float) SystemUtils.dpToPx(getContext(), 20);
        this.photoEditorView.setCircleRadius((int) this.mTouchRadiusForWaist);
        this.photoEditorView.getStickers().clear();
    }


    private void saveCurrentState() {
        new SaveCurrentState().execute();
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showAdjustBoobs() {
        saveCurrentState();
        this.intensityTwoDirection.setVisibility(View.VISIBLE);
        this.intensityTwoDirection.setDegreeRange(-30, 30);
        this.resetWaist.setVisibility(View.GONE);
        this.photoEditorView.setDrawCirclePoint(false);
        selectFunction(0);
        this.magicLayout.setVisibility(View.GONE);
        this.currentType = 7;
        this.intensityTwoDirection.setDegreeRange(0, 100);
        this.photoEditorView.getStickers().clear();
        this.photoEditorView.addSticker(new BeautyPicSticker(getContext(), 0, ContextCompat.getDrawable(getContext(), R.drawable.circle)));
        this.photoEditorView.addSticker(new BeautyPicSticker(getContext(), 1, ContextCompat.getDrawable(getContext(), R.drawable.circle)));
    }

    public void showAdjustFace() {
        saveCurrentState();
        this.intensityTwoDirection.setVisibility(View.VISIBLE);
        this.intensityTwoDirection.setDegreeRange(-15, 15);
        this.resetWaist.setVisibility(View.GONE);
        this.photoEditorView.setDrawCirclePoint(false);
        selectFunction(3);
        this.currentType = 4;
        this.magicLayout.setVisibility(View.GONE);
        this.intensityTwoDirection.setDegreeRange(0, 100);
        this.photoEditorView.getStickers().clear();
        this.photoEditorView.addSticker(new BeautyPicSticker(getContext(), 4, ContextCompat.getDrawable(getContext(), R.drawable.chin)));
    }


    public void showAdjustHipOne() {
        saveCurrentState();
        this.intensityTwoDirection.setVisibility(View.VISIBLE);
        this.intensityTwoDirection.setDegreeRange(-30, 30);
        this.resetWaist.setVisibility(View.GONE);
        this.photoEditorView.setDrawCirclePoint(false);
        selectFunction(2);
        this.intensityTwoDirection.setDegreeRange(0, 100);
        this.currentType = 9;
        this.photoEditorView.getStickers().clear();
        this.photoEditorView.addSticker(new BeautyPicSticker(getContext(), 2, ContextCompat.getDrawable(getContext(), R.drawable.hip_1)));
    }


    public void hideAllFunction() {
        this.intensityTwoDirection.setVisibility(View.GONE);
        this.resetWaist.setVisibility(View.GONE);
        this.magicLayout.setVisibility(View.GONE);
        this.loadingView.setVisibility(View.GONE);
    }

    public void setOnBeautySave(OnBeautySave onBeautySave2) {
        this.onBeautySave = onBeautySave2;
    }

    public static PicsArtBeautyDialog show(@NonNull AppCompatActivity appCompatActivity, Bitmap bitmap2, OnBeautySave onBeautySave2) {
        PicsArtBeautyDialog picsArtBeautyDialog = new PicsArtBeautyDialog();
        picsArtBeautyDialog.setBitmap(bitmap2);
        picsArtBeautyDialog.setOnBeautySave(onBeautySave2);
        picsArtBeautyDialog.show(appCompatActivity.getSupportFragmentManager(), "PicBeautyDialog");
        return picsArtBeautyDialog;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup2, @Nullable Bundle bundle) {
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setFlags(1024, 1024);
        View inflate = layoutInflater.inflate(R.layout.beauty_layout, viewGroup2, false);
        this.intensityTwoDirection = inflate.findViewById(R.id.intensityTwoDirection);
        this.intensityTwoDirection.setDegreeRange(-20, 20);
        this.photoEditorView = inflate.findViewById(R.id.photoEditorView);
        this.glSurfaceView = this.photoEditorView.getGLSurfaceView();
        this.loadingView = inflate.findViewById(R.id.loadingView);
        this.boobs = inflate.findViewById(R.id.boobs);
        this.wrapBoobs = inflate.findViewById(R.id.wrapBoobs);
        this.wrapBoobs.setOnClickListener(this.onClickListener);
        this.tvBoobs = inflate.findViewById(R.id.tvBoobs);
        this.waise = inflate.findViewById(R.id.waist);
        this.wrapWaist = inflate.findViewById(R.id.wrapWaist);
        this.wrapWaist.setOnClickListener(this.onClickListener);
        this.tvWaise = inflate.findViewById(R.id.tvWaist);
        this.resetWaist = inflate.findViewById(R.id.resetWaist);
        this.resetWaist.setOnClickListener(this.onClickListener);
        this.seat = inflate.findViewById(R.id.seat);
        this.wrapHip = inflate.findViewById(R.id.wrapHip);
        this.wrapHip.setOnClickListener(this.onClickListener);
        this.tvSeat = inflate.findViewById(R.id.tvSeat);
        this.face = inflate.findViewById(R.id.face);
        this.wrapFace = inflate.findViewById(R.id.wrapFace);
        this.wrapFace.setOnClickListener(this.onClickListener);
        this.tvFace = inflate.findViewById(R.id.tvFace);
        this.magicLayout = inflate.findViewById(R.id.magicLayout);
        this.viewGroup = viewGroup2;
        this.intensitySmooth = inflate.findViewById(R.id.intensitySmooth);
        this.intensitySmooth.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.lstRetouch = new ArrayList();
        this.lstRetouch.add(new Retouch(this.boobs, this.tvBoobs, R.drawable.boobs, R.drawable.boobs));
        this.lstRetouch.add(new Retouch(this.waise, this.tvWaise, R.drawable.seat, R.drawable.seat));
        this.lstRetouch.add(new Retouch(this.seat, this.tvSeat, R.drawable.seat, R.drawable.seat));
        this.lstRetouch.add(new Retouch(this.face, this.tvFace, R.drawable.beauty_face, R.drawable.beauty_face));
//        this.intensityTwoDirection.setScrollingListener(new SeekBarDegree.ScrollingListener() {
//            public void onScrollStart() {
//                Iterator<Sticker> it = PicsArtBeautyDialog.this.photoEditorView.getStickers().iterator();
//                while (it.hasNext()) {
//                    ((BeautyPicSticker) it.next()).updateRadius();
//                }
//            }
//
//            public void onScroll(final int currentDegrees) {
//                TextureRenderer.Viewport renderViewport = PicsArtBeautyDialog.this.glSurfaceView.getRenderViewport();
//                final float w = (float) renderViewport.width;
//                final float h = (float) renderViewport.height;
//                if (PicsArtBeautyDialog.this.currentType == 7) {
//                    PicsArtBeautyDialog.this.glSurfaceView.lazyFlush(true, new Runnable() {
//                        public final void run() {
//                            if (mDeformWrapper != null) {
//                                mDeformWrapper.restore();
//                                for (Sticker next : photoEditorView.getStickers()) {
//                                    PointF mappedCenterPoint2 = ((BeautyPicSticker) next).getMappedCenterPoint2();
//                                    for (int i2 = 0; i2 < Math.abs(currentType); i2++) {
//                                        if (currentType > 0) {
//                                            mDeformWrapper.bloatDeform(mappedCenterPoint2.x, mappedCenterPoint2.y, w, h, (float) (next.getWidth() / 2), 0.03f);
//                                        } else if (currentType < 0) {
//                                            mDeformWrapper.wrinkleDeform(mappedCenterPoint2.x, mappedCenterPoint2.y, w, h, (float) (next.getWidth() / 2), 0.03f);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    });
//                } else if (PicsArtBeautyDialog.this.currentType == 9) {
//                    PicsArtBeautyDialog.this.glSurfaceView.lazyFlush(true, new Runnable() {
//                        public void run() {
//                            if (PicsArtBeautyDialog.this.mDeformWrapper != null) {
//                                PicsArtBeautyDialog.this.mDeformWrapper.restore();
//                                Iterator<Sticker> it = PicsArtBeautyDialog.this.photoEditorView.getStickers().iterator();
//                                while (it.hasNext()) {
//                                    BeautyPicSticker beautyPicSticker = (BeautyPicSticker) it.next();
//                                    PointF mappedCenterPoint2 = beautyPicSticker.getMappedCenterPoint2();
//                                    RectF mappedBound = beautyPicSticker.getMappedBound();
//                                    for (int i = 0; i < Math.abs(currentDegrees); i++) {
//                                        if (currentDegrees > 0) {
//                                            float f = (float) 20;
//                                            float f2 = f;
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(mappedBound.right - f, mappedCenterPoint2.y, mappedBound.right + f, mappedCenterPoint2.y, f, f2, (float) beautyPicSticker.getRadius(), 0.01f);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(mappedBound.left + f2, mappedCenterPoint2.y, mappedBound.left - f2, mappedCenterPoint2.y, f, f2, (float) beautyPicSticker.getRadius(), 0.01f);
//                                        } else {
//                                            float f3 = (float) 20;
//                                            float f4 = f3;
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(mappedBound.right + f3, mappedCenterPoint2.y, mappedBound.right - f3, mappedCenterPoint2.y, w, h, (float) beautyPicSticker.getRadius(), 0.01f);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(mappedBound.left - f4, mappedCenterPoint2.y, mappedBound.left + f4, mappedCenterPoint2.y, w, h, (float) beautyPicSticker.getRadius(), 0.01f);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    });
//                } else if (PicsArtBeautyDialog.this.currentType == 4)
//                    PicsArtBeautyDialog.this.glSurfaceView.lazyFlush(true, new Runnable() {
//                        public void run() {
//                            if (PicsArtBeautyDialog.this.mDeformWrapper == null)
//                                return;
//                            PicsArtBeautyDialog.this.mDeformWrapper.restore();
//                            Iterator<Sticker> iterator = PicsArtBeautyDialog.this.photoEditorView.getStickers().iterator();
//                            label17:
//                            while (iterator.hasNext()) {
//                                BeautyPicSticker beautyPicSticker = (BeautyPicSticker) iterator.next();
//                                PointF pointF = beautyPicSticker.getMappedCenterPoint2();
//                                RectF rectF = beautyPicSticker.getMappedBound();
//                                int i = beautyPicSticker.getRadius() / 2;
//                                float f1 = (rectF.left + pointF.x) / 2.0F;
//                                float f2 = rectF.left + (f1 - rectF.left) / 2.0F;
//                                float f3 = (rectF.bottom + rectF.top) / 2.0F;
//                                float f4 = rectF.top + (f3 - rectF.top) / 2.0F;
//                                float f5 = (rectF.right + pointF.x) / 2.0F;
//                                float f6 = rectF.right - (rectF.right - f5) / 2.0F;
//                                float f7 = (rectF.bottom + rectF.top) / 2.0F;
//                                float f8 = rectF.top + (f7 - rectF.top) / 2.0F;
//                                int j = 0;
//                                Iterator<Sticker> iterator1 = iterator;
//                                while (true) {
//                                    iterator = iterator1;
//                                    if (j < Math.abs(currentDegrees)) {
//                                        if (currentDegrees > 0) {
//                                            CGEDeformFilterWrapper cGEDeformFilterWrapper = PicsArtBeautyDialog.this.mDeformWrapper;
//                                            float f9 = rectF.right;
//                                            float f10 = rectF.top;
//                                            float f11 = rectF.right;
//                                            float f12 = i;
//                                            cGEDeformFilterWrapper.forwardDeform(f9, f10, f11 - f12, rectF.top, w, h, beautyPicSticker.getRadius(), 0.002F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f6, f8, f6 - f12, f8, w, h, beautyPicSticker.getRadius(), 0.005F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f5, f7, f5 - f12, f7, w, h, beautyPicSticker.getRadius(), 0.007F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(rectF.left, rectF.top, rectF.left + f12, rectF.top, w, h, beautyPicSticker.getRadius(), 0.002F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f2, f4, f2 + f12, f4, w, h, beautyPicSticker.getRadius(), 0.005F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f1, f3, f1 + f12, f3, w, h, beautyPicSticker.getRadius(), 0.007F);
//                                        } else {
//                                            CGEDeformFilterWrapper cGEDeformFilterWrapper = PicsArtBeautyDialog.this.mDeformWrapper;
//                                            float f9 = rectF.right;
//                                            float f10 = rectF.top;
//                                            float f11 = rectF.right;
//                                            float f12 = i;
//                                            cGEDeformFilterWrapper.forwardDeform(f9, f10, f11 + f12, rectF.top, w, h, beautyPicSticker.getRadius(), 0.002F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f6, f8, f6 + f12, f8, w, h, beautyPicSticker.getRadius(), 0.005F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f5, f7, f5 + f12, f7, w, h, beautyPicSticker.getRadius(), 0.007F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(rectF.left + f12, rectF.top, rectF.left, rectF.top, w, h, beautyPicSticker.getRadius(), 0.002F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f2, f4, f2 - f12, f4, w, h, beautyPicSticker.getRadius(), 0.005F);
//                                            PicsArtBeautyDialog.this.mDeformWrapper.forwardDeform(f1, f3, f1 - f12, f3, w, h, beautyPicSticker.getRadius(), 0.007F);
//                                        }
//                                        j++;
//                                        continue;
//                                    }
//                                    continue label17;
//                                }
//                            }
//                        }
//                    });
//            }
//
//
//            public void onScrollEnd() {
//                PicsArtBeautyDialog.this.glSurfaceView.requestRender();
//            }
//        });
        StickerIconBitmap stickerIconBitmap = new StickerIconBitmap(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_ic_scale_black_18dp), 3, StickerIconBitmap.ZOOM);
        stickerIconBitmap.setIconEvent(new EventZoomIconStickerIcon());
        StickerIconBitmap stickerIconBitmap2 = new StickerIconBitmap(ContextCompat.getDrawable(getContext(), R.drawable.ic_sticker_ic_scale_black_2_18dp), 2, StickerIconBitmap.ZOOM);
        stickerIconBitmap2.setIconEvent(new EventZoomIconStickerIcon());
        this.photoEditorView.setIcons(Arrays.asList(stickerIconBitmap, stickerIconBitmap2));
        this.photoEditorView.setBackgroundColor(-16777216);
        this.photoEditorView.setLocked(false);
        this.photoEditorView.setConstrained(true);
        this.photoEditorView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            public void onStickerAdded(@NonNull Sticker sticker) {
            }

            public void onStickerClicked(@NonNull Sticker sticker) {
            }

            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
            }

            public void onStickerDragFinished() {
            }

            public void onStickerFlipped() {
            }

            public void onStickerTouchOutside() {
            }

            public void onStickerTouchedDown() {
            }

            public void onStickerZoomFinished() {
            }

            public void onTouchUpForBeauty() {
            }

            public void onStickerDeleted() {
                PicsArtBeautyDialog.this.loadingView.setVisibility(View.GONE);
            }

            public void onTouchDownForBeauty(float f, float f2) {
                PicsArtBeautyDialog.this.startX = f;
                PicsArtBeautyDialog.this.startY = f2;
            }

            public void onTouchDragForBeauty(float f, float f2) {
                TextureRenderer.Viewport renderViewport = PicsArtBeautyDialog.this.glSurfaceView.getRenderViewport();
                float f3 = (float) renderViewport.height;
                PicsArtBeautyDialog.this.glSurfaceView.lazyFlush(true, new Runnable() {


                    public final void run() {

                        if (mDeformWrapper != null) {
                            mDeformWrapper.forwardDeform(PicsArtBeautyDialog.this.startX, PicsArtBeautyDialog.this.startY, f, f2, renderViewport.width, f3, 200.0f, 0.02f);
                        }
                    }
                });
                PicsArtBeautyDialog.this.startX = f;
                PicsArtBeautyDialog.this.startY = f2;
            }


        });
        this.compare = inflate.findViewById(R.id.compare);
        this.compare.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getActionMasked()) {
                    case 0:
                        photoEditorView.getGLSurfaceView().setAlpha(0.0f);
                        return true;
                    case 1:
                        photoEditorView.getGLSurfaceView().setAlpha(1.0f);
                        return false;
                    default:
                        return true;
                }

            }
        });
        inflate.findViewById(R.id.imgSave).setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                new SaveCurrentState(true).execute(new Void[0]);
            }
        });
        inflate.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                PicsArtBeautyDialog.this.dismiss();
            }
        });
        this.photoEditorView.setImageSource(this.bitmap, new ImageGLSurfaceView.OnSurfaceCreatedCallback() {
            public final void surfaceCreated() {

                if (bitmap == null) {
                    return;
                }
                glSurfaceView.setImageBitmap(bitmap);
                glSurfaceView.queueEvent(new Runnable() {
                    public final void run() {

                        float width = (float) bitmap.getWidth();
                        float height = (float) bitmap.getHeight();
                        float min = Math.min(((float) glSurfaceView.getRenderViewport().width) / width, ((float) glSurfaceView.getRenderViewport().height) / height);
                        if (min < 1.0f) {
                            width *= min;
                            height *= min;
                        }
                        mDeformWrapper = CGEDeformFilterWrapper.create((int) width, (int) height, 10.0f);
                        mDeformWrapper.setUndoSteps(ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                        if (mDeformWrapper != null) {
                            CGEImageHandler imageHandler = glSurfaceView.getImageHandler();
                            imageHandler.setFilterWithAddres(mDeformWrapper.getNativeAddress());
                            imageHandler.processFilters();
                        }
                    }
                });
            }
        });
        this.photoEditorView.post(new Runnable() {
            public final void run() {

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(glSurfaceView.getRenderViewport().width, glSurfaceView.getRenderViewport().height);
                layoutParams.addRule(13);
                photoEditorView.setLayoutParams(layoutParams);
            }
        });
        hideAllFunction();
        return inflate;
    }


    private void selectFunction(int i) {
        for (int i2 = 0; i2 < this.lstRetouch.size(); i2++) {
            if (i2 == i) {
                Retouch retouch = this.lstRetouch.get(i2);
                retouch.imageView.setImageResource(retouch.drawableSelected);
                retouch.textView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorPrimary));
            } else {
                Retouch retouch2 = this.lstRetouch.get(i2);
                retouch2.imageView.setImageResource(retouch2.drawable);
                retouch2.textView.setTextColor(ContextCompat.getColor(getContext(), R.color.unselected_color));
            }
        }
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(-16777216));
        }
    }

    class Retouch {
        int drawable;
        int drawableSelected;
        ImageView imageView;
        TextView textView;

        Retouch(ImageView imageView2, TextView textView2, int i, int i2) {
            this.drawable = i;
            this.drawableSelected = i2;
            this.imageView = imageView2;
            this.textView = textView2;
        }
    }

    class SaveCurrentState extends AsyncTask<Void, Void, Bitmap> {
        boolean isCloseDialog;

        SaveCurrentState() {
        }

        SaveCurrentState(boolean z) {
            this.isCloseDialog = z;
        }


        public void onPreExecute() {
            PicsArtBeautyDialog.this.getDialog().getWindow().setFlags(16, 16);
            PicsArtBeautyDialog.this.loadingView.setVisibility(View.VISIBLE);
        }


        public Bitmap doInBackground(Void... voidArr) {
            final Bitmap[] bitmapArr = {null};
            PicsArtBeautyDialog.this.photoEditorView.saveGLSurfaceViewAsBitmap(new OnSaveBitmap() {
                public void onBitmapReady(Bitmap bitmap) {
                    bitmapArr[0] = bitmap;
                }
            });
            while (bitmapArr[0] == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return bitmapArr[0];
        }


        public void onPostExecute(Bitmap bitmap) {
            PicsArtBeautyDialog.this.photoEditorView.setImageSource(bitmap);
            PicsArtBeautyDialog.this.loadingView.setVisibility(View.GONE);
            try {
                PicsArtBeautyDialog.this.getDialog().getWindow().clearFlags(16);
            } catch (Exception e) {
            }
            PicsArtBeautyDialog.this.glSurfaceView.flush(true, new Runnable() {
                public final void run() {
                    if (PicsArtBeautyDialog.this.mDeformWrapper != null) {
                        PicsArtBeautyDialog.this.mDeformWrapper.restore();
                        PicsArtBeautyDialog.this.glSurfaceView.requestRender();
                    }
                }
            });
            if (this.isCloseDialog) {
                PicsArtBeautyDialog.this.onBeautySave.onBeautySave(bitmap);
                PicsArtBeautyDialog.this.dismiss();
            }
        }


    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mDeformWrapper != null) {
            this.mDeformWrapper.release(true);
            this.mDeformWrapper = null;
        }
        this.glSurfaceView.release();
        this.glSurfaceView.onPause();
    }
}
