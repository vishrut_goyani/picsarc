package picsarc.picspro.picslab.photocollageeditor.utils;

import java.util.ArrayList;

public class Constants {
    public static int BORDER_WIDTH_DP = 5;
    public static ArrayList<String> FORMAT_IMAGE = new ImageTypeList();
    public static String PKG_APP = "photoalbumgallery.photomanager.securegallery";
    public static final String PRIVACY_POLICY = "https://sites.google.com/site/scriptsexamples/available-web-apps/privacy-policy";

    static class ImageTypeList extends ArrayList<String> {
        ImageTypeList() {
            add(".PNG");
            add(".JPEG");
            add(".jpg");
            add(".png");
            add(".jpeg");
            add(".JPG");
        }
    }
}