package picsarc.picspro.picslab.photocollageeditor.features.addtext.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.features.addtext.AddTextProperties;

import java.util.List;

public class AdapterShadow extends RecyclerView.Adapter<AdapterShadow.ViewHolder> {
    private Context context;
    private List<AddTextProperties.TextShadow> lstTextShadows;
    public ShadowItemClickListener mClickListener;
    private LayoutInflater mInflater;
    public int selectedItem = 0;

    public interface ShadowItemClickListener {
        void onShadowItemClick(int i);
    }

    public int getItemViewType(int i) {
        return i;
    }

    public AdapterShadow(Context context2, List<AddTextProperties.TextShadow> list) {
        this.mInflater = LayoutInflater.from(context2);
        this.context = context2;
        this.lstTextShadows = list;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this.mInflater.inflate(R.layout.shadow_adapter, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int i2;
        AddTextProperties.TextShadow textShadow = this.lstTextShadows.get(i);
        viewHolder.textShadow.setShadowLayer((float) textShadow.getRadius(), (float) textShadow.getDx(), (float) textShadow.getDy(), textShadow.getColorShadow());
        ConstraintLayout constraintLayout = viewHolder.wrapperFontItems;
        if (this.selectedItem != i) {
            i2 = R.drawable.border_black_view;
        } else {
            i2 = R.drawable.border_view;
        }
        constraintLayout.setBackground(ContextCompat.getDrawable(context, i2));
    }

    public int getItemCount() {
        return this.lstTextShadows.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textShadow;
        ConstraintLayout wrapperFontItems;

        ViewHolder(View view) {
            super(view);
            this.textShadow = view.findViewById(R.id.font_item);
            this.wrapperFontItems = view.findViewById(R.id.wrapper_font_item);
            view.setOnClickListener(this);
        }

        public void onClick(View view) {
            if (AdapterShadow.this.mClickListener != null) {
                AdapterShadow.this.mClickListener.onShadowItemClick(getAdapterPosition());
            }
            AdapterShadow.this.selectedItem = getAdapterPosition();
            AdapterShadow.this.notifyDataSetChanged();
        }
    }

    public void setSelectedItem(int i) {
        this.selectedItem = i;
    }

    public void setClickListener(ShadowItemClickListener shadowItemClickListener) {
        this.mClickListener = shadowItemClickListener;
    }
}
