package picsarc.picspro.picslab.photocollageeditor.features.college.layout.straight;

import picsarc.picspro.picslab.photocollageeditor.features.college.Line;
import picsarc.picspro.picslab.photocollageeditor.features.college.CollegeLayout;
import picsarc.picspro.picslab.photocollageeditor.features.college.straight.StraightCollegeLayout;

public class OneStraightLayout extends NumberStraightLayout {
    public int getThemeCount() {
        return 6;
    }

    public OneStraightLayout(StraightCollegeLayout straightPuzzleLayout, boolean z) {
        super(straightPuzzleLayout, z);
    }

    public OneStraightLayout(int i) {
        super(i);
    }

    public void layout() {
        switch (this.theme) {
            case 0:
                addLine(0,
                        Line.Direction.HORIZONTAL, 0.5f);
                return;
            case 1:
                addLine(0, Line.Direction.VERTICAL, 0.5f);
                return;
            case 2:
                addCross(0, 0.5f);
                return;
            case 3:
                cutAreaEqualPart(0, 2, 1);
                return;
            case 4:
                cutAreaEqualPart(0, 1, 2);
                return;
            case 5:
                cutAreaEqualPart(0, 2, 2);
                return;
            default:
                addLine(0, Line.Direction.HORIZONTAL, 0.5f);
        }
    }

    public CollegeLayout clone(CollegeLayout collegeLayout) {
        return new OneStraightLayout((StraightCollegeLayout) collegeLayout, true);
    }
}
