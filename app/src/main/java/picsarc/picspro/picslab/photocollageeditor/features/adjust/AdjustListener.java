package picsarc.picspro.picslab.photocollageeditor.features.adjust;

public interface AdjustListener {
    void onAdjustSelected(AdapterAdjust.AdjustModel adjustModel);
}
