package picsarc.picspro.picslab.photocollageeditor.features.addtext.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.utils.UtilsFont;

import java.util.List;

public class AdapterFonts extends RecyclerView.Adapter<AdapterFonts.ViewHolder> {
    private Context context;
    private List<String> lstFonts;

    public ItemClickListener mClickListener;
    private LayoutInflater mInflater;

    public int selectedItem = 0;

    public interface ItemClickListener {
        void onItemClick(int i);
    }

    public int getItemViewType(int i) {
        return i;
    }

    public AdapterFonts(Context context2, List<String> list) {
        this.mInflater = LayoutInflater.from(context2);
        this.context = context2;
        this.lstFonts = list;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this.mInflater.inflate(R.layout.font_adapter, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int i2;
        UtilsFont.setFontByName(this.context, viewHolder.font, this.lstFonts.get(i));
        ConstraintLayout constraintLayout = viewHolder.wrapperFontItems;
        if (this.selectedItem != i) {
            i2 = R.drawable.border_black_view;
        } else {
            i2 = R.drawable.border_view;
        }
        constraintLayout.setBackground(ContextCompat.getDrawable(context, i2));
    }

    public int getItemCount() {
        return this.lstFonts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView font;
        ConstraintLayout wrapperFontItems;

        ViewHolder(View view) {
            super(view);
            this.font = view.findViewById(R.id.font_item);
            this.wrapperFontItems = view.findViewById(R.id.wrapper_font_item);
            view.setOnClickListener(this);
        }

        public void onClick(View view) {
            selectedItem = getAdapterPosition();
            if (AdapterFonts.this.mClickListener != null) {
                AdapterFonts.this.mClickListener.onItemClick(selectedItem);
            }
            notifyDataSetChanged();
        }
    }

    public void setSelectedItem(int i) {
        this.selectedItem = i;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}
