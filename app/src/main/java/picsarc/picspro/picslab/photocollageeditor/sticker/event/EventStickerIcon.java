package picsarc.picspro.picslab.photocollageeditor.sticker.event;

import android.view.MotionEvent;

import picsarc.picspro.picslab.photocollageeditor.sticker.StickerView;

public interface EventStickerIcon {
    void onActionDown(StickerView paramStickerView, MotionEvent paramMotionEvent);

    void onActionMove(StickerView paramStickerView, MotionEvent paramMotionEvent);

    void onActionUp(StickerView paramStickerView, MotionEvent paramMotionEvent);
}
