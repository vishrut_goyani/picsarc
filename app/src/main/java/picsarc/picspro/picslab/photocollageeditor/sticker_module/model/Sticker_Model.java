package picsarc.picspro.picslab.photocollageeditor.sticker_module.model;

public class Sticker_Model {
    public static class Datas {
        private String category_name;
        private String category_big_thumb;
        private String category_zip_url;

        public String getCategory_name() {
            return category_name;
        }

        public String getCategory_big_thumb() {
            return category_big_thumb;
        }

        public String getCategory_zip_url() {
            return category_zip_url;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public void setCategory_big_thumb(String category_big_thumb) {
            this.category_big_thumb = category_big_thumb;
        }

        public void setCategory_zip_url(String category_zip_url) {
            this.category_zip_url = category_zip_url;
        }
    }
}
