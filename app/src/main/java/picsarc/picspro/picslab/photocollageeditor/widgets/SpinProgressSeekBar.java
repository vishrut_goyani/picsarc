package picsarc.picspro.picslab.photocollageeditor.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;

import picsarc.picspro.picslab.photocollageeditor.R;

public class SpinProgressSeekBar extends View {

    private final Rect mCanvasClipBounds = new Rect();

    private ScrollingListener mScrollingListener;
    private float mLastTouchedPosition;

    private Paint mProgressLinePaint;
    private Paint mProgressMiddleLinePaint;
    private int mProgressLineWidth, mProgressLineHeight;
    private int mProgressLineMargin;

    private boolean mScrollStarted;
    private float mTotalScrollDistance;

    private int mMiddleLineColor;

    private int mCurrentDegrees = 0;
    private float mDragFactor = 2.1f;
    private int mMinReachableDegrees = -45;
    private int mMaxReachableDegrees = 45;

    public SpinProgressSeekBar(Context context) {
        this(context, null);
    }

    public SpinProgressSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpinProgressSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SpinProgressSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setScrollingListener(ScrollingListener scrollingListener) {
        mScrollingListener = scrollingListener;
    }

    public void setMiddleLineColor(@ColorInt int middleLineColor) {
        mMiddleLineColor = middleLineColor;
        mProgressMiddleLinePaint.setColor(mMiddleLineColor);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastTouchedPosition = event.getX();
                if (!mScrollStarted) {
                    mScrollStarted = true;
                    if (mScrollingListener != null) {
                        mScrollingListener.onScrollStart();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                mScrollStarted = false;
                if (mScrollingListener != null) {
                    mScrollingListener.onScrollEnd();
                }
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                float distance = event.getX() - mLastTouchedPosition;
                if (mCurrentDegrees >= mMaxReachableDegrees && distance < 0) {
                    mCurrentDegrees = mMaxReachableDegrees;
                    invalidate();
                    break;
                }
                if (mCurrentDegrees <= mMinReachableDegrees && distance > 0) {
                    mCurrentDegrees = mMinReachableDegrees;
                    invalidate();
                    break;
                }
                if (distance != 0) {
                    onScrollEvent(event, distance);
                }
                break;
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.getClipBounds(mCanvasClipBounds);

        int linesCount = mCanvasClipBounds.width() / (mProgressLineWidth + mProgressLineMargin);
        float deltaX = (mTotalScrollDistance) % (float) (mProgressLineMargin + mProgressLineWidth);

        for (int i = 0; i < linesCount; i++) {
            if (i < (linesCount / 2)) {
                mProgressLinePaint.setAlpha((int) (255 * (i / (float) (linesCount / 2))));
            } else if (i > (linesCount / 2)) {
                mProgressLinePaint.setAlpha((int) (255 * ((linesCount - i) / (float) (linesCount / 2))));
            } else {
                mProgressLinePaint.setAlpha(255);
            }
            canvas.drawLine(
                    -deltaX + mCanvasClipBounds.left + i * (mProgressLineWidth + mProgressLineMargin),
                    mCanvasClipBounds.centerY() - mProgressLineHeight / 4.0f,
                    -deltaX + mCanvasClipBounds.left + i * (mProgressLineWidth + mProgressLineMargin),
                    mCanvasClipBounds.centerY() + mProgressLineHeight / 4.0f, mProgressLinePaint);
        }

        canvas.drawLine(mCanvasClipBounds.centerX(), mCanvasClipBounds.centerY() - mProgressLineHeight / 2.0f, mCanvasClipBounds.centerX(), mCanvasClipBounds.centerY() + mProgressLineHeight / 2.0f, mProgressMiddleLinePaint);
    }

    public void setCurrentDegree(int mCurrentDegree) {
        this.mCurrentDegrees = mCurrentDegree;
    }

    public int getCurrentDegrees() {
        return mCurrentDegrees;
    }

    public void setDegreeRange(int min, int max) {
        if (min > max) {
        } else {
            mMinReachableDegrees = min;
            mMaxReachableDegrees = max;

            if (mCurrentDegrees >= mMaxReachableDegrees || mCurrentDegrees <= mMinReachableDegrees) {
                mCurrentDegrees = (mMinReachableDegrees + mMaxReachableDegrees) / 2;
            }
            mTotalScrollDistance = 0;
            invalidate();
        }
    }

    private void init() {
        mMiddleLineColor = ContextCompat.getColor(getContext(), R.color.seekbardegree_color_widget_rotate_mid_line);

        mProgressLineWidth = getContext().getResources().getDimensionPixelSize(R.dimen.spinprogress_width_horizontal_wheel_progress_line);
        mProgressLineHeight = getContext().getResources().getDimensionPixelSize(R.dimen.spinprogress_height_horizontal_wheel_progress_line);
        mProgressLineMargin = getContext().getResources().getDimensionPixelSize(R.dimen.spinprogress_margin_horizontal_wheel_progress_line);

        mProgressLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mProgressLinePaint.setStyle(Paint.Style.STROKE);
        mProgressLinePaint.setStrokeWidth(mProgressLineWidth);
        mProgressLinePaint.setColor(getResources().getColor(R.color.seekbardegree_color_progress_wheel_line));

        mProgressMiddleLinePaint = new Paint(mProgressLinePaint);
        mProgressMiddleLinePaint.setColor(mMiddleLineColor);
        mProgressMiddleLinePaint.setStrokeCap(Paint.Cap.ROUND);
        mProgressMiddleLinePaint.setStrokeWidth(getContext().getResources().getDimensionPixelSize(R.dimen.spinprogress_width_middle_wheel_progress_line));
    }

    public interface ScrollingListener {

        void onScrollStart();

        void onScroll(int currentDegrees);

        void onScrollEnd();
    }

    private void onScrollEvent(MotionEvent event, float distance) {
        mTotalScrollDistance -= distance;
        postInvalidate();
        mLastTouchedPosition = event.getX();
        mCurrentDegrees = (int) ((mTotalScrollDistance * mDragFactor) / mProgressLineMargin);
        if (mScrollingListener != null) {
            mScrollingListener.onScroll(mCurrentDegrees);
        }
    }
}