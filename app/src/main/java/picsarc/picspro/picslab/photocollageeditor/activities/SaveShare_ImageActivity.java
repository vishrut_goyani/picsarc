package picsarc.picspro.picslab.photocollageeditor.activities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;

import java.io.File;

import dev.yuganshtyagi.smileyrating.SmileyRatingView;
import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.base.BaseActivity;
import picsarc.picspro.picslab.photocollageeditor.interfaces.FeedbackListener;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;
import picsarc.picspro.picslab.photocollageeditor.widgets.FeedbackSheetBuilder;

public class SaveShare_ImageActivity extends BaseActivity implements View.OnClickListener, FeedbackListener {

    ImageView ic_home, preview;
    RelativeLayout instaBtn, whatsappBtn, facebookBtn, shareBtn;
    AlertDialog alertDialog;
    Toolbar toolbar;
    Uri uri;
    File file;

    public FeedbackListener feedbackListenerMain;
    FeedbackSheetBuilder feedbackSheetBuilder;
    SmileyRatingView smiley_view;
    AppCompatRatingBar ratingBar;
    TextView tv_submit, tv_maybe_later, rate_text;
    float rating;

    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        makeFullScreen();
        setContentView(R.layout.save_and_share_layout);
        String string = getIntent().getExtras().getString("path");
        file = new File(string);
        uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
        Glide.with(getApplicationContext()).load(file).into((ImageView) findViewById(R.id.preview));
        init();

        preview.setColorFilter(0x88000000, PorterDuff.Mode.OVERLAY);
        Glide.with(this)
                .load(string)
                .placeholder(android.R.drawable.ic_menu_report_image)
                .into(preview);

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_preview_image, null);
        ImageView previewImage = view.findViewById(R.id.imgPreview);
        LinearLayout container_main = view.findViewById(R.id.container_main);

        Glide.with(this)
                .load(file)
                .placeholder(android.R.drawable.ic_menu_report_image)
                .into(previewImage);
        alertDialog = new AlertDialog.Builder(this, R.style.Widget_Photo_DialogTheme)
                .setCancelable(true)
                .setView(container_main).create();

        preview.setOnClickListener(v -> alertDialog.show());
        ((TextView) findViewById(R.id.path)).setText(string);
        init();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        ic_home.setOnClickListener(this);
        instaBtn.setOnClickListener(this);
        whatsappBtn.setOnClickListener(this);
        facebookBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);

        if (!AppUtils.getInstance(SaveShare_ImageActivity.this).getRated())
            showRatingDialog();
    }

    private void showRatingDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.best_rating_dialog, null);
        initDialog(view);

        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setView(view);

        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        rate_text.setText(getResources().getString(R.string.rating_desc));

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            smiley_view.setSmiley(rating <= 4.5 ? rating : rating - 1);
            this.rating = rating;
        });

        tv_submit.setOnClickListener(v -> {
            if (rating >= 4) {
                AppUtils.getInstance(this).setRated(true);
                alertDialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            } else if (rating > 0) {
                alertDialog.dismiss();
                feedbackListenerMain.onFeedbackGiven();
            } else {
                Toast.makeText(SaveShare_ImageActivity.this, "Ratings can't be empty!", Toast.LENGTH_SHORT).show();
            }
        });

        tv_maybe_later.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    private void initDialog(View view) {
        rate_text = view.findViewById(R.id.rate_text);
        smiley_view = view.findViewById(R.id.smiley_view);
        ratingBar = view.findViewById(R.id.ratingBar);
        tv_submit = view.findViewById(R.id.tv_submit);
        tv_maybe_later = view.findViewById(R.id.tv_later);
    }

    private void init() {
        ic_home = findViewById(R.id.ic_home);
        instaBtn = findViewById(R.id.rvInsta);
        whatsappBtn = findViewById(R.id.rvWhatsApp);
        facebookBtn = findViewById(R.id.rvFB);
        shareBtn = findViewById(R.id.rvShareApp);
        toolbar = findViewById(R.id.toolbar);
        preview = findViewById(R.id.preview);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_home:
                Intent intent = new Intent(SaveShare_ImageActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.rvInsta:
                Intent intent1 = getPackageManager().getLaunchIntentForPackage("com.instagram.android");
                if (intent1 != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setPackage("com.instagram.android");
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                    startActivity(shareIntent);
                } else {
                    Toast.makeText(this, "Instagram is not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rvWhatsApp:
                Intent intent2 = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                if (intent2 != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setPackage("com.whatsapp");
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                    startActivity(shareIntent);
                } else {
                    Toast.makeText(this, "Whatsapp is not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rvFB:
                Intent intent3 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
                if (intent3 != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setPackage("com.facebook.katana");
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                    startActivity(shareIntent);
                } else {
                    Toast.makeText(this, "Whatsapp is not installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rvShareApp:
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                Uri uri1 = FileProvider.getUriForFile(this, getPackageName() + ".provider", new File(String.valueOf(uri)));
                share.putExtra(Intent.EXTRA_STREAM, uri1);
                share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(share, getString(R.string.send_to)));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onFeedbackGiven() {
        feedbackSheetBuilder = FeedbackSheetBuilder.with(getSupportFragmentManager())
                .title("Feedback Form")
                .setupLayout(SaveShare_ImageActivity.this, R.layout.bottom_sheet_feedback);
        View feedbackView = feedbackSheetBuilder.getLayout();
        initFeedbackheet(feedbackView);
        feedbackSheetBuilder.show();
    }

    private void initFeedbackheet(View feedbackView) {
        EditText editMail = feedbackView.findViewById(R.id.editMail);
        EditText editDesc = feedbackView.findViewById(R.id.editFeedback);
        Button sendEmail = feedbackView.findViewById(R.id.SendEmail);
        ImageView ic_close = feedbackView.findViewById(R.id.ic_close);

        sendEmail.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppUtils.RECEIVER_ADDRESS});
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_TEXT,
                    "Email" + " : " + editMail.getText().toString() + "\n"
                            + "Description" + " : " + editDesc.getText().toString());
            try {
                AppUtils.getInstance(this).setRated(true);
                startActivity(Intent.createChooser(intent, "Send Mail"));
            } catch (ActivityNotFoundException ex) {
                ex.printStackTrace();
                Toast.makeText(SaveShare_ImageActivity.this, getResources().getString(R.string.no_mail_found), Toast.LENGTH_SHORT).show();
            }
        });
        ic_close.setOnClickListener(v -> feedbackSheetBuilder.dismiss());
    }
}
