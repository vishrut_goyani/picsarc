package picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import java.io.File;
import java.util.ArrayList;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.model.PictureModel;
import picsarc.picspro.picslab.photocollageeditor.features.college.photopicker.myinterface.OnFolder;

public class GalleryAlbumAdapter extends ArrayAdapter<PictureModel> {
    Context context;
    ArrayList<PictureModel> data;
    int layoutResourceId;
    OnFolder onItem;
    int pHeightItem;
    int pWHIconNext;

    static class RecordHolder {
        ImageView iconNext;
        ImageView imageItem;
        RelativeLayout layoutRoot;
        TextView txtPath;
        TextView txtTitle;

        RecordHolder() {
        }
    }

    public GalleryAlbumAdapter(Context context2, int layoutId, ArrayList<PictureModel> arrayList) {
        super(context2, layoutId, arrayList);
        this.layoutResourceId = layoutId;
        this.context = context2;
        this.data = arrayList;
        this.pHeightItem = getDisplayInfo((Activity) context2).widthPixels / 6;
        this.pWHIconNext = this.pHeightItem / 4;
    }

    public View getView(final int i, View view, ViewGroup viewGroup) {
        RecordHolder recordHolder;
        if (view == null) {
            view = ((Activity) this.context).getLayoutInflater().inflate(this.layoutResourceId, viewGroup, false);
            recordHolder = new RecordHolder();
            recordHolder.txtTitle = view.findViewById(R.id.name_album);
            recordHolder.txtPath = view.findViewById(R.id.path_album);
            recordHolder.imageItem = view.findViewById(R.id.icon_album);
            recordHolder.iconNext = view.findViewById(R.id.iconNext);
            recordHolder.layoutRoot = view.findViewById(R.id.layoutRoot);
            recordHolder.layoutRoot.getLayoutParams().height = this.pHeightItem;
            recordHolder.imageItem.getLayoutParams().width = this.pHeightItem;
            recordHolder.imageItem.getLayoutParams().height = this.pHeightItem;
            recordHolder.iconNext.getLayoutParams().width = this.pWHIconNext;
            recordHolder.iconNext.getLayoutParams().height = this.pWHIconNext;
            view.setTag(recordHolder);
        } else {
            recordHolder = (RecordHolder) view.getTag();
        }
        PictureModel pictureModel = this.data.get(i);
        recordHolder.txtTitle.setText(pictureModel.getName());
        recordHolder.txtPath.setText(pictureModel.getPathFolder());
        ((RequestBuilder) Glide.with(this.context).load(new File(pictureModel.getPathFile())).placeholder(R.drawable.piclist_icon_default)).into(recordHolder.imageItem);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GalleryAlbumAdapter.this.onItem != null) {
                    GalleryAlbumAdapter.this.onItem.OnItemAlbumClick(i);
                }
            }
        });
        return view;
    }


    public void setOnItem(OnFolder onFolder) {
        this.onItem = onFolder;
    }

    public static DisplayMetrics getDisplayInfo(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }
}
