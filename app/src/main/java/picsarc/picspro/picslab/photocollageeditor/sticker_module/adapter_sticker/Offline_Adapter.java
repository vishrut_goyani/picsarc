package picsarc.picspro.picslab.photocollageeditor.sticker_module.adapter_sticker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import picsarc.picspro.picslab.photocollageeditor.R;
import picsarc.picspro.picslab.photocollageeditor.utils.AppUtils;

import java.util.ArrayList;


public class Offline_Adapter extends RecyclerView.Adapter<Offline_Adapter.myh> {
    Context context;
    ArrayList<String> strings;
    Sticker_interfce sticker_interfce;

    public Offline_Adapter(Context context, Sticker_interfce sticker_interfce, ArrayList<String> strings) {
        this.context = context;
        this.sticker_interfce = sticker_interfce;
        this.strings = strings;

    }

    @NonNull
    @Override
    public myh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_offline_frames, parent, false);
        return new myh(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myh holder, final int position) {

        Glide.with(context).load(this.strings.get(position)).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.images = strings.get(position);

                Drawable d = Drawable.createFromPath(AppUtils.images);

                AppUtils.Sticker_bitmap = drawableToBitmap(d);

                if (sticker_interfce != null) {
                    sticker_interfce.getsticker();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public class myh extends RecyclerView.ViewHolder {
        ImageView imageView;

        public myh(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;

    }
}
