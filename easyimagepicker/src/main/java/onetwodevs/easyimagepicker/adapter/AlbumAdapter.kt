package onetwodevs.easyimagepicker.adapter

import android.view.ViewGroup
import com.bumptech.glide.Glide
import onetwodevs.easyimagepicker.R
import onetwodevs.easyimagepicker.base.BaseRecyclerViewAdapter
import onetwodevs.easyimagepicker.base.BaseViewHolder
import onetwodevs.easyimagepicker.builder.EasyImagePickerBaseBuilder
import onetwodevs.easyimagepicker.databinding.ItemAlbumBinding
import onetwodevs.easyimagepicker.model.Album
import onetwodevs.easyimagepicker.util.TextFormatUtil

internal class AlbumAdapter(private val builder: EasyImagePickerBaseBuilder<*>) :
    BaseRecyclerViewAdapter<Album, AlbumAdapter.AlbumViewHolder>() {

    private var selectedPosition = 0

    override fun getViewHolder(parent: ViewGroup, viewType: ViewType) = AlbumViewHolder(parent)

    fun setSelectedAlbum(album: Album) {
        val index = items.indexOf(album)
        if (index >= 0 && selectedPosition != index) {
            val lastSelectedPosition = selectedPosition
            selectedPosition = index
            notifyItemChanged(lastSelectedPosition)
            notifyItemChanged(selectedPosition)
        }
    }

    inner class AlbumViewHolder(parent: ViewGroup) :
        BaseViewHolder<ItemAlbumBinding, Album>(parent, R.layout.item_album) {
        override fun bind(data: Album) {
            binding.album = data
            binding.isSelected = adapterPosition == selectedPosition
            binding.mediaCountText =
                TextFormatUtil.getMediaCountText(builder.imageCountFormat, data.mediaCount)
        }

        override fun recycled() {
            Glide.with(itemView).clear(binding.ivImage)
        }
    }
}