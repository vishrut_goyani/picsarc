package onetwodevs.easyimagepicker.builder.listener

interface OnErrorListener {
    fun onError(throwable: Throwable)
}
