package onetwodevs.easyimagepicker.builder

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import onetwodevs.easyimagepicker.builder.listener.OnErrorListener
import onetwodevs.easyimagepicker.builder.listener.OnMultiSelectedListener
import onetwodevs.easyimagepicker.builder.listener.OnSelectedListener
import onetwodevs.easyimagepicker.builder.type.SelectType
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.lang.ref.WeakReference


class EasyRxImagePicker {
    companion object {
        @JvmStatic
        fun with(context: Context) = Builder(WeakReference(context))
    }

    @SuppressLint("ParcelCreator")
    class Builder(private val contextWeakReference: WeakReference<Context>) :
        EasyImagePickerBaseBuilder<Builder>() {

        fun start(): Single<Uri> =
            Single.create { emitter ->
                this.onSelectedListener = object : OnSelectedListener {
                    override fun onSelected(uri: Uri) {
                        emitter.onSuccess(uri)
                    }
                }
                start(SelectType.SINGLE, emitter)
            }


        fun startMultiImage(): Single<List<Uri>> =
            Single.create { emitter ->
                this.onMultiSelectedListener = object : OnMultiSelectedListener {
                    override fun onSelected(uriList: List<Uri>) {
                        emitter.onSuccess(uriList)
                    }
                }
                start(SelectType.MULTI, emitter)
            }

        private fun start(selectType: SelectType, emitter: SingleEmitter<*>) {
            this.onErrorListener = object : OnErrorListener {
                override fun onError(throwable: Throwable) {
                    emitter.onError(throwable)
                }
            }
            this.selectType = selectType
            contextWeakReference.get()?.let {
                startInternal(it)
            }

        }
    }


}


