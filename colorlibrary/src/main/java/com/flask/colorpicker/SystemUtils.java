package com.flask.colorpicker;

import android.content.Context;

public class SystemUtils {
    public static int dpToPx(Context context, int i) {
        return (int) (((float) i) * context.getResources().getDisplayMetrics().density);
    }

}